package jmetal.util.comparators;

import java.util.Comparator;
import jmetal.core.Solution;

public class SingleObjectiveComparatorMinimum implements Comparator<Solution> {

	public SingleObjectiveComparatorMinimum() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public int compare(Solution S, Solution T) {
		if(S.getObjective(0) < T.getObjective(0)){
			return +1;
		} else if(S.getObjective(0) > T.getObjective(0)){
			return -1;
		} else {
			return 0;
		}
		
	}

}
