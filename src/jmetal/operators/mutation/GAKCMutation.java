package jmetal.operators.mutation;

import java.util.HashMap;

import zelig.util.MersenneTwister;

import jmetal.core.Solution;
import jmetal.core.Variable;
import jmetal.util.JMException;

public class GAKCMutation extends Mutation {

	private double razao;
	private double finalProbability;
	private int generations;
	private int generation;
	
	public GAKCMutation(HashMap<String, Object> parameters) {
		super(parameters);
		double initialP = (Double) parameters.get("initialProbability");
		this.finalProbability = (Double) parameters.get("finalProbability");
		this.generations = (Integer) parameters.get("generations");
		
		razao = initialP/finalProbability;
		
		generation = 0;
		
	}

	@Override
	public Object execute(Object object) throws JMException {
		Solution solution = new Solution((Solution) object);
		Variable[] genes = solution.getDecisionVariables();
		
		generation = (generation + 1) % generations;
		
		MersenneTwister R = new MersenneTwister();
		
		for (int i = 0; i < genes.length; i++) {
			double probability = finalProbability*Math.pow(razao, 1-(generation/generations));
			
			if(R.nextDouble() < probability){
				int upper = (int) (genes[i].getUpperBound() - (genes[i].getUpperBound() % 1));
				genes[i].setValue(1 + R.nextInt(upper));
			}
			
		}
				
		return solution;
	}
	
}
