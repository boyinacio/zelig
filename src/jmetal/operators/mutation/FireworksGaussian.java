package jmetal.operators.mutation;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import jmetal.core.Solution;
import jmetal.core.Variable;
import jmetal.util.JMException;
import jmetal.util.MersenneTwisterFast;
import jmetal.util.wrapper.XReal;

public class FireworksGaussian extends Mutation {

	private MersenneTwisterFast random;
	
	public FireworksGaussian(HashMap<String, Object> parameters) {
		super(parameters);
		random = new MersenneTwisterFast();
	}

	@Override
	public Object execute(Object object) throws JMException {
		Solution firework = (Solution) object;
		
		Solution newSolution = new Solution(firework);
		XReal dimensions = new XReal(firework);
		Variable[] vars = newSolution.getDecisionVariables();

		double z = firework.numberOfVariables() * random.nextDouble();
		double g = random.nextGaussian() + 1; // gaussian with mean 1 and
							// standard deviation 1
		double newValue;
		List<Integer> dimensionsSelected = new LinkedList<Integer>();
		for (int counter = 0, dimension = 0; counter < z; dimension = ((dimension + 1) % firework
				.numberOfVariables())) {
			if (random.nextBoolean() && !dimensionsSelected.contains(dimension)) {
				counter++;
				dimensionsSelected.add(dimension);
				try {
					newValue = dimensions.getValue(dimension) * g;

					if ((newValue < dimensions.getLowerBound(dimension))
							|| (newValue > dimensions.getUpperBound(dimension))) {
						newValue = dimensions.getLowerBound(dimension)
								+ (newValue % (dimensions
										.getUpperBound(dimension) - dimensions
										.getLowerBound(dimension)));
						vars[dimension].setValue(newValue);
					}
				} catch (JMException e) {
					e.printStackTrace();
					System.exit(1);
				}

			}
		}

		newSolution.setDecisionVariables(vars);
		return newSolution;
	}

}
