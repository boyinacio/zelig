package jmetal.operators.mutation;

import java.util.HashMap;

import jmetal.core.Solution;
import jmetal.core.Variable;
import jmetal.util.JMException;
import jmetal.util.PseudoRandom;
import weka.core.Instance;
import weka.core.Instances;
import zelig.util.Calculator;
import zelig.util.distances.Distance;
import zelig.util.translators.ClusterNumberTranslator;


public class DistanceBasedMutation extends Mutation {

	
	private static final long serialVersionUID = 446548696550686165L;
	
	private Double mutationProbability_ = null;
	private Instances base;
	private int numInstances;
	private Distance distanceMeasure;

	public DistanceBasedMutation(HashMap<String, Object> parameters) {
		super(parameters);
		if (parameters.get("probability") != null)
			mutationProbability_ = (Double) parameters.get("probability");
		base = new Instances((Instances) parameters.get("base"));
		numInstances = base.numInstances();
		distanceMeasure = (Distance) parameters.get("distance");
	}

	@Override
	public Object execute(Object object) throws JMException {
		Solution solution = new Solution((Solution) object);
		Variable[] vars = solution.getDecisionVariables();
		
		ClusterNumberTranslator translator = new ClusterNumberTranslator();
		
		this.base = translator.translate(solution, base);

		Instance[] centroids;
		double[] distancesToCentroids;
		double[] clustersProbabilities;
		double dmax, sumOfDistances, sumOfProbabilities = 0, selector;
		double cm = PseudoRandom.randInt(2, 4);

		for (int i = 0; i < numInstances; i++) {
			if (PseudoRandom.randDouble() < mutationProbability_) {
				centroids = Calculator.centroidsOf(this.base);
				distancesToCentroids = new double[centroids.length];
				clustersProbabilities = new double[centroids.length];
				
				
				for (int j = 0; j < distancesToCentroids.length; j++) {
					distancesToCentroids[j] = distanceMeasure.distanceBetween(
							base.instance(i), centroids[j]);
				}

				if (distanceMeasure.distanceBetween(base.instance(i),
						centroids[(int) base.instance(i).classValue()]) > 0) {
					
					dmax = distancesToCentroids[0];
					for (int j = 0; j < distancesToCentroids.length; j++) {
						if(dmax > distancesToCentroids[j]){
							dmax = distancesToCentroids[j];
						}
					}
					
					sumOfDistances = 0;
					for (int j = 0; j < distancesToCentroids.length; j++) {
						sumOfDistances += ((cm * dmax) - distancesToCentroids[j]);
					}

					for (int j = 0; j < centroids.length; j++) {
						clustersProbabilities[j] = ((cm * dmax) - distancesToCentroids[j])
								/ sumOfDistances;
					}
					
					for (int j = 0; j < clustersProbabilities.length; j++) {
						sumOfProbabilities += clustersProbabilities[j];
					}
									
					selector = PseudoRandom.randDouble(0, sumOfProbabilities);
					sumOfProbabilities = 0;
					
					for (int j = 0; j < clustersProbabilities.length; j++) {
						sumOfProbabilities += clustersProbabilities[j];
						if(sumOfProbabilities >= selector){
							vars[i].setValue(j);
							break;
						}
					}	
				}
			}
		}
		
		solution.setDecisionVariables(vars);

		return solution;
	}

}
