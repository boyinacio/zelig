package jmetal.operators.mutation;

import java.util.HashMap;

import jmetal.core.Solution;
import jmetal.core.Variable;
import jmetal.util.JMException;
import jmetal.util.PseudoRandom;

public class PARMutation extends Mutation {

	private double PAR = 0.5;
	
	public PARMutation(HashMap<String, Object> parameters) {
		super(parameters);
		if (parameters.get("PAR") != null) {
			PAR = (Double) parameters.get("PAR");
		}
	}

	@Override
	public Object execute(Object object) throws JMException {
		Solution solution = new Solution((Solution) object);
		Variable[] V = solution.getDecisionVariables();
		
		double coin;
		
		for (int i = 0; i < V.length; i++) {
			coin = PseudoRandom.randDouble();
			if(coin <= PAR){
				coin = PseudoRandom.randDouble();
				V[i].setValue(V[i].getValue()+1);
				if(coin < 0.5){
					V[i].setValue(V[i].getValue()-2);
				}
			}
		}
		
		solution.setDecisionVariables(V);
		
		return solution;
	}

}
