package jmetal.operators.crossover;

import java.util.HashMap;

import jmetal.core.Solution;
import jmetal.core.Variable;
import jmetal.util.JMException;
import jmetal.util.PseudoRandom;

public class UniformCrossover extends Crossover {

	public UniformCrossover(HashMap<String, Object> parameters) {
		super(parameters);
	}

	@Override
	public Object execute(Object object) throws JMException {
		Solution[] parents = (Solution[]) object;
		Solution child = new Solution(parents[0]);
		
		Variable[] dad = parents[0].getDecisionVariables();
		Variable[] mom = parents[1].getDecisionVariables();
		Variable[] chil = child.getDecisionVariables();
		
		
		int index;
		Variable var;
		
		for (int i = 0; i < dad.length/2; i++) {
			index = PseudoRandom.randInt(0, dad.length-1);
			chil[index].setValue(dad[index].getValue());
			
			index = PseudoRandom.randInt(0, mom.length-1);
			chil[index].setValue(mom[index].getValue());
		}
		
		child.setDecisionVariables(chil);
		
		return child;
	}

}
