package jmetal.operators.crossover;

import java.util.HashMap;

import weka.clusterers.HierarchicalClusterer;
import zelig.util.MersenneTwister;

import jmetal.core.Solution;
import jmetal.core.Variable;
import jmetal.util.JMException;

public class HeritageCrossover extends Crossover {

	public HeritageCrossover(HashMap<String, Object> parameters) {
		super(parameters);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Object execute(Object object) throws JMException {
		Solution[] parents = (Solution[]) object;
		
		Solution child = new Solution(parents[0]);
		
		Variable[] genes = child.getDecisionVariables();
		Variable[] momGenes = parents[1].getDecisionVariables();
		
		MersenneTwister R = new MersenneTwister();
		
		for (int i = 0; i < genes.length; i++) {
			if(genes[i].getValue() != momGenes[i].getValue()){
				if(R.nextDouble() >= 0.5){
					genes[i].setValue(momGenes[i].getValue());
				}
			}
		}
		
		child.setDecisionVariables(genes);
		
		return child;
	}

}
