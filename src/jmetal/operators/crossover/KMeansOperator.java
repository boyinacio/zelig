package jmetal.operators.crossover;

import java.util.Comparator;
import java.util.HashMap;

import jmetal.core.Solution;
import jmetal.core.Variable;
import jmetal.util.JMException;
import jmetal.util.comparators.SingleObjectiveComparatorMaximum;
import jmetal.util.comparators.SingleObjectiveComparatorMinimum;
import weka.core.Instance;
import weka.core.Instances;
import zelig.util.Calculator;
import zelig.util.distances.Distance;
import zelig.util.translators.ClusterNumberTranslator;


public class KMeansOperator extends Crossover {

	private Instances base;
	private int numInstances;
	private Distance distanceMeasure;
	private Comparator<Solution> comparator;

	public KMeansOperator(HashMap<String, Object> parameters) {
		super(parameters);
		base = new Instances((Instances) parameters.get("base"));
		numInstances = base.numInstances();
		distanceMeasure = (Distance) parameters.get("distance");
		comparator = (Comparator) parameters.get("comparator");
	}

	@Override
	public Object execute(Object object) throws JMException {
		Solution[] parents = (Solution[]) object;
		Solution solution;
		
		if(comparator.compare(parents[0], parents[1]) > 0){
			solution = new Solution(parents[0]);
		} else {
			solution = new Solution(parents[1]);
		}
				
		Variable[] vars = solution.getDecisionVariables();
		
		ClusterNumberTranslator translator = new ClusterNumberTranslator();
		
		this.base = translator.translate(solution, base);
		
		
		Instance[] centroids = Calculator.centroidsOf(base);
		double[] distanceToCentroids = new double[centroids.length];
		double leastDistance;

		for (int i = 0; i < numInstances; i++) {
			
			for (int j = 0; j < distanceToCentroids.length; j++) {
				distanceToCentroids[j] = distanceMeasure.distanceBetween(
						base.instance(i), centroids[j]);
			}
			
			leastDistance = distanceToCentroids[0];
			
			for (int j = 0; j < distanceToCentroids.length; j++) {
				if(leastDistance > distanceToCentroids[j]){
					leastDistance = distanceToCentroids[j];
					vars[i].setValue(j);
				}
			}

		}
		
		solution.setDecisionVariables(vars);

		return solution;
	}

}
