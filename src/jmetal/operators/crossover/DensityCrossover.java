package jmetal.operators.crossover;

import java.util.HashMap;

import jmetal.core.Solution;
import jmetal.core.Variable;
import jmetal.util.JMException;
import weka.core.Instances;
import zelig.util.Calculator;
import zelig.util.clusteringEvaluators.MXIndex;
import zelig.util.distances.Distance;
import zelig.util.translators.Translator;

/**
 * Implements a crossover based on density
 * 
 * For each instance I of base, it finds its nearest neighbor , verifies the
 * best cluster (between dad and mom solutions, according to MXIndex) this
 * neighbor belongs, and assigns I to that cluster.
 * 
 * @author Inacio Gomes
 * 
 */

public class DensityCrossover extends Crossover {

	private Instances base, dadBase, momBase;
	private double EPS;
	private Distance measure;
	private Translator translator;
	private double[][] distanceMatrix;
	private double[] mom;
	private double[] father;
	private Variable[] vars, momVars, dadVars;
	
	public DensityCrossover(HashMap<String, Object> parameters) {
		super(parameters);
		base = (Instances) parameters.get("base");
		EPS = (Double) parameters.get("EPS");
		measure = (Distance) parameters.get("distance");
		translator = (Translator) parameters.get("translator");

		distanceMatrix = Calculator.baseDistanceMatrix(base, measure);
	}

	@Override
	public Object execute(Object object) throws JMException {
		Solution[] parents = (Solution[]) object;
		Solution dad = new Solution(parents[0]);

		vars = dad.getDecisionVariables();
		momVars = parents[1].getDecisionVariables();
		dadVars = parents[0].getDecisionVariables();

		mom = new double[momVars.length];
		father = new double[momVars.length];

		for (int i = 0; i < mom.length; i++) {
			mom[i] = momVars[i].getValue();
		}

		for (int i = 0; i < mom.length; i++) {
			father[i] = dadVars[i].getValue();
		}

		dadBase = translator.translate(parents[0], new Instances(base));
		momBase = translator.translate(parents[1], new Instances(base));

		int nearestNeighborIndex;
		double leastDistance, dadClusterIndex, momClusterIndex;

		for (int i = 0; i < distanceMatrix.length; i++) {
			nearestNeighborIndex = i;
			leastDistance = Double.MAX_VALUE;

			for (int j = 0; j < distanceMatrix.length; j++) {
				if (j == i) {
					continue;
				}

				if (distanceMatrix[i][j] < leastDistance) {
					nearestNeighborIndex = j;
					leastDistance = distanceMatrix[i][j];
				}
			}

			momClusterIndex = MXIndex.evaluate(momBase, measure, EPS,
					mom[nearestNeighborIndex], distanceMatrix);

			dadClusterIndex = MXIndex.evaluate(dadBase, measure, EPS,
					father[nearestNeighborIndex], distanceMatrix);

			if (momClusterIndex > dadClusterIndex) {
				vars[i].setValue(mom[nearestNeighborIndex]);
			}
		}

		Solution child = new Solution(parents[0]);
		child.setDecisionVariables(vars);

		return child;
	}
}
