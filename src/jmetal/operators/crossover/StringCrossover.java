package jmetal.operators.crossover;

import java.util.HashMap;

import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.core.Variable;
import jmetal.util.JMException;

public class StringCrossover extends Crossover {

	private static final long serialVersionUID = -1422491149968148822L;

	Problem problem;
	
	public StringCrossover(HashMap<String, Object> parameters) {
		super(parameters);
		problem = (Problem) parameters.get("problem");
	}

	@Override
	public Object execute(Object object) throws JMException {
		Solution[] parents = (Solution[]) object;
		Solution[] child = new Solution[2];
		
		child[0] = new Solution(parents[0]);
		Variable[] varsA = child[0].getDecisionVariables();
		
		child[1] = new Solution(parents[1]);
		Variable[] varsB = child[0].getDecisionVariables();
		
		for (int i = varsB.length/2; i < varsB.length; i++) {
			varsB[i].setValue(varsA[i].getValue());
		}
		
		for (int i = 0; i < varsA.length; i++) {
			varsA[i].setValue(varsB[i].getValue());
		}
		
		child[0].setDecisionVariables(varsA);
		problem.evaluate(child[0]);
		
		child[1].setDecisionVariables(varsB);
		problem.evaluate(child[1]);
		
		if(child[0].getObjective(0) < child[1].getObjective(0)){
			return child[0];
		}
		
		return child[1];
	}

}
