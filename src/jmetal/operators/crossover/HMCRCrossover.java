package jmetal.operators.crossover;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import jmetal.core.Solution;
import jmetal.core.SolutionSet;
import jmetal.core.Variable;
import jmetal.util.JMException;
import jmetal.util.PseudoRandom;

/**
 * This class allows to apply a HMCR crossover (from Harmony Seach) operator for
 * CRO-HS using a SolutionSet called "Broadcast Spawners".
 */
public class HMCRCrossover extends Crossover {

	private double HMCR = 0.5;
	private HashMap<Double, Integer> observation; // How many times a value
													// appears in all solutions

	public HMCRCrossover(HashMap<String, Object> parameters) {
		super(parameters);
		if (parameters.get("HMCR") != null) {
			HMCR = (Double) parameters.get("HMCR");
		}
	}

	@Override
	public Object execute(Object object) throws JMException {
		SolutionSet broadcastSpawners = (SolutionSet) object;
		observation = new HashMap<Double, Integer>();

		Solution S;
		Variable[] V;
		Integer I;
		int sum = 0;

		// Builds up map, storing the plenty of times that each value appear in
		// each
		// solution
		for (int i = 0; i < broadcastSpawners.size(); i++) {
			S = broadcastSpawners.get(i);
			V = S.getDecisionVariables();

			for (int j = 0; j < V.length; j++) {
				if (observation.containsKey(V[j].getValue())) {
					I = observation.get(V[j].getValue());
					observation.put(V[j].getValue(), I + 1);
				} else {
					observation.put(V[j].getValue(), 1);
				}
				sum++;
			}
		}

		// Computes probabilites of each value
		Set<Double> keys = observation.keySet();

		Double[] keysValues = keys.toArray(new Double[0]);
		double[] probabilities = new double[keys.size()];

		int k = 0;
		for (Double key : keys) {
			probabilities[k] = observation.get(key) / sum;
			k++;
		}

		SolutionSet children = new SolutionSet(broadcastSpawners.size());
		double coin;
		int lower, upper, index;

		for (int j = 0; j < (broadcastSpawners.size() / 2); j++) {
			S = new Solution(broadcastSpawners.get(0));
			V = S.getDecisionVariables();

			for (int j2 = 0; j2 < V.length; j2++) {
				coin = PseudoRandom.randDouble();
				lower = (int) V[j2].getLowerBound();
				upper = (int) V[j2].getUpperBound();

				if (coin > HMCR) {
					V[j2].setValue(PseudoRandom.randInt(lower, upper));
				} else {
					index = -1;
					coin = PseudoRandom.randDouble();
					for (int l = 0; l < probabilities.length; l++) {
						if (probabilities[l] >= coin) {
							index = l;
							break;
						}
					}

					if (index != -1) {
						V[j2].setValue(keysValues[index]);
					} else {
						V[j2].setValue(PseudoRandom.randInt(lower, upper));
					}

				}
			}

			S.setDecisionVariables(V);
			children.add(S);

		}

		return children;
	}
}
