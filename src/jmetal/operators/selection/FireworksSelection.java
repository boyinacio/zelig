package jmetal.operators.selection;

import java.util.HashMap;

import jmetal.core.SolutionSet;
import jmetal.util.JMException;
import jmetal.util.MersenneTwisterFast;

public class FireworksSelection extends Selection {

	private MersenneTwisterFast random;

	public FireworksSelection(HashMap<String, Object> parameters) {
		super(parameters);
		random = new MersenneTwisterFast();
	}

	@Override
	public Object execute(Object object) throws JMException {
		Object[] data = (Object[]) object;
		SolutionSet[] sets = (SolutionSet[]) data[0]; // Are 3 SolutionSets
		double[] cumulativeProbs = (double[]) data[1];

		int firstSetSize = sets[0].size();
		int secondSetSize = sets[1].size();

		double randomPointer = random.nextDouble()
				% cumulativeProbs[cumulativeProbs.length - 1];

		int pos;
		for (pos = 0; pos < cumulativeProbs.length; pos++) {
			if (cumulativeProbs[pos] >= randomPointer) {
				break;
			}
		}
		
		if(pos == cumulativeProbs.length){
			pos = cumulativeProbs.length-1;
		}
		
		if (pos < firstSetSize) { // solution is in first set
			return sets[0].get(pos);
			// solution is in second set
		} else if ((pos >= firstSetSize)
				&& (pos < (firstSetSize + secondSetSize))) {
			pos = pos - firstSetSize; // "shifts" pos to range [0, #2ndSet-1]
			return sets[1].get(pos);
			// solution is in third set
		} else {
			pos = pos - firstSetSize - secondSetSize; // "shifts" pos to range
														// [0, #3rdSet-1]
			return sets[2].get(pos);
		}
	}

}
