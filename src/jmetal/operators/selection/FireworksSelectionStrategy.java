package jmetal.operators.selection;

import java.util.Comparator;
import java.util.HashMap;

import jmetal.core.Solution;
import jmetal.core.SolutionSet;
import jmetal.util.JMException;
import jmetal.util.MersenneTwisterFast;

public class FireworksSelectionStrategy extends Selection {

	private MersenneTwisterFast random;
	private Comparator comparator;
	
	public FireworksSelectionStrategy(HashMap<String, Object> parameters) {
		super(parameters);
		random = new MersenneTwisterFast();
		comparator = (Comparator) parameters.get("comparator");
	}

	@Override
	public Object execute(Object object) throws JMException {		
		Object[] data = (Object[]) object;
		
		SolutionSet S = (SolutionSet) data[0];
		SolutionSet T = (SolutionSet) data[1];
		SolutionSet U = (SolutionSet) data[2];
		
		int Ssize = S.size();
		int Tsize = T.size();
		
		double[] probabilities = (double[]) data[3];
		
		double randomPointer = random.nextDouble();
		
		for (int i = 0; i < probabilities.length; i++) {
			if(randomPointer <= probabilities[i]){
				if(i < Ssize){
					return S.get(i);
				} else if(i < (Ssize + Tsize)){
					return S.get(i-Ssize);
				} else {
					return S.get(i-Ssize-Tsize);
				}
			}
		}
		
		Solution best = S.best(comparator);
		
		if(comparator.compare(T.best(comparator), best) < 0){
			best = T.best(comparator);
		}
		
		if(comparator.compare(U.best(comparator), best) < 0){
			best = U.best(comparator);
		}
		
		return best;
	}

}
