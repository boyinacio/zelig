package jmetal.operators.selection;

import java.util.HashMap;

import jmetal.core.Solution;
import jmetal.core.SolutionSet;
import jmetal.operators.selection.Selection;
import jmetal.util.JMException;
import jmetal.util.PseudoRandom;

public class GKAMaxRouletteWheelSelection extends Selection {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5911970168060219655L;

	public GKAMaxRouletteWheelSelection(HashMap<String, Object> parameters) {
		super(parameters);
	}

	@Override
	public Object execute(Object object) throws JMException {
		SolutionSet population = (SolutionSet) object;
		int popSize = population.size();
		
		SolutionSet newPopulation = new SolutionSet(popSize);
		Solution chosen = null;
		
		double[] probs = new double[popSize];
		double sumOfProbs = 0, selector;
		int index;
		
		for (int i = 0; i < probs.length; i++) {
			probs[i] = population.get(i).getObjective(0);
			sumOfProbs += probs[i]; 
		}
		
		for (int i = 0; i < probs.length; i++) {
			probs[i] /= sumOfProbs;
		}
		
		for (int i = 0; i < 1; i++) {
			selector = PseudoRandom.randDouble(0, sumOfProbs);
			sumOfProbs = 0;
			index = 0;
			
			for (int j = 0; j < probs.length; j++) {
				sumOfProbs += probs[j];
				if(sumOfProbs >= selector){
					index = j;
					break;
				}
			}
			
			chosen = new Solution(population.get(index));
			newPopulation.add(chosen);
			
		}
				
		return newPopulation;
	}

}
