package jmetal.encodings.solutionType;

import jmetal.core.Problem;
import jmetal.core.SolutionType;
import jmetal.core.Variable;
import jmetal.encodings.variable.Int;

public class ClusterNumberSolutionType extends SolutionType {

	private int maxClusters;
	
	public ClusterNumberSolutionType(Problem problem) {
		super(problem);
	}
		
	public ClusterNumberSolutionType(Problem problem, int maxClusters) {
		super(problem);
		this.maxClusters = maxClusters;
	}

	@Override
	public Variable[] createVariables() throws ClassNotFoundException {
		Variable[] variables = new Variable[problem_.getNumberOfVariables()];

		for (int var = 0; var < problem_.getNumberOfVariables(); var++)
			variables[var] = new Int(1,this.maxClusters);
		
		return variables ;
	}

}
