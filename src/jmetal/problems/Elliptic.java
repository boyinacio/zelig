/**
 * 
 */
package jmetal.problems;

import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.core.Variable;
import jmetal.encodings.solutionType.BinaryRealSolutionType;
import jmetal.encodings.solutionType.RealSolutionType;
import jmetal.util.JMException;

/**
 * @author Samih
 *
 */
public class Elliptic extends Problem {

	public Elliptic(String solutionType, Integer numberOfVariables) {
		numberOfVariables_ = numberOfVariables;
		numberOfObjectives_ = 1;
		numberOfConstraints_ = 0;
		problemName_ = "High Conditioned Elliptic";

		upperLimit_ = new double[numberOfVariables_];
		lowerLimit_ = new double[numberOfVariables_];
		for (int var = 0; var < numberOfVariables_; var++) {
			lowerLimit_[var] = -5.12;
			upperLimit_[var] = 5.12;
		} // for

		if (solutionType.compareTo("BinaryReal") == 0)
			solutionType_ = new BinaryRealSolutionType(this);
		else if (solutionType.compareTo("Real") == 0)
			solutionType_ = new RealSolutionType(this);
		else {
			System.out.println("Error: solution type " + solutionType
					+ " invalid");
			System.exit(-1);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see jmetal.core.Problem#evaluate(jmetal.core.Solution)
	 */
	@Override
	public void evaluate(Solution solution) throws JMException {
		Variable[] decisionVariables = solution.getDecisionVariables();

		double sum = 0.0;
		for (int var = 0; var < numberOfVariables_; var++) {
			// sum += StrictMath.pow(decisionVariables[var].getValue(), 2.0);
			sum += StrictMath.pow(10.0, 6.0 * var / (numberOfVariables_ - 1))
					* decisionVariables[var].getValue()
					* decisionVariables[var].getValue();
		}
		solution.setObjective(0, sum);
	}

}
