package jmetal.problems;

import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.core.SolutionType;
import jmetal.core.Variable;
import jmetal.encodings.solutionType.RealSolutionType;
import jmetal.util.JMException;

/**
 * Implementa a Egghold Funcion
 * Ref.: http://www.sfu.ca/~ssurjano/egg.html
 * @author inacio
 *
 */
public class EggholderFunction extends Problem {

	public EggholderFunction() throws ClassNotFoundException {
		numberOfVariables_   = 2 ;
	    numberOfObjectives_  = 1 ;
	    numberOfConstraints_ = 0 ;
	    problemName_         = "EggholderFunctionProblem" ;
	      
	    solutionType_ = new RealSolutionType(this);
	    
	    upperLimit_ = new double[numberOfVariables_] ;
	    lowerLimit_ = new double[numberOfVariables_] ;
	       
	    for (int i = 0; i < numberOfVariables_; i++) {
	      lowerLimit_[i] = -512;
	      upperLimit_[i] = 512;
	    } // for
	}

	public EggholderFunction(SolutionType solutionType) {
		super(solutionType);
	}

	@Override
	public void evaluate(Solution solution) throws JMException {
		Variable[] vars = solution.getDecisionVariables();
		
		double x1 = vars[0].getValue();
		double x2 = vars[1].getValue();
		
		double term1 = -(x2+47) * Math.sin(Math.sqrt(Math.abs(x2+x1/2+47)));
		double term2 = -x1 * Math.sin(Math.sqrt(Math.abs(x1-(x2+47))));
		
		solution.setObjective(0, term1+term2);

	}

}
