package jmetal.problems.CEC2015;

public class TestProgram {

	public static void main(String[] args) {
		long startTime;
		long endTime;

		double x;
		
		startTime = System.currentTimeMillis();
		
		for (int i = 0; i < 1000000; i++) {
			x = 0.55 + (double) i;
			x = x + x;
			x = x / 2;
			x = x * x;
			x = Math.sqrt(x);
			x = Math.log(x);
			x = Math.exp(x);
			x = x / (x + 2);
		}
		
		endTime = System.currentTimeMillis();
		
		System.out.println("T0: " + (endTime - startTime) + " milisseconds");

	}
}