package jmetal.problems.CEC2015;

import jmetal.core.Solution;
import jmetal.core.SolutionType;
import jmetal.util.JMException;
import jmetal.util.wrapper.XReal;

/**
 * Implements Composition Function from CEC code.
 * 
 * @author inacio-medeiros
 *
 */
public abstract class CECCompositionFunction2 extends CECProblem {

	private FunctionsSuite FS;
	private int numberOfCompositionFunction, populationSize;
	private double Fi;
	private double[] bias;

	public CECCompositionFunction2(int numberOfVariables) {
		super(numberOfVariables);
	}

	public CECCompositionFunction2(int numberOfVariables, int populationSize) {
		super(numberOfVariables);
		this.populationSize = populationSize;
	}

	public CECCompositionFunction2(SolutionType solutionType) {
		super(solutionType);
	}

	@Override
	protected void problemBuild() {
		FS = new FunctionsSuite();
		defineParameters();
		// Sets name of Problem
		setName("Composition Function " + this.numberOfCompositionFunction);

		this.numberOfCompositionFunction += 8;

		readShiftedGlobalOptimum("shift_data_"
				+ this.numberOfCompositionFunction + ".txt");

		int nVars = getNumberOfVariables();

		String rotationMatrixFile = "M_" + this.numberOfCompositionFunction
				+ "_";
		String shuffleDataFile = "shuffle_data_"
				+ this.numberOfCompositionFunction + "_";

		if (nVars <= 2) {
			rotationMatrixFile += "D2";
		} else if (nVars <= 10) {
			rotationMatrixFile += "D10";
		} else if (nVars <= 30) {
			rotationMatrixFile += "D30";
		} else if (nVars <= 50) {
			rotationMatrixFile += "D50";
		} else {
			rotationMatrixFile += "D100";
		}

		if (nVars <= 10) {
			shuffleDataFile += "D10";
		} else if (nVars <= 30) {
			shuffleDataFile += "D30";
		} else if (nVars <= 50) {
			shuffleDataFile += "D50";
		} else {
			shuffleDataFile += "D100";
		}

		rotationMatrixFile += ".txt";
		shuffleDataFile += ".txt";

		readRotationMatrix(rotationMatrixFile);
		readShuffleData(shuffleDataFile);

	}

	protected abstract void defineParameters();

	@Override
	public void evaluate(Solution solution) throws JMException {
		double[] vector = new double[solution.numberOfVariables()];
		XReal values = new XReal(solution);

		for (int i = 0; i < vector.length; i++) {
			vector[i] = values.getValue(i);
		}

		double[] Os = getShiftedGlobalOptimum();
		double[] rotationMatrix = getRotationMatrix();
		int[] shuffleData = getShuffleData();

		double objective = 0;

		try {
			FS.test_func(Os, new double[2], vector.length, this.populationSize,
					getNumberOfCompositionFunction());
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
				
		double fitness = this.Fi;
		
		switch (this.numberOfCompositionFunction) {
		case 9:
			fitness += FS.cf01(vector, objective, vector.length, Os, rotationMatrix, bias, 1); 
			break;
		case 10:
			fitness += FS.cf02(vector, objective, vector.length, Os, rotationMatrix, shuffleData, bias, 1); 
			break;
		case 11:
			fitness += FS.cf03(vector, objective, vector.length, Os, rotationMatrix, bias, 1); 
			break;
		case 12:
			fitness += FS.cf04(vector, objective, vector.length, Os, rotationMatrix, bias, 1);
			break;
		case 13:
			fitness += FS.cf05(vector, objective, vector.length, Os, rotationMatrix, shuffleData, bias, 1);
			break;
		case 14:
			fitness += FS.cf06(vector, objective, vector.length, Os, rotationMatrix, bias, 1);
			break;
		case 15:
			fitness += FS.cf07(vector, objective, vector.length, Os, rotationMatrix, bias, 1);
			break;
		default:
			fitness = Double.POSITIVE_INFINITY;
			break;
		}
		
		solution.setObjective(0, fitness);

	}

	public void setnumberOfCompositionFunction(int numberOfCompositionFunction) {
		this.numberOfCompositionFunction = numberOfCompositionFunction;
	}

	public void setFi(double fi) {
		this.Fi = fi;
	}

	public void setPopulationSize(int populationSize) {
		this.populationSize = populationSize;
	}

	public FunctionsSuite getFS() {
		return FS;
	}

	public int getNumberOfCompositionFunction() {
		return numberOfCompositionFunction;
	}

	public int getPopulationSize() {
		return populationSize;
	}

	public double getFi() {
		return Fi;
	}

	public double[] getBias() {
		return bias;
	}

	public void setFS(FunctionsSuite fS) {
		FS = fS;
	}

	public void setNumberOfCompositionFunction(int numberOfCompositionFunction) {
		this.numberOfCompositionFunction = numberOfCompositionFunction;
	}

	public void setBias(double[] bias) {
		this.bias = bias;
	}

}
