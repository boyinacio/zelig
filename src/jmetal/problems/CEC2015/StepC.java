package jmetal.problems.CEC2015;

import java.io.IOException;
import java.util.Comparator;
import java.util.HashMap;

import jmetal.core.Algorithm;
import jmetal.core.Operator;
import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.core.SolutionSet;
import jmetal.metaheuristics.fireworks.Fireworks;
import jmetal.operators.mutation.FireworksExplosion;
import jmetal.operators.mutation.FireworksGaussian;
import jmetal.operators.selection.FireworksSelectionStrategy;
import jmetal.operators.selection.Selection;
import jmetal.problems.CEC2015.benchmarkFunctions.HybridFunction2;
import jmetal.problems.CEC2015.benchmarkFunctions.UnimodalFunction1;
import jmetal.util.JMException;
import jmetal.util.comparators.ObjectiveComparator;

public class StepC {

	public static void main(String[] args) throws JMException,
			SecurityException, IOException, ClassNotFoundException {
		Problem problem; // The problem to solve
		Algorithm algorithm;

		int[] D = {10,30,50,100};
		long[] times = new long[D.length];
		 
		int iter = 0;
		for(Integer d: D){
			problem = new UnimodalFunction1(d);

			algorithm = new Fireworks(problem);
			configureAlgorithm(algorithm, 200000);
			
			// Execute the Algorithm
			long initTime = System.currentTimeMillis();
			SolutionSet population = algorithm.execute();
			times[iter] = System.currentTimeMillis() - initTime;
			
			//long estimatedTime = System.currentTimeMillis() - initTime;
			
			//times[iter] = estimatedTime;

			//System.out.println("Dimension " + d + " - T2: " + estimatedTime + " milisseconds");
			
			iter++;
			
		}
		
		for (int i = 0; i < times.length; i++) {
			System.out.println("Dimension " + D[i] + " - T2: " + times[i] + " milisseconds");
		}
		
	}
	
	public static void configureAlgorithm(Algorithm algorithm, int maxGenerations){
		Selection selection; // Selection operator
		Operator explosion;
		Operator gaussian;
		Comparator comparator = new ObjectiveComparator(0);
		
		int numGaussianSparks = 5;
		double maxAmplitude = 40.0;
		int mParameter = 50;
		int popSize = 1;

		double aBound = 0.04;
		double bBound = 0.8;

		// passing parameters to Algorithm
		algorithm.setInputParameter("populationSize", popSize);
		algorithm.setInputParameter("maxGenerations", maxGenerations);
		algorithm.setInputParameter("numGaussianSparks", numGaussianSparks);
		algorithm.setInputParameter("mParameter", mParameter);
		algorithm.setInputParameter("comparator", comparator);
		algorithm.setInputParameter("eps", 1e-38);

		HashMap<String, Object> parameters = new HashMap<String, Object>();

		// passing paramters to the operators
		parameters.put("numGaussianSparks", numGaussianSparks);
		parameters.put("maxAmplitude", maxAmplitude);

		parameters.put("aBound", aBound);
		parameters.put("bBound", bBound);
		parameters.put("mParameter", mParameter); // maxSparks

		parameters.put("populationSize", popSize);
		parameters.put("comparator", comparator);
		parameters.put("eps", 1e-38);

		parameters.put("comparator", comparator);

		explosion = new FireworksExplosion(parameters);
		gaussian = new FireworksGaussian(null);

		parameters = new HashMap<String, Object>();
		parameters.put("comparator", comparator);
		selection = new FireworksSelectionStrategy(parameters);

		algorithm.addOperator("selection", selection);
		algorithm.addOperator("explosion", explosion);
		algorithm.addOperator("gaussian", gaussian);
	}

}
