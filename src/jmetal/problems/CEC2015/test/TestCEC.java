package jmetal.problems.CEC2015.test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class TestCEC {

	public static void main(String[] args) {
		Scanner reader = null;
		File F;
				
		try {
			F = new File("cec2015_input_data/shift_data_1.txt");
			reader = new Scanner(F);
		} catch (FileNotFoundException e) {
			System.err.println("Error opening file.");
			System.exit(1);
		}		// TODO Auto-generated method stub
		
		while(reader.hasNext()){
			System.out.println(Double.parseDouble(reader.next()));
		}
		
		reader.close();

	}

}
