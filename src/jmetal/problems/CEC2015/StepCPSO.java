package jmetal.problems.CEC2015;

import java.io.IOException;
import java.util.HashMap;

import jmetal.core.Algorithm;
import jmetal.core.Operator;
import jmetal.core.Problem;
import jmetal.core.SolutionSet;
import jmetal.metaheuristics.singleObjective.particleSwarmOptimization.PSO;
import jmetal.operators.mutation.MutationFactory;
import jmetal.problems.CEC2015.benchmarkFunctions.UnimodalFunction1;
import jmetal.util.JMException;

public class StepCPSO {

	public static void main(String[] args) throws JMException,
			SecurityException, IOException, ClassNotFoundException {
		Problem problem; // The problem to solve
		Algorithm algorithm;

		int[] D = { 10, 30, 50, 100 };
		long[] times = new long[D.length];

		int iter = 0;
		for (Integer d : D) {
			problem = new UnimodalFunction1(d);

			algorithm = new PSO(problem);
			configureAlgorithm(algorithm, 200000, problem);

			// Execute the Algorithm
			long initTime = System.currentTimeMillis();
			SolutionSet population = algorithm.execute();
			times[iter] = System.currentTimeMillis() - initTime;

			// long estimatedTime = System.currentTimeMillis() - initTime;

			// times[iter] = estimatedTime;

			// System.out.println("Dimension " + d + " - T2: " + estimatedTime +
			// " milisseconds");

			iter++;

		}

		for (int i = 0; i < times.length; i++) {
			System.out.println("Dimension " + D[i] + " - T2: " + times[i]
					+ " milisseconds");
		}

	}

	public static void configureAlgorithm(Algorithm algorithm,
			int maxGenerations, Problem problem) throws JMException {
		Operator mutation; // Mutation operator

		HashMap parameters;
		
		// Algorithm parameters
		algorithm.setInputParameter("swarmSize", 1);
		algorithm.setInputParameter("maxIterations", maxGenerations);

		parameters = new HashMap();
		parameters.put("probability", 1.0 / problem.getNumberOfVariables());
		parameters.put("distributionIndex", 20.0);
		mutation = MutationFactory.getMutationOperator("PolynomialMutation",
				parameters);

		algorithm.addOperator("mutation", mutation);

	}

}
