package jmetal.problems.CEC2015.benchmarkFunctions;

import jmetal.core.Solution;
import jmetal.core.SolutionType;
import jmetal.problems.CEC2015.CECCompositionFunction2;
import jmetal.util.JMException;
import jmetal.util.wrapper.XReal;

public class CompositionFunction5 extends CECCompositionFunction2 {

	public CompositionFunction5(int numberOfVariables) {
		super(numberOfVariables);
	}
	
	public CompositionFunction5(int numberOfVariables, int popSize) {
		super(numberOfVariables);
		setPopulationSize(popSize);
	}

	public CompositionFunction5(SolutionType solutionType) {
		super(solutionType);
	}

	@Override
	protected void defineParameters() {
		setFi(1300);
		setnumberOfCompositionFunction(5);
		
		double[] bias = {0+1300,100+1300,200+1300,300+1300,400+1300};
		setBias(bias);
	}

}
