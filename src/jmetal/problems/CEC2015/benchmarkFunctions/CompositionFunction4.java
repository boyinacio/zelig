package jmetal.problems.CEC2015.benchmarkFunctions;

import jmetal.core.Solution;
import jmetal.core.SolutionType;
import jmetal.problems.CEC2015.CECCompositionFunction2;
import jmetal.util.JMException;
import jmetal.util.wrapper.XReal;

public class CompositionFunction4 extends CECCompositionFunction2 {

	public CompositionFunction4(int numberOfVariables) {
		super(numberOfVariables);
	}
	
	public CompositionFunction4(int numberOfVariables, int popSize) {
		super(numberOfVariables);
		setPopulationSize(popSize);
	}

	public CompositionFunction4(SolutionType solutionType) {
		super(solutionType);
	}

	@Override
	protected void defineParameters() {
		setFi(1200);
		setnumberOfCompositionFunction(4);
		
		double[] bias = {0+1200,100+1200,100+1200,200+1200,200+1200};
		setBias(bias);
	}

	

}
