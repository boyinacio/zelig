package jmetal.problems.CEC2015.benchmarkFunctions;

import java.util.LinkedList;

import jmetal.core.Solution;
import jmetal.core.SolutionType;
import jmetal.problems.CEC2015.CECBasicFunction;
import jmetal.problems.CEC2015.CECProblem;
import jmetal.problems.CEC2015.basicFunctions.BF8;
import jmetal.util.JMException;
import jmetal.util.wrapper.XReal;

/**
 * Implements Multimodal Function 2 "Shifted and Rotated Rastrigin’s Function"
 * 
 * @author inacio-medeiros
 *
 */
public class MultimodalFunction2 extends CECProblem {

	private LinkedList<Double> list;
	private double Fi;

	public MultimodalFunction2(int numberOfVariables) {
		super(numberOfVariables);
	}

	public MultimodalFunction2(SolutionType solutionType) {
		super(solutionType);
	}

	// In this method we set values for attributes "name",
	// "functions", "shiftedGlobalOptimum" and "rotationMatrix", which are
	@Override
	protected void problemBuild() {
		readShiftedGlobalOptimum("shift_data_4.txt");

		int nVars = getNumberOfVariables();
		String rotationMatrixFile = "M_4_";

		if (nVars <= 4) {
			rotationMatrixFile += "D2";
		} else if (nVars <= 100) {
			rotationMatrixFile += "D10";
		} else if (nVars <= 900) {
			rotationMatrixFile += "D30";
		} else if (nVars <= 2500) {
			rotationMatrixFile += "D50";
		} else {
			rotationMatrixFile += "D100";
		}

		rotationMatrixFile += ".txt";

		readRotationMatrix(rotationMatrixFile);

		setName("Shifted and Rotated Rastrigin’s Function"); // Sets name of
											// Problem
		addFunction(new BF8()); // This function deals only with BF1

		this.Fi = 400;
	}

	@Override
	public void evaluate(Solution solution) throws JMException {
		CECBasicFunction BF8 = getFunction(0); // Gets 1st function from list of
												// BF Functions. In this case,
												// BF1

		XReal vector = new XReal(solution); // double vector from solution

		double[] shiftedGO = getShiftedGlobalOptimum(); // ShiftedGlobalOptimum

		// Makes x - o_i operation
		// WARNING: this method change inner solution values, so, after fitness
		// calculation, this subtraction is "undone", for recovering original
		// solution values (same is done with rotation)
		for (int i = 0; i < vector.size(); i++) {
			vector.setValue(i, vector.getValue(i) - shiftedGO[i]);
			vector.setValue(i, (vector.getValue(i) * 5.12)/100);
		}

		doRotation(vector);

		solution.setObjective(0, BF8.calculate(vector) + Fi);
		
		undoRotation(vector);

		for (int i = 0; i < vector.size(); i++) {
			vector.setValue(i, vector.getValue(i) + shiftedGO[i]);
			vector.setValue(i, (vector.getValue(i) * 100)/5.12);
		}
	
	}

}
