package jmetal.problems.CEC2015.benchmarkFunctions;

import jmetal.core.Solution;
import jmetal.core.SolutionType;
import jmetal.problems.CEC2015.CECCompositionFunction2;
import jmetal.util.JMException;
import jmetal.util.wrapper.XReal;

public class CompositionFunction2 extends CECCompositionFunction2 {

	public CompositionFunction2(int numberOfVariables) {
		super(numberOfVariables);
	}
	
	public CompositionFunction2(int numberOfVariables, int popSize) {
		super(numberOfVariables);
		setPopulationSize(popSize);
	}

	public CompositionFunction2(SolutionType solutionType) {
		super(solutionType);
	}

	@Override
	protected void defineParameters() {
		setFi(1000);
		setnumberOfCompositionFunction(2);
		
		double[] bias = {0+1000,100+1000,200+1000};
		setBias(bias);
	}

	

}
