package jmetal.problems.CEC2015.benchmarkFunctions;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.Scanner;

import jmetal.core.Solution;
import jmetal.core.SolutionType;
import jmetal.problems.CEC2015.CECBasicFunction;
import jmetal.problems.CEC2015.CECProblem;
import jmetal.problems.CEC2015.basicFunctions.BF1;
import jmetal.util.JMException;
import jmetal.util.wrapper.XReal;

/**
 * Implements Unimodal Function 1 "Rotated High Conditioned Elliptic Function"
 * 
 * @author inacio-medeiros
 *
 */
public class UnimodalFunction1 extends CECProblem {

	private LinkedList<Double> list;
	private double Fi;

	public UnimodalFunction1(int numberOfVariables) {
		super(numberOfVariables);
	}

	public UnimodalFunction1(SolutionType solutionType) {
		super(solutionType);
	}

	// In this method we set values for attributes "name",
	// "functions", "shiftedGlobalOptimum" and "rotationMatrix", which are
	@Override
	protected void problemBuild() {
		readShiftedGlobalOptimum("shift_data_1.txt");

		int nVars = getNumberOfVariables();
		String rotationMatrixFile = "M_1_";

		if (nVars <= 4) {
			rotationMatrixFile += "D2";
		} else if (nVars <= 100) {
			rotationMatrixFile += "D10";
		} else if (nVars <= 900) {
			rotationMatrixFile += "D30";
		} else if (nVars <= 2500) {
			rotationMatrixFile += "D50";
		} else {
			rotationMatrixFile += "D100";
		}

		rotationMatrixFile += ".txt";

		readRotationMatrix(rotationMatrixFile);

		setName("Rotated High Conditioned Elliptic Function"); // Sets name of
																// Problem
		addFunction(new BF1()); // This function deals only with BF1

		this.Fi = 100;
	}

	@Override
	public void evaluate(Solution solution) throws JMException {
		CECBasicFunction BF1 = getFunction(0); // Gets 1st function from list of
												// BF Functions. In this case,
												// BF1

		XReal vector = new XReal(solution); // double vector from solution

		double[] shiftedGO = getShiftedGlobalOptimum(); // ShiftedGlobalOptimum

		// Makes x - o_i operation
		// WARNING: this method change inner solution values, so, after fitness
		// calculation, this subtraction is "undone", for recovering original
		// solution values (same is done with rotation)
		for (int i = 0; i < vector.size(); i++) {
			vector.setValue(i, vector.getValue(i) - shiftedGO[i]);
		}

		doRotation(vector);

		solution.setObjective(0, BF1.calculate(vector) + Fi);

		undoRotation(vector);
		
		for (int i = 0; i < vector.size(); i++) {
			vector.setValue(i, vector.getValue(i) + shiftedGO[i]);
		}

		

	}

}
