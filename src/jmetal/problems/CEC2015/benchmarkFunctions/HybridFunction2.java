package jmetal.problems.CEC2015.benchmarkFunctions;

import jmetal.core.SolutionType;
import jmetal.problems.CEC2015.CECHybridFunction;

/**
 * Implements HybridFunction2
 * @author inacio-medeiros
 *
 */
public class HybridFunction2 extends CECHybridFunction {


	private static final long serialVersionUID = 1L;
	private int popSize;
	
	public HybridFunction2(int numberOfVariables) {
		super(numberOfVariables);
	}
	
	public HybridFunction2(int numberOfVariables, int populationSize) {
		super(numberOfVariables);
		this.popSize = populationSize;
	}
	
	public HybridFunction2(SolutionType solutionType) {
		super(solutionType);
	}

	@Override
	protected void defineParameters() {
		setNumberOfHybridFunction(2);
		setPopulationSize(popSize);
		setFi(700);
	}

}
