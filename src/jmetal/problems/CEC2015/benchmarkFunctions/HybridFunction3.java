package jmetal.problems.CEC2015.benchmarkFunctions;

import jmetal.core.SolutionType;
import jmetal.problems.CEC2015.CECHybridFunction;

/**
 * Implements HybridFunction3
 * @author inacio-medeiros
 *
 */
public class HybridFunction3 extends CECHybridFunction {


	private static final long serialVersionUID = 1L;
	private int popSize;
	
	public HybridFunction3(int numberOfVariables) {
		super(numberOfVariables);
	}
	
	public HybridFunction3(int numberOfVariables, int populationSize) {
		super(numberOfVariables);
		this.popSize = populationSize;
	}
	
	public HybridFunction3(SolutionType solutionType) {
		super(solutionType);
	}

	@Override
	protected void defineParameters() {
		setNumberOfHybridFunction(3);
		setPopulationSize(popSize);
		setFi(800);
	}

}
