package jmetal.problems.CEC2015.benchmarkFunctions;

import jmetal.core.Solution;
import jmetal.core.SolutionType;
import jmetal.problems.CEC2015.CECCompositionFunction2;
import jmetal.util.JMException;
import jmetal.util.wrapper.XReal;

public class CompositionFunction1 extends CECCompositionFunction2 {

	public CompositionFunction1(int numberOfVariables) {
		super(numberOfVariables);
	}
	
	public CompositionFunction1(int numberOfVariables, int popSize) {
		super(numberOfVariables);
		setPopulationSize(popSize);
	}

	public CompositionFunction1(SolutionType solutionType) {
		super(solutionType);
	}

	@Override
	protected void defineParameters() {
		setFi(900);
		setnumberOfCompositionFunction(1);
		
		double[] bias = {0+900,100+900,200+900};
		setBias(bias);
	}

	

}
