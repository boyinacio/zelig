package jmetal.problems.CEC2015.benchmarkFunctions;

import jmetal.core.Solution;
import jmetal.core.SolutionType;
import jmetal.problems.CEC2015.CECCompositionFunction2;
import jmetal.util.JMException;
import jmetal.util.wrapper.XReal;

public class CompositionFunction7 extends CECCompositionFunction2 {

	public CompositionFunction7(int numberOfVariables) {
		super(numberOfVariables);
	}

	public CompositionFunction7(int numberOfVariables, int popSize) {
		super(numberOfVariables);
		setPopulationSize(popSize);
	}

	public CompositionFunction7(SolutionType solutionType) {
		super(solutionType);
	}

	@Override
	protected void defineParameters() {
		setFi(1500);
		setnumberOfCompositionFunction(7);

		double[] bias = { 0 + 1500, 100 + 1500, 100 + 1500, 200 + 1500,
				200 + 1500, 300 + 1500, 300 + 1500, 400 + 1500, 400 + 1500,
				500 + 1500 };
		setBias(bias);
	}

}
