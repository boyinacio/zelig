package jmetal.problems.CEC2015.benchmarkFunctions;

import jmetal.core.Solution;
import jmetal.core.SolutionType;
import jmetal.problems.CEC2015.CECCompositionFunction2;
import jmetal.util.JMException;
import jmetal.util.wrapper.XReal;

public class CompositionFunction3 extends CECCompositionFunction2 {

	public CompositionFunction3(int numberOfVariables) {
		super(numberOfVariables);
	}
	
	public CompositionFunction3(int numberOfVariables, int popSize) {
		super(numberOfVariables);
		setPopulationSize(popSize);
	}

	public CompositionFunction3(SolutionType solutionType) {
		super(solutionType);
	}

	@Override
	protected void defineParameters() {
		setFi(1100);
		setnumberOfCompositionFunction(3);
		
		double[] bias = {0+1100,100+1100,200+1100,300+1100,400+1100};
		setBias(bias);
	}

	

}
