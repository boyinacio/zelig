package jmetal.problems.CEC2015.benchmarkFunctions;

import jmetal.core.SolutionType;
import jmetal.problems.CEC2015.CECHybridFunction;

/**
 * Implements HybridFunction1
 * @author inacio-medeiros
 *
 */
public class HybridFunction1 extends CECHybridFunction {


	private static final long serialVersionUID = 1L;
	private int popSize;
	
	public HybridFunction1(int numberOfVariables) {
		super(numberOfVariables);
	}
	
	public HybridFunction1(int numberOfVariables, int populationSize) {
		super(numberOfVariables);
		this.popSize = populationSize;
	}
	
	public HybridFunction1(SolutionType solutionType) {
		super(solutionType);
	}

	@Override
	protected void defineParameters() {
		setNumberOfHybridFunction(1);
		setPopulationSize(popSize);
		setFi(600);
	}

}
