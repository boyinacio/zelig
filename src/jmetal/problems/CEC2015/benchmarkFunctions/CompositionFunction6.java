package jmetal.problems.CEC2015.benchmarkFunctions;

import jmetal.core.Solution;
import jmetal.core.SolutionType;
import jmetal.problems.CEC2015.CECCompositionFunction2;
import jmetal.util.JMException;
import jmetal.util.wrapper.XReal;

public class CompositionFunction6 extends CECCompositionFunction2 {

	public CompositionFunction6(int numberOfVariables) {
		super(numberOfVariables);
	}

	public CompositionFunction6(int numberOfVariables, int popSize) {
		super(numberOfVariables);
		setPopulationSize(popSize);
	}

	public CompositionFunction6(SolutionType solutionType) {
		super(solutionType);
	}

	@Override
	protected void defineParameters() {
		setFi(1400);
		setnumberOfCompositionFunction(6);

		double[] bias = { 0 + 1400, 100 + 1400, 200 + 1400, 300 + 1400,
				300 + 1400, 400 + 1400, 400 + 1400 };
		setBias(bias);
	}

}
