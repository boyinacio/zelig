package jmetal.problems.CEC2015;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import jmetal.core.Problem;
import jmetal.core.SolutionType;
import jmetal.encodings.solutionType.RealSolutionType;
import jmetal.util.JMException;
import jmetal.util.wrapper.XReal;

public abstract class CECProblem extends Problem {

	private static final long serialVersionUID = -4687382878353623964L;
	private String name; // Specific problem name
	private List<CECBasicFunction> functions; // Once a "problem" deals with
												// different functions, they're
												// stored in a list
	private double[] shiftedGlobalOptimum;
	private double[] rotationMatrix;
	private int[] shuffleData;

	private String directory;

	/**
	 * Empty-constructor-like Initialize attributes as empty
	 *
	 * @param numberOfVariables
	 *            Size of "input vector" (a "candidate solution")
	 */
	public CECProblem(int numberOfVariables) {
		this.numberOfVariables_ = numberOfVariables;
		this.directory = "cec2015_input_data/";
		initializing();
		problemBuild();
		build();
	}

	public CECProblem(SolutionType solutionType) {
		super(solutionType);
		this.numberOfVariables_ = 0;
		initializing();
		problemBuild();
		build();
	}

	/**
	 * TODO Implement method that initialize values to attributes "name",
	 * "functions", "shiftedGlobalOptimum" and "rotationMatrix"
	 */
	protected abstract void problemBuild();

	/**
	 * Initializes attributes to be empty
	 */
	private void initializing() {
		// If user didn't set number of variables
		if (this.numberOfVariables_ == 0) {
			this.numberOfVariables_ = 3;
		}
		name = "";
		functions = new ArrayList<CECBasicFunction>();
	}

	/**
	 * Sets initial configuration for CEC Problems
	 */
	private void build() {
		numberOfObjectives_ = 1;
		numberOfConstraints_ = 0;
		problemName_ = "CEC2015_" + this.name;

		solutionType_ = new RealSolutionType(this);

		upperLimit_ = new double[numberOfVariables_];
		lowerLimit_ = new double[numberOfVariables_];

		for (int i = 0; i < numberOfVariables_; i++) {
			lowerLimit_[i] = -100;
			upperLimit_[i] = 100;
		} // for
	}

	protected void readShiftedGlobalOptimum(String path) {
		File F;
		Scanner reader = null;
		double value;
		LinkedList<Double> list = new LinkedList<Double>();

		// Path in which are values of shiftedGlobalOptimum vector
		String shiftedGlobalOptimumFilePath = this.directory + path;

		// Reads file
		try {
			F = new File(shiftedGlobalOptimumFilePath);
			reader = new Scanner(F);
		} catch (FileNotFoundException e) {
			System.err.println("Error opening file.");
			System.exit(1);
		}

		// Pass values to vector
		while (reader.hasNext()) {
			value = Double.parseDouble(reader.next());
			list.add(value);
		}

		reader.close();

		this.shiftedGlobalOptimum = new double[list.size()];

		for (int i = 0; i < this.shiftedGlobalOptimum.length; i++) {
			this.shiftedGlobalOptimum[i] = list.get(i);
		}
	}

		protected void readRotationMatrix(String path) {
		File F;
		Scanner reader = null;
		double value;
		LinkedList<Double> list = new LinkedList<Double>();

		// Path in which are values of shiftedGlobalOptimum vector
		String rotationMatrixFilePath = this.directory + path;

		// Reads file
		try {
			F = new File(rotationMatrixFilePath);
			reader = new Scanner(F);
		} catch (FileNotFoundException e) {
			System.err.println("Error opening file.");
			System.exit(1);
		}

		// Pass values to vector
		while (reader.hasNext()) {
			value = Double.parseDouble(reader.next());
			list.add(value);
		}

		reader.close();

		this.rotationMatrix = new double[list.size()];

		for (int i = 0; i < this.rotationMatrix.length; i++) {
			this.rotationMatrix[i] = list.get(i);
		}
	}

	protected void readShuffleData(String path){
		File F;
		Scanner reader = null;
		int value;
		LinkedList<Integer> list = new LinkedList<Integer>();

		// Path in which are values of shiftedGlobalOptimum vector
		String shuffleDataFilePath = this.directory + path;

		// Reads file
		try {
			F = new File(shuffleDataFilePath);
			reader = new Scanner(F);
		} catch (FileNotFoundException e) {
			System.err.println("Error opening file.");
			System.exit(1);
		}

		// Pass values to vector
		while (reader.hasNext()) {
			value = Integer.parseInt(reader.next());
			list.add(value);
		}

		reader.close();

		this.shuffleData = new int[list.size()];

		for (int i = 0; i < this.shuffleData.length; i++) {
			this.shuffleData[i] = list.get(i);
		}
	}

	protected void doRotation(XReal vector) throws JMException {
		double valueRotated;
		for (int i = 0; i < vector.size(); i++) {
			valueRotated = 0;
			for (int j = 0; j < vector.size(); j++) {
				valueRotated += vector.getValue(j) * rotationMatrix[j];
			}
			vector.setValue(i, valueRotated);
		}
	}

	protected void undoRotation(XReal vector) throws JMException {
		double valueRotated;
		for (int i = 0; i < vector.size(); i++) {
			valueRotated = 0;
			for (int j = 0; j < vector.size(); j++) {
				valueRotated -= vector.getValue(j) * rotationMatrix[j];
			}
			vector.setValue(i, valueRotated);
		}
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getName() {
		return name;
	}

	public List<CECBasicFunction> getFunctions() {
		return functions;
	}

	public double[] getRotationMatrix() {
		return rotationMatrix == null ? null
				: (double[]) rotationMatrix.clone();
	}

	public double[] getShiftedGlobalOptimum() {
		return shiftedGlobalOptimum == null ? null
				: (double[]) shiftedGlobalOptimum.clone();
	}

	public int[] getShuffleData() {
		return shuffleData == null ? null
				: (int[]) shuffleData.clone();
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setFunctions(List<CECBasicFunction> functions) {
		this.functions = functions;
	}

	public void setShiftedGlobalOptimum(double[] shiftedGlobalOptimum) {
		if (shiftedGlobalOptimum != null) {
			this.shiftedGlobalOptimum = shiftedGlobalOptimum;
		}
	}

	public void setShuffleData(int[] shuffleData) {
		if(shuffleData != null){
			this.shuffleData = shuffleData;
		}
	}

	public boolean addFunction(CECBasicFunction arg0) {
		return functions.add(arg0);
	}

	public boolean containsFunction(Object arg0) {
		return functions.contains(arg0);
	}

	public CECBasicFunction getFunction(int arg0) {
		return functions.get(arg0);
	}

	public int indexOfFunction(Object arg0) {
		return functions.indexOf(arg0);
	}

	public CECBasicFunction remove(int arg0) {
		return functions.remove(arg0);
	}

	public boolean removeAllFunctions(Collection<?> arg0) {
		return functions.removeAll(arg0);
	}

	public int quantityofFunctions() {
		return functions.size();
	}
}
