package jmetal.problems.CEC2015;

import java.util.List;

import jmetal.core.Solution;
import jmetal.core.SolutionType;
import jmetal.util.JMException;
import jmetal.util.wrapper.XReal;

public abstract class CECCompositionFunction extends CECProblem {

	private double[] weights; // normalized weight value for each g i (x). This
								// vector is calculated in evalute method

	// An CECCompositionFunction-inherited class must provide these parameters
	// in problemBuild method
	private double Fstar; // F*(i)
	private double[] lambda; // used to control each g i (x)’s height
	private double[] bias; // defines which optimum is global optimum
	private double[] sigma; // used to control each g i (x)’s coverage range, a
							// small sigma i give a narrow range for
							// that g i ( x )
	private boolean[] needsRotating; // arrays that says, for each function, if
									// it's necessary to make rotation

	public CECCompositionFunction(int numberOfVariables) {
		super(numberOfVariables);
	}

	public CECCompositionFunction(SolutionType solutionType) {
		super(solutionType);
	}

	@Override
	public void evaluate(Solution solution) throws JMException {
		XReal vars = new XReal(solution);
		List<CECBasicFunction> functions = getFunctions();
		weights = new double[functions.size()];
		double[] oi;

		// Calculates each weight
		double sum, totalWeightSum = 0;
		for (int w = 0; w < weights.length; w++) {
			sum = 0;
			
			readShiftedGlobalOptimum("shift_data_" + (w + 1) + ".txt");
			oi = getShiftedGlobalOptimum();

			for (int d = 0; d < vars.size(); d++) {
				sum += Math.pow(vars.getValue(d) - oi[d], 2);
			}

			weights[w] = (1 / Math.sqrt(sum))
					* Math.exp(-(sum / (2 * vars.size() * sigma[w] * sigma[w])));

			totalWeightSum += weights[w];
		}

		// Normalize each weight
		for (int w = 0; w < weights.length; w++) {
			weights[w] /= totalWeightSum;
		}

		// Calculate F(X)

		// Objective is set initially as Fstar
		double objective = Fstar;

		sum = 0; // Same as used long above

		// Note: number of functions is the same of number of lambdas and
		// weights
		
		double nVars = vars.size();
		for (int f = 0; f < functions.size(); f++) {
			if(needsRotating[f]){
				String rotationMatrixFile = "M_" + (f+1) + "_";

				if (nVars <= 4) {
					rotationMatrixFile += "D2";
				} else if (nVars <= 100) {
					rotationMatrixFile += "D10";
				} else if (nVars <= 900) {
					rotationMatrixFile += "D30";
				} else if (nVars <= 2500) {
					rotationMatrixFile += "D50";
				} else {
					rotationMatrixFile += "D100";
				}

				rotationMatrixFile += ".txt";

				readRotationMatrix(rotationMatrixFile);
				doRotation(vars);
			}
			
			sum += (weights[f] * ((lambda[f] * functions.get(f).calculate(vars)) + bias[f]));
			
			if(needsRotating[f]){
				undoRotation(vars);
			}
			
		}

		// Here is that big sum (from eq 21) with F*
		objective += sum;

		solution.setObjective(0, objective);
	}

	public void setWeights(double[] weights) {
		this.weights = weights;
	}

	public void setFstar(double fstar) {
		Fstar = fstar;
	}

	public void setLambda(double[] lambda) {
		this.lambda = lambda;
	}

	public void setBias(double[] bias) {
		this.bias = bias;
	}

	public void setSigma(double[] sigma) {
		this.sigma = sigma;
	}
	
	public void setNeedsRotating(boolean[] needsRotating){
		this.needsRotating = needsRotating;
	}

}
