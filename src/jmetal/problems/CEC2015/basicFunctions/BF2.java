package jmetal.problems.CEC2015.basicFunctions;

import jmetal.problems.CEC2015.CECBasicFunction;
import jmetal.util.JMException;
import jmetal.util.wrapper.XReal;

/**
 * Implements CEC2015 Basic Function 2 "Cigar Function"
 * 
 * @author inacio-medeiros
 *
 */
public class BF2 implements CECBasicFunction {

	public BF2() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public double calculate(XReal vector) throws JMException {
		int dimensions = vector.getNumberOfDecisionVariables();
		double x1 = Math.pow(vector.getValue(0), 2); // (x_1)^2
		double sum = 0;

		// Once vector range here is from 0 to n-1, dimension, which in original
		// function starts with 2, here starts with 1
		for (int dimension = 1; dimension < dimensions; dimension++) {
			sum += Math.pow(vector.getValue(dimension), 2);
		}
		
		sum *= BF1.million;
		sum += x1;

		return sum;
	}
}
