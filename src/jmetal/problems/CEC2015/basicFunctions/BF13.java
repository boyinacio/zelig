package jmetal.problems.CEC2015.basicFunctions;



import jmetal.problems.CEC2015.CECBasicFunction;
import jmetal.util.JMException;
import jmetal.util.wrapper.XReal;

/**
 * Implements CEC2015 Basic Function 13 "Expanded Griewank’s plus Rosenbrock’s Function"
 * 
 * @author Abdul Samih <ab630@hw.ac.uk>
 *
 */

public class BF13 implements CECBasicFunction{

	
	public BF13() {
	}

	@Override
	public double calculate(XReal vector) throws JMException {
		int dimensions = vector.getNumberOfDecisionVariables(); // retrieves
								        // number of
									// dimensions
									// from
									// input
									// vector
		double sum = 0; // accumulates values for summations in this function		
		double temp1 = 0.0, temp2 = 0.0, temp;
                
		    for (int var = 0; var < dimensions - 1; var++) {		         
		      temp1 = vector.getValue(var) * vector.getValue(var) - vector.getValue(var + 1);
		      temp2 = vector.getValue(var) - 1.0;
		      temp = 100.0 * temp1 * temp1 + temp2 * temp2;
		      sum += (temp * temp)/ 4000.0 - StrictMath.cos(temp) + 1.0;
		    }        
		      temp1 = vector.getValue(dimensions - 1) * vector.getValue(dimensions - 1) - vector.getValue(0);
		      temp2 = vector.getValue(dimensions - 1) - 1.0;
		      temp = 100.0 * temp1 * temp1 + temp2 * temp2;
		      sum += (temp * temp)/ 4000.0 - StrictMath.cos(temp) + 1.0;
		    	    
		    

		return sum;
	}


}
