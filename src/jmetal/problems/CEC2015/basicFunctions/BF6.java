package jmetal.problems.CEC2015.basicFunctions;

import jmetal.problems.CEC2015.CECBasicFunction;
import jmetal.util.JMException;
import jmetal.util.wrapper.XReal;


/**
 * Implements CEC2015 Basic Function 6 "Weierstrass Function"
 * 
 * @author inacio-medeiros
 *
 */
public class BF6 implements CECBasicFunction {

	// Constants used in equation
	private final static double a = 0.5, b = 3, kmax = 20;

	public BF6() {
	}

	@Override
	public double calculate(XReal vector) throws JMException {
		int dimensions = vector.getNumberOfDecisionVariables();

		double result = firstSummation(vector, dimensions)
				- (dimensions * secondSummation(vector, dimensions));

		return result;
	}

	// Calculates first summation from input vector
	private double firstSummation(XReal vector, int dimensions)
			throws JMException {
		double sum = 0;
		for (int dim = 0; dim < dimensions; dim++) {
			for (int k = 0; k <= kmax; k++) {
				sum += Math.pow(a, k)
						* Math.cos(2 * Math.PI * Math.pow(b, k)
								* (vector.getValue(dim) + 0.5));
			}
		}
		return sum;
	}

	// Calculates second summation from input vector
	private double secondSummation(XReal vector, int dimensions)
			throws JMException {
		double sum = 0;
		for (int k = 0; k <= kmax; k++) {
			sum += Math.pow(a, k)
					* Math.cos(2 * Math.PI * Math.pow(b, k) * 0.5);
		}
		return sum;
	}

}
