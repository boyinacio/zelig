package jmetal.problems.CEC2015.basicFunctions;

import jmetal.problems.CEC2015.CECBasicFunction;
import jmetal.util.JMException;
import jmetal.util.wrapper.XReal;

/**
 * Implements CEC2015 Basic Function 5 "Ackley’s Function"
 * 
 * @author inacio-medeiros
 *
 */
public class BF5 implements CECBasicFunction {

	public BF5() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public double calculate(XReal vector) throws JMException {
		int dimensions = vector.getNumberOfDecisionVariables();
		
		// Calculates argument for first call of exp(x) function in equation
		double firstExp = -0.2
				* Math.sqrt((1.0 / dimensions)
						* quadraticSummation(vector, dimensions));
		
		
		// Calculates argument for second call of exp(x) function in equation
		double secondExp = (1.0 / dimensions) * cosineSummation(vector, dimensions);
		
		// "Main equation"
		double result = (-20 * Math.exp(firstExp)) - Math.exp(secondExp) + 20 + Math.E;  

		return result;
	}

	// Sums up all (x_i)^2 from input vector
	private double quadraticSummation(XReal vector, int dimensions)
			throws JMException {
		double sum = 0;

		for (int dim = 0; dim < dimensions; dim++) {
			sum += Math.pow(vector.getValue(dim), 2);
		}

		return sum;
	}

	// Sums up all cos(2*PI*x_i) from input vector
	private double cosineSummation(XReal vector, int dimensions)
			throws JMException {
		double sum = 0;

		for (int dim = 0; dim < dimensions; dim++) {
			sum += Math.cos(2 * Math.PI * vector.getValue(dim));
		}

		return sum;
	}

}
