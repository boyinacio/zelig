package jmetal.problems.CEC2015.basicFunctions;

import jmetal.problems.CEC2015.CECBasicFunction;
import jmetal.util.JMException;
import jmetal.util.wrapper.XReal;

/**
 * Implements CEC2015 Basic Function 7 "Griewank’s Function"
 * 
 * @author inacio-medeiros
 *
 */
public class BF7 implements CECBasicFunction {

	public BF7() {
		// TODO Auto-generated constructor stub
	}

	// Once JMetal just has a code for calculating this function, it's gonna be
	// reused here with necessary adaptations.
	@Override
	public double calculate(XReal vector) throws JMException {
		int dimensions = vector.getNumberOfDecisionVariables();

		double sum = 0.0;
		double mult = 1.0;
		double d = 4000.0;
		for (int var = 0; var < dimensions; var++) {
			sum += vector.getValue(var) * vector.getValue(var);
			mult *= Math.cos(vector.getValue(var) / Math.sqrt(var + 1));
		}
		
		double result = 1.0/d * sum - mult + 1.0;

		return result;
	}
}
