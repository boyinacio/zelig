package jmetal.problems.CEC2015.basicFunctions;

import jmetal.problems.CEC2015.CECBasicFunction;
import jmetal.util.JMException;
import jmetal.util.wrapper.XReal;

/**
 * Implements CEC2015 Basic Function 12 "HGBatFunction"
 * 
 * @author inacio-medeiros
 *
 */
public class BF12 implements CECBasicFunction {

	public BF12() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public double calculate(XReal vector) throws JMException {
		int dimensions = vector.getNumberOfDecisionVariables();

		double quadSum = 0; // Sum((x_i)^2) from i=1 to D
		double normalSum = 0; // Sum(x_i) from i=1 to D
		for (int dimension = 0; dimension < dimensions; dimension++) {
			quadSum += Math.pow(vector.getValue(dimension), 2);
			normalSum += vector.getValue(dimension);
		}

		double firstTerm = Math.pow(
				Math.abs(Math.pow(quadSum, 2) - Math.pow(normalSum, 2)), 0.5);
		
		double secondTerm = ((0.5 * quadSum) + normalSum) / dimensions;
		
		double result = firstTerm + secondTerm + 0.5;

		return result;
	}
}
