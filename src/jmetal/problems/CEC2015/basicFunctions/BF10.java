package jmetal.problems.CEC2015.basicFunctions;

import jmetal.problems.CEC2015.CECBasicFunction;
import jmetal.util.JMException;
import jmetal.util.wrapper.XReal;

/**
 * Implements CEC2015 Basic Function 9 "Katsuura Function"
 * 
 * @author inacio-medeiros
 *
 */
public class BF10 implements CECBasicFunction {

	public BF10() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public double calculate(XReal vector) throws JMException {
		int dimensions = vector.getNumberOfDecisionVariables();
		
		double quocient = 10 / Math.pow(dimensions, 2); // 10 / D^2 
				
		double product = 1;
		
		for (int i = 0; i < dimensions; i++) {
			double summation = 0;
			double mult = 0;
			for (int j = 1; j <= 32; j++) {
				mult = Math.pow(2,j) * vector.getValue(i); // 2^j * x_i
				summation += ((mult - Math.round(mult)) / Math.pow(2, j));
			}
			
			product *= Math.pow(1 + ((i+1) * summation), 10 / (Math.pow(dimensions, 1.2))); 
		}
		
		double result = (quocient * product) - quocient;
				
		return result;
	}

}
