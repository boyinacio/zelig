package jmetal.problems.CEC2015.basicFunctions;

import jmetal.problems.CEC2015.CECBasicFunction;
import jmetal.util.JMException;
import jmetal.util.wrapper.XReal;

/**
 * Implements CEC2015 Basic Function 8 "Rastrigin’s Function"
 * 
 * @author inacio-medeiros
 *
 */
public class BF8 implements CECBasicFunction {

	public BF8() {
		// TODO Auto-generated constructor stub
	}

	// Once JMetal just has a code for calculating this function, it's gonna be
	// reused here with necessary adaptations.
	@Override
	public double calculate(XReal vector) throws JMException {
		int dimensions = vector.getNumberOfDecisionVariables();

		double result = 0.0;
		double a = 10.0;
		double w = 2 * Math.PI;

		for (int i = 0; i < dimensions; i++) {
			result += vector.getValue(i) * vector.getValue(i) - a
					* Math.cos(w * vector.getValue(i));
		}
		result += a * dimensions;

		return result;
	}
}
