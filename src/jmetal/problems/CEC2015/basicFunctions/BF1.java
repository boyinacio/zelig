package jmetal.problems.CEC2015.basicFunctions;

import jmetal.problems.CEC2015.CECBasicFunction;
import jmetal.util.JMException;
import jmetal.util.wrapper.XReal;

/**
 * Implements CEC2015 Basic Function 1 "High Conditioned Elliptic Function"
 * 
 * @author inacio-medeiros
 *
 */
public class BF1 implements CECBasicFunction {

	public final static double million = Math.pow(10, 6); // One millioin (10^6)

	public BF1() {
	}

	@Override
	public double calculate(XReal vector) throws JMException {
		int dimensions = vector.getNumberOfDecisionVariables(); // retrieves
																// number of
																// dimensions
																// from
																// input
																// vector
		double sum = 0; // accumulates values for summations in this function
		double product;
		double quocient; // calculates quocient from expoent

		// This "dimension+1" is because in original function is shown "i-1" in
		// exponent,
		// so for keeping "correcting calculation" (once it's used from 1 to n
		// in original function, it's used "dimension+1"
		for (int dimension = 0; dimension < dimensions; dimension++) {
			quocient = (double) ((dimension + 1) - 1) / (dimensions - 1);
			product = Math.pow(BF1.million, quocient);
			product *= Math.pow(vector.getValue(dimension), 2);
			sum += product;
		}

		return sum;
	}

}
