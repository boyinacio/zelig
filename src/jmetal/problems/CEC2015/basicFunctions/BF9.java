package jmetal.problems.CEC2015.basicFunctions;

import jmetal.problems.CEC2015.CECBasicFunction;
import jmetal.util.JMException;
import jmetal.util.wrapper.XReal;

/**
 * Implements CEC2015 Basic Function 9 "Modified Schwefel’s Function"
 * 
 * @author inacio-medeiros
 *
 */
public class BF9 implements CECBasicFunction {

	private final static double constant = 4.209687462275036e+002;

	public BF9() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public double calculate(XReal vector) throws JMException {
		int dimensions = vector.getNumberOfDecisionVariables();
		
		double sum = 0;
		for (int dimension = 0; dimension < dimensions; dimension++) {
			sum += g(vector.getValue(dimension),dimensions);
		}
		
		double result = (418.9829 * dimensions) - sum;
		
		return result;
	}

	// for calculating g(z_i) function
	private double g(double value, int dimensions) {
		value += constant; // turns value into "z_i"

		double result = 0;

		if (Math.abs(value) <= 500) {
			result = value * Math.sin(Math.sqrt(Math.abs(value)));
		} else if (value > 500) {
			double mod = value % 500;
			double subtraction = 500 - mod; // 500 - (mod(z_i,500))
			double quadSubtraction = Math.pow(value - 500, 2); // (z_i - 500)^2
			double sinOfSqrt = Math.sin(Math.sqrt(Math.abs(subtraction))); // sin(sqrt(500
																			// -
																			// mod(z,500)))

			result = (subtraction * sinOfSqrt)
					- (quadSubtraction / (10000 * dimensions));

		} else if (value < -500) {
			double subtraction = (Math.abs(value) % 500) - 500; // mod(|z_i|,500) - 500 
			double quadSum = Math.pow(value + 500, 2); // (z_i + 500)^2
			double sinOfSqrt = Math.sin(Math.sqrt(Math.abs(subtraction)));
			
			result = (subtraction * sinOfSqrt)
					- (quadSum / (10000 * dimensions));

		}

		return result;
	}

}
