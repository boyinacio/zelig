package jmetal.problems.CEC2015.basicFunctions;

import jmetal.problems.CEC2015.CECBasicFunction;
import jmetal.util.JMException;
import jmetal.util.wrapper.XReal;

/**
 * Implements CEC2015 Basic Function 3 "Discus Function"
 * 
 * @author inacio-medeiros
 *
 */
public class BF3 implements CECBasicFunction {

	public BF3() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public double calculate(XReal vector) throws JMException {
		int dimensions = vector.getNumberOfDecisionVariables();
		double term = BF1.million * Math.pow(vector.getValue(0), 2); // 10^6 * (x_1)^2
		double sum = 0;

		// Once vector range here is from 0 to n-1, dimension, which in original
		// function starts with 2, here starts with 1
		for (int dimension = 1; dimension < dimensions; dimension++) {
			sum += Math.pow(vector.getValue(dimension), 2);
		}
		
		sum += term;

		return sum;
	}

}
