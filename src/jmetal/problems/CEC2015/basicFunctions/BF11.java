package jmetal.problems.CEC2015.basicFunctions;

import jmetal.problems.CEC2015.CECBasicFunction;
import jmetal.util.JMException;
import jmetal.util.wrapper.XReal;

/**
 * Implements CEC2015 Basic Function 11 "HappyCat Function"
 * 
 * @author inacio-medeiros
 *
 */
public class BF11 implements CECBasicFunction {

	public BF11() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public double calculate(XReal vector) throws JMException {
		int dimensions = vector.getNumberOfDecisionVariables();
						
		double sum = 0; // Sum((x_i)^2 - D) from i=1 to D
		double quadSum = 0; // Sum((x_i)^2) from i=1 to D
		double normalSum = 0; // Sum(x_i) from i=1 to D
		for (int dimension = 0; dimension < dimensions; dimension++) {
			sum += (Math.pow(vector.getValue(dimension), 2) - dimensions);
			quadSum += Math.pow(vector.getValue(dimension), 2);
			normalSum += vector.getValue(dimension);
		}
		
		sum = Math.pow(Math.abs(sum), 0.25); // |Sum(...)|^(1/4)
		quadSum *= 0.5;
		
		double result = sum + ((quadSum + normalSum) / dimensions) + 0.5;
		
		return result;
	}

}
