package jmetal.problems.CEC2015.basicFunctions;

import jmetal.problems.CEC2015.CECBasicFunction;
import jmetal.util.JMException;
import jmetal.util.wrapper.XReal;

/**
 * Implements CEC2015 Basic Function 14 "Expanded Scaffer’s F6 Function"
 * 
 * @author inacio-medeiros
 *
 */
public class BF14 implements CECBasicFunction {

	public BF14() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public double calculate(XReal vector) throws JMException {
		int dimensions = vector.getNumberOfDecisionVariables();
		
		double sum = 0;
		double xValue, yValue;
		for (int x = 0, y = 1; x < dimensions; x++, y++) {
			if(y == dimensions){
				y = 0;
			}
			
			xValue = vector.getValue(x);
			yValue = vector.getValue(y);
			
			sum += g(xValue,yValue); // g(x_0,x_1) [x_1,x_2], ..., g(x_d-1,x_1) [x_d,x_1] 
		}
		
		return sum;
	}

	private double g(double x, double y) {
		double sqrtQuadSum = Math.sqrt((x * x) + (y * y));

		double firstTerm = Math.pow(Math.sin(sqrtQuadSum), 2) - 0.5; // sin²(sqrt(x²
																		// +
																		// y²))
																		// - 0.5
		// For sqrt(x² + y²) become x² + y² 
		sqrtQuadSum = Math.pow(sqrtQuadSum, 2);
		
		double secondTerm = 1 + (0.001 * sqrtQuadSum);
		secondTerm = Math.pow(secondTerm,2); // (1 + (0.001 * (x² + y²))²
		
		double result = 0.5 + (firstTerm / secondTerm);

		return result;
	}

}
