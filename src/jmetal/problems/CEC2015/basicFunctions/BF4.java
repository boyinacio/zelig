package jmetal.problems.CEC2015.basicFunctions;

import jmetal.problems.CEC2015.CECBasicFunction;
import jmetal.util.JMException;
import jmetal.util.wrapper.XReal;

/**
 * Implements CEC2015 Basic Function 4 "Rosenbrock’s Function"
 * 
 * @author inacio-medeiros
 *
 */
public class BF4 implements CECBasicFunction {

	public BF4() {
		// TODO Auto-generated constructor stub
	}

	// Once JMetal just has a code for calculating this function, it's gonna be
	// reused here with necessary adaptations.
	@Override
	public double calculate(XReal vector) throws JMException {
		double sum = 0.0;
		int dimensions = vector.getNumberOfDecisionVariables();

		double[] x = new double[dimensions];

		for (int i = 0; i < dimensions; i++) {
			x[i] = vector.getValue(i);
		}

		for (int i = 0; i < dimensions - 1; i++) {
			sum += 100.0 * (x[i + 1] - x[i] * x[i]) * (x[i + 1] - x[i] * x[i])
					+ (x[i] - 1) * (x[i] - 1);
		}
		
		return sum;
	}

}
