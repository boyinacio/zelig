package jmetal.problems.CEC2015;

import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.problems.CEC2015.benchmarkFunctions.UnimodalFunction1;
import jmetal.util.JMException;

public class StepB {

	public static void main(String[] args) throws JMException, ClassNotFoundException {
		int[] D = {10,30,50,100};
		long startTime, endTime;
		Problem P;
		Solution S;
		
		for(Integer d: D){
			P = new UnimodalFunction1(d);
			
			startTime = System.currentTimeMillis();
			for (int evaluation = 0; evaluation < 200000; evaluation++) {
				S = new Solution(P);
				P.evaluate(S);
			}
			endTime = System.currentTimeMillis();
			
			System.out.println("Dimension " + d + " - T1: " + (endTime - startTime) + " milisseconds");
			
		}

	}

}
