package jmetal.problems.CEC2015;

import jmetal.core.Solution;
import jmetal.core.SolutionType;
import jmetal.problems.CEC2015.basicFunctions.BF2;
import jmetal.util.JMException;
import jmetal.util.wrapper.XReal;

/**
 * Implements an Abstract Hybrid Function class Once it uses CEC code, inherited
 * classes of it just need to inform which hybrid function will be used (1, 2 or
 * 3), Fi, and populationSize
 * 
 * @author inacio-medeiros
 *
 */
public abstract class CECHybridFunction extends CECProblem {

	private FunctionsSuite FS;
	private int numberOfHybridFunction, populationSize;
	private double Fi;

	public CECHybridFunction(int numberOfVariables) {
		super(numberOfVariables);
	}

	public CECHybridFunction(SolutionType solutionType) {
		super(solutionType);
	}

	@Override
	protected void problemBuild() {
		FS = new FunctionsSuite();
		defineParameters();
		// Sets name of Problem
		setName("Hybrid Function " + this.numberOfHybridFunction);

		// Once Hybrid Functions, in the list of hybrid functions, are 6th, 7th,
		// 8th, and so are referred in this way in files, number of hybrid
		// function is increased in 5 units to fit between 6 and 8
		this.numberOfHybridFunction += 5;

		readShiftedGlobalOptimum("shift_data_" + this.numberOfHybridFunction
				+ ".txt");

		int nVars = getNumberOfVariables();

		String rotationMatrixFile = "M_" + this.numberOfHybridFunction + "_";
		String shuffleDataFile = "shuffle_data_" + this.numberOfHybridFunction
				+ "_";

		if (nVars <= 2) {
			rotationMatrixFile += "D2";
		} else if (nVars <= 10) {
			rotationMatrixFile += "D10";
		} else if (nVars <= 30) {
			rotationMatrixFile += "D30";
		} else if (nVars <= 50) {
			rotationMatrixFile += "D50";
		} else {
			rotationMatrixFile += "D100";
		}

		if (nVars <= 10) {
			shuffleDataFile += "D10";
		} else if (nVars <= 30) {
			shuffleDataFile += "D30";
		} else if (nVars <= 50) {
			shuffleDataFile += "D50";
		} else {
			shuffleDataFile += "D100";
		}

		rotationMatrixFile += ".txt";
		shuffleDataFile += ".txt";

		readRotationMatrix(rotationMatrixFile);
		readShuffleData(shuffleDataFile);

	}

	/**
	 * In this method, user informs number of Hybrid Function used and Fi
	 */
	protected abstract void defineParameters();

	@Override
	public void evaluate(Solution solution) throws JMException {
		solution.setObjective(0, calculate(solution));
	}
	
	public double calculate(Solution solution) throws JMException{
		double[] vector = new double[solution.numberOfVariables()];
		XReal values = new XReal(solution);

		for (int i = 0; i < vector.length; i++) {
			vector[i] = values.getValue(i);
		}

		double[] Os = getShiftedGlobalOptimum();
		double[] rotationMatrix = getRotationMatrix();
		int[] shuffleData = getShuffleData();

		double objective = 0;

		try {
			FS.test_func(Os, new double[2], vector.length, this.populationSize,
					this.numberOfHybridFunction);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}

		if (this.numberOfHybridFunction == 6) {
			objective = FS.hf01(vector, objective, vector.length, Os,
					rotationMatrix, shuffleData, 1, 1);
		} else if (this.numberOfHybridFunction == 7) {
			objective = FS.hf02(vector, objective, vector.length, Os,
					rotationMatrix, shuffleData, 1, 1);
		} else {
			objective = FS.hf03(vector, objective, vector.length, Os,
					rotationMatrix, shuffleData, 1, 1);
		}
		
		return objective+this.Fi;
	}

	public void setNumberOfHybridFunction(int numberOfHybridFunction) {
		this.numberOfHybridFunction = numberOfHybridFunction;
	}

	public void setFi(double fi) {
		this.Fi = fi;	
	}
	
	public void setPopulationSize(int populationSize) {
		this.populationSize = populationSize;
	}

}
