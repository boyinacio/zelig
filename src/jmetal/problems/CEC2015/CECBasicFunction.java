package jmetal.problems.CEC2015;

import jmetal.util.JMException;
import jmetal.util.wrapper.XReal;

/**
 * Interface that implements a basic function for a CEC2015 Learning-Based Problem
 * @author inacio-medeiros
 *
 */
public interface CECBasicFunction {
				
	/**
	 * Calculates a basic function given an input-vector parameter.
	 * @param vector Input vector x = [x_1, ..., x_n] over which basic function will be calculated 
	 * @return f(x) value for that input vector 
	 */
	public double calculate(XReal vector) throws JMException ;

}
