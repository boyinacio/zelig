package jmetal.problems;

import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.core.SolutionType;
import jmetal.core.Variable;
import jmetal.encodings.solutionType.IntSolutionType;
import jmetal.util.JMException;

public class SimplestProblem extends Problem {

	public SimplestProblem() {
		numberOfObjectives_  = 1                            ;
	    numberOfConstraints_ = 0                            ;
	    problemName_         = "SimplestProblem"                    ;
	        
	    upperLimit_ = new double[numberOfVariables_] ;
	    lowerLimit_ = new double[numberOfVariables_] ;
	       
	    for (int i = 0; i < numberOfVariables_; i++) {
	      lowerLimit_[i] = 0 ;
	      upperLimit_[i] = 1  ;
	    } // for
	    
	    solutionType_ = new IntSolutionType(this) ;
	    
	}

	public SimplestProblem(SolutionType solutionType) {
		super(solutionType);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void evaluate(Solution solution) throws JMException {
		Variable[] V = solution.getDecisionVariables();
		double x = 0;
		
		for (int i = 0; i < V.length; i++) {
			x += V[i].getValue() * Math.pow(2, V.length-i-1);
		}
		
		solution.setObjective(0, x);

	}

}
