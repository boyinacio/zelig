package jmetal.problems.clustering;

import weka.core.Instances;
import zelig.util.Calculator;
import zelig.util.distances.JoaoCarlosDistance;
import zelig.util.translators.ClusterNumberTranslator;
import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.core.SolutionType;
import jmetal.encodings.solutionType.ClusterNumberSolutionType;
import jmetal.encodings.solutionType.IntSolutionType;
import jmetal.util.JMException;

public class GKA_Problem extends Problem {

	private Instances baseDados;
	private int constant;
	
	public GKA_Problem() {
		// TODO Auto-generated constructor stub
	}

	public GKA_Problem(SolutionType solutionType) {
		super(solutionType);
		// TODO Auto-generated constructor stub
	}
	
	public GKA_Problem(Instances base, int numGrupos){
		baseDados = new Instances(base);
		constant = 2;
		
		numberOfVariables_   = baseDados.numInstances() ;
	    numberOfObjectives_  = 1		 ;
	    numberOfConstraints_ = 0         ;
	    problemName_         = "GKAProblem" ;
	        
	    upperLimit_ = new double[numberOfVariables_] ;
	    lowerLimit_ = new double[numberOfVariables_] ;
	       
	    for (int i = 0; i < numberOfVariables_; i++) {
	      lowerLimit_[i] = 1 ;
	      upperLimit_[i] = numGrupos;
	    } // for
	    
	    solutionType_ = new ClusterNumberSolutionType(this, numGrupos) ;
	    
	}

	@Override
	public void evaluate(Solution solution) throws JMException {	
	}
	
	public void evaluate(Solution solution, double media, double deviation) throws JMException {
		double fitness = TWCV(solution) - (media - (constant*deviation));
		if(fitness >= 0){
			solution.setObjective(0, fitness);
		} else {
			solution.setObjective(0, 0);
		}
	}
	
	public double TWCV(Solution solution) {
		ClusterNumberTranslator translator = new ClusterNumberTranslator();
		Instances base = translator.translate(solution, baseDados);	
		return Calculator.totalWithinClusterVariationOfCluster(base, new JoaoCarlosDistance());
	}
	
}
