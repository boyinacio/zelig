package jmetal.problems.clustering;

import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.core.SolutionType;
import jmetal.core.Variable;
import jmetal.encodings.solutionType.IntSolutionType;
import jmetal.util.JMException;
import jmetal.util.PseudoRandom;
import weka.clusterers.Clusterer;
import weka.clusterers.EM;
import weka.clusterers.HierarchicalClusterer;
import weka.clusterers.SimpleKMeans;
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;
import zelig.util.Calculator;
import zelig.util.ClustererAssigner;
import zelig.util.clusteringEvaluators.AdjustedRandIndex;
import zelig.util.clusteringEvaluators.MXIndex;
import zelig.util.distances.Distance;
import zelig.util.distances.JoaoCarlosDistance;
import zelig.util.translators.ClusterNumberTranslator;

public class EnsembleTWCVProblem extends Problem {

	private static final long serialVersionUID = 1L;

	private Instances base;

	private int numGrupos;

	private SimpleKMeans kmeans;
	private EM em;
	private HierarchicalClusterer hierarquico;
	private Distance distance;
	private int constant;

	private Instances[] particoes;

	private ClustererAssigner assigner = new ClustererAssigner();

	private void gerarAgrupamentos() throws Exception {
		particoes = new Instances[3];
		Clusterer agrupador = null;

		for (int i = 0; i < particoes.length; i++) {
			particoes[i] = new Instances(base);

			Remove filter = new Remove();
			try {
				filter.setAttributeIndices("" + (particoes[i].classIndex() + 1));
				filter.setInputFormat(particoes[i]);
				particoes[i] = Filter.useFilter(particoes[i], filter);
			} catch (Exception e) {
				e.printStackTrace();
				System.exit(-1);
			}

			switch (i) {
			case 0: // Saída do K-Means
				kmeans = new SimpleKMeans();
				kmeans.setPreserveInstancesOrder(true);
				kmeans.setNumClusters(this.numGrupos);
				kmeans.setSeed(PseudoRandom.randInt(1000, 10000));
				agrupador = kmeans;
				break;
			case 1: // Saída do EM
				em = new EM();
				em.setNumClusters(this.numGrupos);
				em.setSeed(PseudoRandom.randInt(1000, 10000));
				agrupador = em;
				break;
			default: // Saída do Hierárquico
				hierarquico = new HierarchicalClusterer();
				hierarquico.setNumClusters(this.numGrupos);
				agrupador = hierarquico;
				break;
			}

			agrupador.buildClusterer(particoes[i]);
			particoes[i] = assigner
					.assignFromClusterer(agrupador, particoes[i]);
		}

	}

	public EnsembleTWCVProblem(Instances base, int numGrupos) {
		this.base = new Instances(base);
		this.numGrupos = numGrupos;
		this.distance = new JoaoCarlosDistance();
		this.constant = 2;

		try {
			gerarAgrupamentos();
		} catch (Exception e1) {
			e1.printStackTrace();
			System.exit(-1);
		}

		numberOfVariables_ = base.numInstances();
		numberOfObjectives_ = 1;
		numberOfConstraints_ = 0;
		problemName_ = "EnsembleProblem";

		solutionType_ = new IntSolutionType(this);
		
		upperLimit_ = new double[numberOfVariables_];
		lowerLimit_ = new double[numberOfVariables_];

		for (int i = 0; i < numberOfVariables_; i++) {
			lowerLimit_[i] = 0;
			upperLimit_[i] = 2;
		} // for
	}

	public EnsembleTWCVProblem(SolutionType solutionType) {
		super(solutionType);
	}

	public Instances[] getParticoes() {
		return this.particoes;
	}

	@Override
	public void evaluate(Solution solution) throws JMException {
	}
	
	public void evaluate(Solution solution, double media, double deviation) throws JMException {
		Variable[] vars = solution.getDecisionVariables(); // Cromossomo
		int[] rotulos = new int[vars.length];

		for (int i = 0; i < rotulos.length; i++) {
			rotulos[i] = ((Double) vars[i].getValue()).intValue();
			rotulos[i] = ((Double) (particoes[rotulos[i]].instance(i)
					.classValue())).intValue();
		}

		Instances particao = new ClusterNumberTranslator().translate(rotulos,
				this.base);

		double fitness = TWCV(particao) - (media - (constant*deviation));
		if(fitness >= 0){
			solution.setObjective(0, fitness);
		} else {
			solution.setObjective(0, 0);
		}
	}
	
	public double TWCV(Instances instances) {	
		return Calculator.totalWithinClusterVariationOfCluster(instances, new JoaoCarlosDistance());
	}
	
	public double TWCV(Solution solution) throws JMException {
		Variable[] vars = solution.getDecisionVariables(); // Cromossomo
		int[] rotulos = new int[vars.length];

		for (int i = 0; i < rotulos.length; i++) {
			rotulos[i] = ((Double) vars[i].getValue()).intValue();
			rotulos[i] = ((Double) (particoes[rotulos[i]].instance(i)
					.classValue())).intValue();
		}

		Instances particao = new ClusterNumberTranslator().translate(rotulos,
				this.base);
		
		return TWCV(particao);
	}
	

}
