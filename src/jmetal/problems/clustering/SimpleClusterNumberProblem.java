package jmetal.problems.clustering;

import java.io.IOException;

import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.core.SolutionType;
import jmetal.encodings.solutionType.ClusterNumberSolutionType;
import jmetal.util.JMException;
import weka.core.Instances;
import zelig.util.clusteringEvaluators.AdjustedRandIndex;
import zelig.util.clusteringEvaluators.BasicDaviesBouldin;
import zelig.util.clusteringEvaluators.MXIndex;
import zelig.util.clusteringEvaluators.Sillhouette;
import zelig.util.distances.Distance;
import zelig.util.translators.ClusterNumberTranslator;

public class SimpleClusterNumberProblem extends Problem {

	private static final long serialVersionUID = 5879828754850644949L;
	private Instances base;
	private Distance distance;
	private double EPS;

	public SimpleClusterNumberProblem(Instances base, Distance distance,
			double EPS, int numGrupos) {
		this.base = new Instances(base);
		this.EPS = EPS;
		this.distance = distance;

		numberOfVariables_ = base.numInstances();
		numberOfObjectives_ = 1;
		numberOfConstraints_ = 0;
		problemName_ = "SimpleClusterNumberProblem";

		upperLimit_ = new double[numberOfVariables_];
		lowerLimit_ = new double[numberOfVariables_];

		for (int i = 0; i < numberOfVariables_; i++) {
			lowerLimit_[i] = 1;
			upperLimit_[i] = numGrupos;
		} // for

		solutionType_ = new ClusterNumberSolutionType(this, numGrupos);
	}

	public SimpleClusterNumberProblem(SolutionType solutionType) {
		super(solutionType);
	}

	@Override
	public void evaluate(Solution solution) throws JMException {
		ClusterNumberTranslator translator = new ClusterNumberTranslator();

		Instances insts = translator.translate(solution, new Instances(base));

		double index = 1;

		index = MXIndex.evaluate(insts, distance, EPS);
		solution.setObjective(0, index);

		// try {
		// index = BasicDaviesBouldin.main(insts);
		//
		// if(index == Double.NEGATIVE_INFINITY){
		// index = Double.POSITIVE_INFINITY;
		// }
		//
		// } catch (IOException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// System.exit(-1);
		// }
		//
		// solution.setObjective(0, index);

		// try {
		// index = Sillhouette.Silhouette(insts);
		// } catch (IOException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// System.exit(-1);
		// }
		// solution.setObjective(0, index);

		// index = AdjustedRandIndex.ARI(this.base, insts);
		// if ((index == Double.NEGATIVE_INFINITY)
		// || (index == Double.POSITIVE_INFINITY)) {
		// index = -1;
		// }
		// solution.setObjective(0, index);

	}

}
