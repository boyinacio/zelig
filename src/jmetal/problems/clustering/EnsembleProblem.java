package jmetal.problems.clustering;

import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.core.SolutionType;
import jmetal.core.Variable;
import jmetal.encodings.solutionType.IntSolutionType;
import jmetal.util.JMException;
import jmetal.util.PseudoRandom;
import weka.clusterers.Clusterer;
import weka.clusterers.EM;
import weka.clusterers.HierarchicalClusterer;
import weka.clusterers.SimpleKMeans;
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;
import zelig.util.ClustererAssigner;
import zelig.util.clusteringEvaluators.AdjustedRandIndex;
import zelig.util.clusteringEvaluators.MXIndex;
import zelig.util.distances.Distance;
import zelig.util.distances.JoaoCarlosDistance;
import zelig.util.translators.ClusterNumberTranslator;

public class EnsembleProblem extends Problem {

	private static final long serialVersionUID = 1L;

	private Instances base;

	private int numGrupos;

	private SimpleKMeans kmeans;
	private EM em;
	private HierarchicalClusterer hierarquico;
	private Distance distance;

	private Instances[] particoes;
	private Instances[] entrada;

	private ClustererAssigner assigner = new ClustererAssigner();

	private void gerarAgrupamentos() throws Exception {
		particoes = new Instances[3 * entrada.length];
		Clusterer agrupador = null;
				
		for (int i = 0; i <= (particoes.length-3); i += 3) {
			particoes[i] = new Instances(base);
			particoes[i+1] = new Instances(base);
			particoes[i+2] = new Instances(base);

			for (int j = 0; j < 3; j++) {
				Remove filter = new Remove();
				try {
					filter.setAttributeIndices("" + (particoes[i+j].classIndex() + 1));
					filter.setInputFormat(particoes[i+j]);
					particoes[i+j] = Filter.useFilter(particoes[i+j], filter);
				} catch (Exception e) {
					e.printStackTrace();
					System.exit(-1);
				}

				switch (j) {
				case 0: // Saída do K-Means
					kmeans = new SimpleKMeans();
					kmeans.setPreserveInstancesOrder(true);
					kmeans.setNumClusters(this.numGrupos);
					kmeans.setSeed(PseudoRandom.randInt(1000, 10000));
					agrupador = kmeans;
					break;
				case 1: // Saída do EM
					em = new EM();
					em.setNumClusters(this.numGrupos);
					em.setSeed(PseudoRandom.randInt(1000, 10000));
					agrupador = em;
					break;
				default: // Saída do Hierárquico
					hierarquico = new HierarchicalClusterer();
					hierarquico.setNumClusters(this.numGrupos);
					agrupador = hierarquico;
					break;
				}

				agrupador.buildClusterer(particoes[i+j]);
				particoes[i+j] = assigner.assignFromClusterer(agrupador,
						particoes[i+j]);
			}
		}
	}

	public EnsembleProblem(Instances[] bases, int numGrupos) {
		this.base = new Instances(bases[0]);
		this.entrada = bases;
		this.distance = new JoaoCarlosDistance();
		this.numGrupos = numGrupos;

		try {
			gerarAgrupamentos();
		} catch (Exception e1) {
			e1.printStackTrace();
			System.exit(-1);
		}

		numberOfVariables_ = base.numInstances();
		numberOfObjectives_ = 1;
		numberOfConstraints_ = 0;
		problemName_ = "EnsembleProblem";

		solutionType_ = new IntSolutionType(this);

		upperLimit_ = new double[numberOfVariables_];
		lowerLimit_ = new double[numberOfVariables_];

		for (int i = 0; i < numberOfVariables_; i++) {
			lowerLimit_[i] = 0;
			upperLimit_[i] = (bases.length * 3) - 1;
		} // for
	}

	public EnsembleProblem(SolutionType solutionType) {
		super(solutionType);
	}

	public Instances[] getParticoes() {
		return this.particoes;
	}

	@Override
	public void evaluate(Solution solution) throws JMException {
		Variable[] vars = solution.getDecisionVariables(); // Cromossomo
		int[] rotulos = new int[vars.length];

		for (int i = 0; i < rotulos.length; i++) {
			rotulos[i] = ((Double) vars[i].getValue()).intValue();
			rotulos[i] = ((Double) (particoes[rotulos[i]].instance(i)
					.classValue())).intValue();
		}

		Instances particao = new ClusterNumberTranslator().translate(rotulos,
				this.base);

		double index = 0;

		index = MXIndex.evaluate(particao, distance, 0.8);
		solution.setObjective(0, index);

		// try {
		// index = BasicDaviesBouldin.main(particao);
		// } catch (IOException e) {
		// e.printStackTrace();
		// System.exit(-1);
		// }

		// index = AdjustedRandIndex.ARI(this.base, particao);
		// if ((index == Double.NEGATIVE_INFINITY)
		// || (index == Double.POSITIVE_INFINITY)) {
		// index = -1;
		// }

		solution.setObjective(0, index);

	}

}
