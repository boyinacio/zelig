package jmetal.problems;

import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.core.SolutionType;
import jmetal.core.Variable;
import jmetal.encodings.solutionType.RealSolutionType;
import jmetal.util.JMException;

public class DisciplinesNumberProblem extends Problem {

	private int cargaHoraria;
	
	public DisciplinesNumberProblem(int cargaHoraria) throws ClassNotFoundException {
		this.cargaHoraria = cargaHoraria;
		numberOfVariables_   = 2 ;
	    numberOfObjectives_  = 1 ;
	    numberOfConstraints_ = 0 ;
	    problemName_         = "DisciplinesNumberProblem" ;
	      
	    solutionType_ = new RealSolutionType(this);
	    
	    upperLimit_ = new double[numberOfVariables_] ;
	    lowerLimit_ = new double[numberOfVariables_] ;
	       
	    for (int i = 0; i < numberOfVariables_; i++) {
	      lowerLimit_[i] = 0;
	      upperLimit_[i] = 9;
	    } // for
	}

	public DisciplinesNumberProblem(SolutionType solutionType) {
		super(solutionType);
	}

	@Override
	public void evaluate(Solution solution) throws JMException {
		Variable[] vars = solution.getDecisionVariables();
						
		double eq = (90 * vars[0].getValue()) + (60 * vars[1].getValue());  
		solution.setObjective(0, Math.abs(this.cargaHoraria-eq));
		
	}

}
