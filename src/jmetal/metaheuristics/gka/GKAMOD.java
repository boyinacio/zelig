package jmetal.metaheuristics.gka;

import jmetal.core.Algorithm;
import jmetal.core.Operator;
import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.core.SolutionSet;
import weka.core.Instances;
import zelig.util.Calculator;
import zelig.util.distances.JoaoCarlosDistance;
import zelig.util.translators.ClusterNumberTranslator;

public class GKAMOD extends Algorithm {

	private static final long serialVersionUID = -4790054436506426843L;

	private Instances base;

	public GKAMOD(Problem problem) {
		super(problem);
		// TODO Auto-generated constructor stub
	}

	public SolutionSet executar() throws Exception {
		base = new Instances((Instances) getInputParameter("baseDados"));

		int populationSize;
		int maxEvaluations;

		SolutionSet population;
		SolutionSet newPopulation;

		Solution solucaoFinal;
		Solution melhorDaPopulacao = null;
		Solution iterador;

		Operator mutationOperator;
		Operator crossoverOperator;
		Operator selectionOperator;

		// Read the parameters
		populationSize = ((Integer) getInputParameter("populationSize"))
				.intValue();
		maxEvaluations = ((Integer) getInputParameter("maxEvaluations"))
				.intValue();

		// Read the operators
		mutationOperator = operators_.get("mutation");
		crossoverOperator = operators_.get("crossover");
		selectionOperator = operators_.get("selection");

		// Initialize the variables
		population = new SolutionSet(populationSize);

		Solution newSolution;
		for (int i = 0; i < populationSize; i++) {
			newSolution = new Solution(problem_);
			population.add(newSolution);
		}

		solucaoFinal = new Solution(population.get(0));

		for (int i = 0; i < maxEvaluations; i++) {

			for (int j = 0; j < populationSize; j++) {
				problem_.evaluate(population.get(j));
			}

			newPopulation = (SolutionSet) selectionOperator.execute(population);

			for (int k = 0; k < newPopulation.size(); k++) {
				iterador = (Solution) mutationOperator.execute(newPopulation
						.get(k));
				iterador = (Solution) crossoverOperator.execute(iterador);

				population.replace(k, iterador);

				if (k == 0) {
					melhorDaPopulacao = population.get(0);
				} else if (TWCV(population.get(k)) < TWCV(melhorDaPopulacao)) {
					melhorDaPopulacao = new Solution(population.get(k));
				}

			}

			if (TWCV(solucaoFinal) > TWCV(melhorDaPopulacao)) {
				solucaoFinal = new Solution(melhorDaPopulacao);
			}

		}

		population = new SolutionSet(1);
		population.add(solucaoFinal);

		return population;
	}

	private double TWCV(Solution solucaoFinal) {
		ClusterNumberTranslator translator = new ClusterNumberTranslator();
		Instances baseD = translator.translate(solucaoFinal, base);	
		return Calculator.totalWithinClusterVariationOfCluster(baseD, new JoaoCarlosDistance());
	}
	@Override
	public SolutionSet execute() {
		return null;
	}

}
