package jmetal.metaheuristics.gka;

import jmetal.core.Algorithm;
import jmetal.core.Operator;
import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.core.SolutionSet;
import jmetal.problems.clustering.GKA_Problem;
import weka.core.Instances;
import zelig.util.Statistics;

public class GKA extends Algorithm {

	private static final long serialVersionUID = -4790054436506426843L;

	private GKA_Problem problema;
	private Instances base;
	private int numGrupos;

	public GKA(Problem problem) {
		super(problem);
		// TODO Auto-generated constructor stub
	}

	public GKA() {
		super(null);
	}

	public SolutionSet executar() throws Exception {
		base = new Instances((Instances) getInputParameter("baseDados"));
		numGrupos = ((Integer) getInputParameter("numGrupos")).intValue();
		// problema = new GKAProblem(base, numGrupos);
		problema = new GKA_Problem(base, numGrupos);

		int populationSize;
		int maxEvaluations;

		SolutionSet population;
		SolutionSet newPopulation;

		Solution solucaoFinal;
		Solution melhorDaPopulacao = null;
		Solution iterador;

		Operator mutationOperator;
		Operator crossoverOperator;
		Operator selectionOperator;

		// Read the parameters
		populationSize = ((Integer) getInputParameter("populationSize"))
				.intValue();
		maxEvaluations = ((Integer) getInputParameter("maxEvaluations"))
				.intValue();

		// Read the operators
		mutationOperator = operators_.get("mutation");
		crossoverOperator = operators_.get("crossover");
		selectionOperator = operators_.get("selection");

		// Initialize the variables
		population = new SolutionSet(populationSize);

		Solution newSolution;
		for (int i = 0; i < populationSize; i++) {
			newSolution = new Solution(problema);
			population.add(newSolution);
		}

		solucaoFinal = new Solution(population.get(0));

		for (int i = 0; i < maxEvaluations; i++) {

			double[] mediaAndDev = mediaAndDeviation(problema, population);

			for (int j = 0; j < populationSize; j++) {
				problema.evaluate(population.get(j), mediaAndDev[0],
						mediaAndDev[1]);
			}

			newPopulation = (SolutionSet) selectionOperator.execute(population);

			for (int k = 0; k < newPopulation.size(); k++) {
				iterador = (Solution) mutationOperator.execute(newPopulation
						.get(k));
				iterador = (Solution) crossoverOperator.execute(iterador);

				population.replace(k, iterador);

				if (k == 0) {
					melhorDaPopulacao = population.get(0);
				} else if (problema.TWCV(population.get(k)) < problema
						.TWCV(melhorDaPopulacao)) {
					melhorDaPopulacao = new Solution(population.get(k));
				}

			}

			if (problema.TWCV(solucaoFinal) > problema.TWCV(melhorDaPopulacao)) {
				solucaoFinal = new Solution(melhorDaPopulacao);
			}

		}

		population = new SolutionSet(1);
		population.add(solucaoFinal);

		return population;
	}

	private double[] mediaAndDeviation(GKA_Problem pro, SolutionSet solucoes)
			throws Exception {

		int quantSolucoes = solucoes.size();

		double[] valores = new double[quantSolucoes];

		for (int i = 0; i < quantSolucoes; i++) {
			valores[i] = pro.TWCV(solucoes.get(i));
		}

		// values[0] = media | values[1] = deviation
		double[] values = new double[2];
		values[0] = Statistics.mean(valores);
		values[1] = Statistics.deviation(valores);

		return values;
	}

	@Override
	public SolutionSet execute() {
		return null;
	}

}
