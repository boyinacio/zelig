package jmetal.metaheuristics.cro;

import java.io.IOException;
import java.util.HashMap;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

import jmetal.core.Algorithm;
import jmetal.core.Operator;
import jmetal.core.Problem;
import jmetal.core.SolutionSet;
import jmetal.operators.crossover.UniformCrossover;
import jmetal.operators.mutation.MutationFactory;
import jmetal.operators.selection.RouletteWheelSelection;
import jmetal.problems.CEC2015.benchmarkFunctions.CompositionFunction7;
import jmetal.util.Configuration;
import jmetal.util.JMException;

public class CRO_Main {
	public static Logger logger_; // Logger object
	public static FileHandler fileHandler_; // FileHandler object

	public static void main(String[] args) throws ClassNotFoundException,
			JMException, SecurityException, IOException {
		Problem problem; // The problem to solve
		Algorithm algorithm; // The algorithm to use
		Operator crossover; // Crossover operator
		Operator mutation; // Mutation operator
		Operator selection; // Selection operator

		logger_ = Configuration.logger_;
		fileHandler_ = new FileHandler("CRO_main.log");
		logger_.addHandler(fileHandler_);
		
		HashMap parameters; // Operator parameters
				
		problem = new CompositionFunction7(30);
		//problem = new CompositionFunction1(10);
		//problem = new Sphere("Real", 20);
		algorithm = new CRO(problem);
		
		algorithm.setInputParameter("generations", 100);
		
		algorithm.setInputParameter("reefSizeM", 5);
		algorithm.setInputParameter("reefSizeN", 1);
		
		algorithm.setInputParameter("broadcastProb", 0.9);
		algorithm.setInputParameter("predationProb", 0.1);
		
		algorithm.setInputParameter("fractionCoralsDepredated", 0.1);
		algorithm.setInputParameter("freeOccupiedProportion", 0.6);
		
		algorithm.setInputParameter("opportunitiesToSettle", 3);

		// Mutation and Crossover for Real codification
		parameters = new HashMap();
		parameters.put("probability", 0.9);
		parameters.put("distributionIndex", 20.0);
		crossover = new UniformCrossover(parameters);

		parameters = new HashMap();
		parameters.put("probability", 1.0 / problem.getNumberOfVariables());
		parameters.put("distributionIndex", 20.0);
		mutation = MutationFactory.getMutationOperator("PolynomialMutation",
				parameters);

		// Selection Operator
		parameters = null;
		selection = new RouletteWheelSelection(parameters);

		// Add the operators to the algorithm
		algorithm.addOperator("crossover", crossover);
		algorithm.addOperator("mutation", mutation);
		algorithm.addOperator("selection", selection);

		// Execute the Algorithm
		long initTime = System.currentTimeMillis();
		SolutionSet population = algorithm.execute();
		long estimatedTime = System.currentTimeMillis() - initTime;

		// Result messages
		logger_.info("Total execution time: " + estimatedTime + "ms");
		logger_.info("Variables values have been writen to file VAR");
		population.printVariablesToFile("VAR");
		logger_.info("Objectives values have been writen to file FUN");
		population.printObjectivesToFile("FUN");

	}

}
