package jmetal.metaheuristics.cro;

import java.util.LinkedList;

import zelig.util.Calculator;

import jmetal.core.Algorithm;
import jmetal.core.Operator;
import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.core.SolutionSet;
import jmetal.util.JMException;
import jmetal.util.PseudoRandom;
import jmetal.util.comparators.SingleObjectiveComparatorMinimum;

/**
 * This class implements the CRO algorithm. This algorithm was constructed under
 * a premise that the problem is of MINIMIZATION
 * 
 */

public class CROMod3 extends Algorithm {

	private static final long serialVersionUID = 1512173715953692623L;
	
	private int generations;

	// Dimensions of Reef Grid
	private int reefSizeM;
	private int reefSizeN;

	private Solution[][] reef; // Reef Grid
	private SolutionSet corals; // Support structure

	private double broadcastProb; // Broadcast Spawning prob.
	private double predationProb; // Depredation reef prob.

	// Fraction of the corals to be eliminated in the depredation operator.
	private double fractionCoralsDepredated;

	// Free/total initial proportion
	private double freeOccupiedProportion;

	// Number of opportunities for a new coral to settle in the reef
	private int opportunitiesToSettle;

	// Operators
	private Operator mutationOperator;
	private Operator crossoverOperator;
	private Operator selectionOperator;

	/**
	 * Constructor
	 * 
	 * @param problem
	 *            Problem to solve
	 */
	public CROMod3(Problem problem) {
		super(problem);
	}

	/**
	 * Runs the CRO algorithm.
	 * 
	 * @return a <code>SolutionSet</code> that is a set of best solutions as a
	 *         result of the algorithm execution
	 * @throws JMException
	 */
	@Override
	public SolutionSet execute() throws JMException, ClassNotFoundException {
		// CREATION AND INSTANTIATION OF VARIABLES
		// =======================================================================
		// Read the parameters
		generations = ((Integer) getInputParameter("generations")).intValue();

		reefSizeM = ((Integer) getInputParameter("reefSizeM")).intValue();
		reefSizeN = ((Integer) getInputParameter("reefSizeN")).intValue();

		broadcastProb = (Double) getInputParameter("broadcastProb");
		predationProb = (Double) getInputParameter("predationProb");

		fractionCoralsDepredated = (Double) getInputParameter("fractionCoralsDepredated");
		freeOccupiedProportion = (Double) getInputParameter("freeOccupiedProportion");

		opportunitiesToSettle = ((Integer) getInputParameter("opportunitiesToSettle"))
				.intValue();

		// Read the operators
		mutationOperator = operators_.get("mutation");
		crossoverOperator = operators_.get("crossover");
		selectionOperator = operators_.get("selection");

		// =======================================================================

		// INITIALIZATION OF REEF AND SETTLING OF FIRST CORALS
		// =======================================================================

		// Initializes reef with all positions free
		reef = new Solution[reefSizeM][reefSizeN];
		corals = new SolutionSet(reefSizeM * reefSizeN);

		for (int i = 0; i < reefSizeM; i++) {
			for (int j = 0; j < reefSizeN; j++) {
				reef[i][j] = null;
			}
		}

		// Sets initial population to beBound a fraction of total reef available
		// spaces
		int initalPopulation = (int) ((freeOccupiedProportion * reefSizeM * reefSizeN) - ((freeOccupiedProportion
				* reefSizeM * reefSizeN) % 1));

		Solution coral;
		int M, N;
		for (int i = 0; i < initalPopulation; i++) {
			do {
				M = PseudoRandom.randInt(0, reefSizeM - 1);
				N = PseudoRandom.randInt(0, reefSizeN - 1);
			} while (reef[M][N] != null);
			coral = new Solution(problem_);
			problem_.evaluate(coral);
			reef[M][N] = coral;
			corals.add(coral);
		}

		// =======================================================================

		// MAIN LOOP
		// =======================================================================
		int spawners, brooders, budders;
		LinkedList<Solution> larvaeFromSpawning = new LinkedList<Solution>();
		LinkedList<Solution> larvaeFromBrooding = new LinkedList<Solution>();
		LinkedList<Solution> larvaeFromBudding = new LinkedList<Solution>();

		// LinkedList<Solution> cantReproduce = new LinkedList<Solution>();

		Solution[] parents = new Solution[2];
		Solution coralForBrooding;
		Solution larvae;

		// [GENERATIONS LOOP]
		for (int i = 0; i < generations; i++) {
			// long t = System.currentTimeMillis();

			int population = corals.size();

			// SELECTION OF CORALS FOR BROADCAST SPAWNING
			// *************************************************************

			// Quantity of spawners
			// Spawnwers are forced to be a par quantity
			spawners = (int) ((broadcastProb * population) - ((broadcastProb * population) % 1));
			if ((spawners % 2) > 0) {
				spawners--;
			}

			// Select spawners
			for (int j = 0; j < (spawners / 2); j++) {
				parents[0] = (Solution) selectionOperator.execute(corals);
				corals.remove(indexOfSolution(parents[0]));

				parents[1] = (Solution) selectionOperator.execute(corals);
				corals.remove(indexOfSolution(parents[1]));

				// Produces new larvae and adds it to "the water"
				larvae = (Solution) crossoverOperator.execute(parents);
				problem_.evaluate(larvae);
				larvaeFromSpawning.add(larvae);

			}

			// Get back capacity of being selected TODO Auto-generated method stubfor reproduction
			// cantReproduce.clear();

			// Quantity of brooders
			// Brooders don't, because they'll suffer mutation
			// Select brooders

			brooders = corals.size();

			for (int k = 0; k < brooders; k++) {
				coralForBrooding = corals.get(k);

				// Produces new larvae and adds it to "the water"
				larvae = (Solution) mutationOperator.execute(new Solution(
						coralForBrooding));
				problem_.evaluate(larvae);
				larvaeFromBrooding.add(larvae);
			}

			// **************************************************************************

			// LARVAE SETTLING
			// *************************************************************
			settlingPhase(larvaeFromSpawning);
			settlingPhase(larvaeFromBrooding);

			larvaeFromSpawning.clear();
			larvaeFromBrooding.clear();
			// *************************************************************

			// BUDDERS AND DEPREDATION PHASE
			// *************************************************************
			reciclation();

			corals.sort(new SingleObjectiveComparatorMinimum());

			double aux = (fractionCoralsDepredated * corals.size());
			budders = (int) (aux - (aux % 1));

			if (budders > 0) {
				for (int bud = corals.size() - 1; bud > ((corals.size() - 1)
						- budders - 1); bud--) {
					larvaeFromBudding.add(corals.get(bud));
				}

				settlingPhase(larvaeFromBudding);

				larvaeFromBudding.clear();

				reciclation();
				corals.sort(new SingleObjectiveComparatorMinimum());

				if (((i + 1) % 5) == 0) {
					depredationPhase(budders);
				}

//				depredationPhase(budders);
			}

			reciclation();
			// MODIFICAÇÃO 1
			 corals.sort(new SingleObjectiveComparatorMinimum());

			// System.out.println("Geração " + i + ": " +
			// (System.currentTimeMillis() - t));

			// *************************************************************

//			double[] values = Calculator.bestWorstMediaStdDev(corals,
//					new SingleObjectiveComparatorMinimum());

//			System.out.println(i + "\t"
//					+ corals.get(corals.size() - 1).getObjective(0) + "\t"
//					+ corals.get(0).getObjective(0));

		} // [/GENERATIONS LOOP]

		return corals;
	}

	private void settlingPhase(LinkedList<Solution> larvae) {
		boolean larvaeGotSettle;
		int targetX, targetY;
		for (int j = 0; j < larvae.size(); j++) {
			larvaeGotSettle = false;
			for (int j2 = 0; j2 < opportunitiesToSettle; j2++) {
				targetX = PseudoRandom.randInt(0, reefSizeM - 1);
				targetY = PseudoRandom.randInt(0, reefSizeN - 1);

				if (reef[targetX][targetY] == null) { // If reef position is
														// free
					larvaeGotSettle = true;
					// Else if larvae's fitness is better than ones from actual
					// coral in position
				} else if (reef[targetX][targetY].getObjective(0) > larvae.get(
						j).getObjective(0)) {
					larvaeGotSettle = true;
				}

				if (larvaeGotSettle) {
					reef[targetX][targetY] = larvae.get(j);
					break;
				}
			}
		}
	}

	private void depredationPhase(int budders) {
		double coin;
		int X, Y;
		Solution larvae;

		for (int j = 0; j < budders; j++) {
			coin = PseudoRandom.randDouble(0, 1);

			if (coin < predationProb) {
				X = -1;
				Y = -1;

				larvae = corals.get(j);

				// GET COORDINATIONS FOR EXCLUDING LARVAE FROM REEF
				for (int j2 = 0; j2 < reefSizeM; j2++) {
					boolean toBreak = false;
					for (int k = 0; k < reefSizeN; k++) {
						if (reef[j2][k] == larvae) {
							X = j2;
							Y = k;
							toBreak = true;
							break;
						}
					}
					if (toBreak) {
						break;
					}
				}

				if ((X != -1) && (Y != -1)) {
					reef[X][Y] = null; // Frees space of reef
				}
			}

		}
	}

	/**
	 * Updates LinkedList<Coral> corals just with values from reef
	 */
	private void reciclation() {
		corals.clear();
		for (int i = 0; i < reefSizeM; i++) {
			for (int j = 0; j < reefSizeN; j++) {
				if (reef[i][j] != null) {
					corals.add(reef[i][j]);
				}
			}
		}
	}

	private int indexOfSolution(Solution sol) {
		for (int i = 0; i < corals.size(); i++) {
			if (corals.get(i) == sol) {
				return i;
			}
		}
		return -1;
	}

}
