package jmetal.metaheuristics.fireworks;

import java.util.Comparator;

import jmetal.core.Algorithm;
import jmetal.core.Operator;
import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.core.SolutionSet;
import jmetal.util.Distance;
import jmetal.util.JMException;
import jmetal.util.MersenneTwisterFast;

public class FireworksAlgorithm extends Algorithm {

	private static final long serialVersionUID = 8197079262495354872L;
	private int populationSize;
	private int maxGenerations;
	private int numGaussianSparks;

	private SolutionSet fireworks; // Stores fireworks
	private SolutionSet sparks; // Stores sparks for each firework
	private SolutionSet gaussianSparks; // Stores sparks for each firework

	private Solution best;

	private Operator explosionOperator;
	private Operator gaussianExplosionOperator;
	private Operator selectionOperator;

	// private Distance distance;
	private MersenneTwisterFast random;
	private Distance D;

	private Comparator comparator;

	public FireworksAlgorithm(Problem problem) {
		super(problem);
		random = new MersenneTwisterFast();
		D = new Distance();
	}

	@Override
	public SolutionSet execute() throws JMException, ClassNotFoundException {
		// Initializing parameters
		populationSize = ((Integer) getInputParameter("populationSize"))
				.intValue();
		maxGenerations = ((Integer) getInputParameter("maxGenerations"))
				.intValue();
		numGaussianSparks = ((Integer) getInputParameter("numGaussianSparks"))
				.intValue();

		explosionOperator = operators_.get("explosion");
		gaussianExplosionOperator = operators_.get("gaussian");
		selectionOperator = operators_.get("selection");

		comparator = (Comparator) getInputParameter("comparator");

		fireworks = new SolutionSet(populationSize);
		gaussianSparks = new SolutionSet(numGaussianSparks);

		// In worst case, each firework sets off "mParameter" new
		// sparks, so sparks population will contain, at maximum,
		// populationSize^2 solutions (or mParaneter sparks)
		// sparks = new SolutionSet(populationSize * mParameter);

		// Populating fireworks
		Solution firework;
		for (int i = 0; i < populationSize; i++) {
			firework = new Solution(problem_);
			problem_.evaluate(firework);
			problem_.evaluateConstraints(firework);
			fireworks.add(firework);
		}

		// MAIN LOOP
		Solution randomFirework;
		SolutionSet sparksFromSolution;
		SolutionSet nextPopulation;
		for (int generation = 0; generation < maxGenerations; generation++) {
			sparks = new SolutionSet();

			double bestFitness = fireworks.best(comparator).getObjective(0);
			double worstFitness = fireworks.worst(comparator).getObjective(0);
			double summationsNumSparks = 0;
			double summationsAmplitude = 0;

			for (int objective = 0; objective < fireworks.size(); objective++) {
				summationsNumSparks += (bestFitness - fireworks.get(objective)
						.getObjective(0));
				summationsAmplitude += (fireworks.get(objective)
						.getObjective(0) - worstFitness);
			}

			Object[] info = new Object[5];
			info[0] = bestFitness;
			info[1] = worstFitness;
			info[2] = summationsNumSparks;
			info[3] = summationsAmplitude;

			for (int solution = 0; solution < fireworks.size(); solution++) {
				info[4] = fireworks.get(solution);
				sparksFromSolution = (SolutionSet) explosionOperator
						.execute(info);
				sparks.setCapacity(sparks.getCapacity()
						+ sparksFromSolution.size());

				for (int i = 0; i < sparksFromSolution.size(); i++) {
					sparks.add(new Solution(sparksFromSolution.get(i)));
				}

			}

			// System.out.println("OK 1");

			for (int i = 0; i < sparks.size(); i++) {
				problem_.evaluate(sparks.get(i));
				problem_.evaluateConstraints(sparks.get(i));
			}

			gaussianSparks.clear();
			Solution solution;
			for (int gSpark = 0; gSpark < numGaussianSparks; gSpark++) {
				randomFirework = fireworks
						.get(random.nextInt(fireworks.size()));

				solution = (Solution) gaussianExplosionOperator
						.execute(randomFirework);
				problem_.evaluate(solution);
				problem_.evaluateConstraints(solution);

				gaussianSparks.add(solution);
			}

			// System.out.println("OK 2");

			// Joins "fireworks" with "sparks" and "gaussianSparks"
			// for selecting populationSize-1 fireworks
			nextPopulation = new SolutionSet(fireworks.size());

			// Clears "fireworks", once it's gonna be repopulated
			best = fireworks.best(comparator);

			if (sparks.best(comparator).getObjective(0) < best.getObjective(0)) {
				best = sparks.best(comparator);
			}

			if (gaussianSparks.best(comparator).getObjective(0) < best
					.getObjective(0)) {
				best = gaussianSparks.best(comparator);
			}

			nextPopulation.add(best);

			// System.out.println("OK 3");

			// Makes selection from populationSize-1 new fireworks
			Solution S;
			SolutionSet[] sets = { fireworks, sparks, gaussianSparks };

			// Through generateProbabilities(sets), it's calculated single
			// probability of each solution, and then, via "cumulative" method,
			// it's calculated cumulative probability for each solution
			double[] cumulativeProbs = cumulative(generateProbabilities(sets));
			Object[] data = { sets, cumulativeProbs };
			for (int sp = 0; sp < populationSize - 1; sp++) {
				S = (Solution) selectionOperator.execute(data);
				nextPopulation.add(S);
			}

			// System.out.println("OK 4");

			fireworks.clear();
			fireworks = nextPopulation;

			// fireworks.printObjectives();
			System.out.printf("Generation %3d - %f%n", generation, fireworks
					.best(comparator).getObjective(0));
		}

		SolutionSet onlyOne = new SolutionSet(1);
		onlyOne.add(fireworks.best(comparator));

		return onlyOne;
	}

	private double[] generateProbabilities(SolutionSet[] sets)
			throws JMException {
		int numSolutions = 0;

		for (int i = 0; i < sets.length; i++) {
			numSolutions += sets[i].size();
		}

		double[] selectionprobability = new double[numSolutions];

		int prevIdx, nextIdx;
		int totalIter = 0;
		double tmpdis;

		for (int i = 0; i < sets.length; i++) {
			prevIdx = -1;
			nextIdx = 1;

			if (i == 1) {
				prevIdx = 0;
				nextIdx = 2;
			} else {
				prevIdx = 1;
				nextIdx = -1;
			}

			// FOR EACH SOLUTION OF I-TH POPULATION
			for (int j = 0; j < sets[i].size(); j++) {
				tmpdis = 0;

				selectionprobability[totalIter] = 0;

				// COMPARE J-TH SOLUTION OF I-TH POPULATION WITH OTHER SOLUTIONS
				// OF I-TH
				// POPULATION
				for (int j2 = 0; j2 < sets[i].size(); j2++) {
					tmpdis += D.distanceBetweenSolutions(sets[i].get(j),
							sets[i].get(j2));
				}

				// COMPARE J-TH SOLUTION OF I-TH POPULATION WITH SOLUTIONS
				// OF 0THER sets
				if (prevIdx >= 0) {
					for (int j2 = 0; j2 < sets[prevIdx].size(); j2++) {
						tmpdis += D.distanceBetweenSolutions(sets[i].get(j),
								sets[prevIdx].get(j2));
					}
				}
				if (nextIdx >= 0) {
					for (int j2 = 0; j2 < sets[nextIdx].size(); j2++) {
						tmpdis += D.distanceBetweenSolutions(sets[i].get(j),
								sets[nextIdx].get(j2));
					}
				}

				selectionprobability[totalIter] = tmpdis;
				totalIter++;
			}
		}
		return selectionprobability;
	}

	private double[] cumulative(double[] probs) {
		double[] cumulativeProbs = new double[probs.length];
		cumulativeProbs[0] = probs[0];

		for (int i = 1; i < cumulativeProbs.length; i++) {
			for (int j = i - 1; j < cumulativeProbs.length; j++) {
				cumulativeProbs[i] += cumulativeProbs[i - 1];
			}
		}

		return cumulativeProbs;
	}

}
