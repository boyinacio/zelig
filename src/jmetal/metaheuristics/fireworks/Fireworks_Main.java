package jmetal.metaheuristics.fireworks;

import java.io.IOException;
import java.util.Comparator;
import java.util.HashMap;

import jmetal.core.Algorithm;
import jmetal.core.Operator;
import jmetal.core.Problem;
import jmetal.core.SolutionSet;
import jmetal.operators.mutation.FireworksExplosion;
import jmetal.operators.mutation.FireworksGaussian;
import jmetal.operators.mutation.MutationFactory;
import jmetal.operators.mutation.PolynomialMutation;
import jmetal.operators.selection.FireworksSelectionStrategy;
import jmetal.operators.selection.Selection;
import jmetal.problems.CEC2015.benchmarkFunctions.CompositionFunction7;
import jmetal.problems.CEC2015.benchmarkFunctions.HybridFunction2;
import jmetal.util.JMException;
import jmetal.util.comparators.ObjectiveComparator;

public class Fireworks_Main {

	public static void main(String[] args) throws JMException,
			SecurityException, IOException, ClassNotFoundException {
		Problem problem; // The problem to solve
		Algorithm algorithm; // The algorithm to use
		Selection selection; // Selection operator
		Operator explosion;
		Operator gaussian;
		Comparator comparator = new ObjectiveComparator(0);
		
		// problem = new Sphere("Real", 30);
		int popSize = 10;
		
		// Hybrid Functions problems has two parameters in constructor: dimension of problem
		// and populationSize
		problem = new HybridFunction2(30);
		
		algorithm = new Fireworks(problem);
		int maxGenerations = 30000;
		int numGaussianSparks = 5;
		double maxAmplitude = 40.0;
		int mParameter = 50;

		double aBound = 0.04;
		double bBound = 0.8;

		// passing parameters to Algorithm
		algorithm.setInputParameter("populationSize", popSize);
		algorithm.setInputParameter("maxGenerations", maxGenerations);
		algorithm.setInputParameter("numGaussianSparks", numGaussianSparks);
		algorithm.setInputParameter("mParameter", mParameter);
		algorithm.setInputParameter("comparator", comparator);
		algorithm.setInputParameter("eps", 1e-38);

		HashMap<String, Object> parameters = new HashMap<String, Object>();

		// passing paramters to the operators
		parameters.put("numGaussianSparks", numGaussianSparks);
		parameters.put("maxAmplitude", maxAmplitude);

		parameters.put("aBound", aBound);
		parameters.put("bBound", bBound);
		parameters.put("mParameter", mParameter); // maxSparks

		parameters.put("populationSize", popSize);
		parameters.put("comparator", comparator);
		parameters.put("eps", 1e-38);

		parameters.put("comparator", comparator);

		explosion = new FireworksExplosion(parameters);
		
		parameters = new HashMap<String, Object>();
	    parameters.put("probability", 1.0/problem.getNumberOfVariables()) ;
	    parameters.put("distributionIndex", 20.0) ;
		gaussian = MutationFactory.getMutationOperator("PolynomialMutation", parameters);

		parameters = new HashMap<String, Object>();
		parameters.put("comparator", comparator);
		selection = new FireworksSelectionStrategy(parameters);

		algorithm.addOperator("selection", selection);
		algorithm.addOperator("explosion", explosion);
		algorithm.addOperator("gaussian", gaussian);

		// Execute the Algorithm
		long initTime = System.currentTimeMillis();
		SolutionSet population = algorithm.execute();
		long estimatedTime = System.currentTimeMillis() - initTime;

		// Result messages
		System.out.println("Total execution time: " + estimatedTime + "ms");
		System.out.println("Objectives values have been writen to file FUN");
		population.printObjectivesToFile("FUN");

	}

}
