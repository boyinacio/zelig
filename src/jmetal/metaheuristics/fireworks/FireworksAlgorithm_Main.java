package jmetal.metaheuristics.fireworks;

import java.io.IOException;
import java.util.Comparator;
import java.util.HashMap;

import jmetal.core.Algorithm;
import jmetal.core.Operator;
import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.core.SolutionSet;
import jmetal.operators.mutation.FireworkGaussianMutation;
import jmetal.operators.mutation.FireworkMutation;
import jmetal.operators.mutation.MutationFactory;
import jmetal.operators.selection.FireworksSelection;
import jmetal.problems.Elliptic;
import jmetal.problems.CEC2015.benchmarkFunctions.UnimodalFunction1;
import jmetal.problems.singleObjective.Sphere;
import jmetal.util.JMException;
import jmetal.util.comparators.SingleObjectiveComparatorMaximum;

public class FireworksAlgorithm_Main {

	public static void main(String[] args) throws JMException,
			SecurityException, IOException, ClassNotFoundException {
		Problem problem; // The problem to solve
		Algorithm algorithm; // The algorithm to use
		Operator selection; // Selection operator
		Operator explosion;
		Operator gaussian;
		Comparator<Solution> comparator = new SingleObjectiveComparatorMaximum();

//		problem = new UnimodalFunction1(2);
		// problem = new Elliptic("Real", 10);
		 problem = new Sphere("Real", 10);
		// problem = new Rosenbrock("Real", 3);

		algorithm = new FireworksAlgorithm(problem);

		int popSize = 5;
		int maxGenerations = 2000;
		int numGaussianSparks = 5;
		double maxAmplitude = 40.0;
		int mParameter = 50;

		double aBound = 0.04;
		double bBound = 0.8;

		algorithm.setInputParameter("populationSize", popSize);
		algorithm.setInputParameter("maxGenerations", maxGenerations);
		algorithm.setInputParameter("numGaussianSparks", numGaussianSparks);

		algorithm.setInputParameter("comparator", comparator);

		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("numGaussianSparks", numGaussianSparks);
		parameters.put("maxAmplitude", maxAmplitude);

		parameters.put("aBound", aBound);
		parameters.put("bBound", bBound);
		parameters.put("mParameter", mParameter); // maxSparks

		parameters.put("numLocations", popSize);
		parameters.put("comparator", comparator);

		explosion = new FireworkMutation(parameters);
		parameters = new HashMap<String, Object>();
	    parameters.put("probability", 1.0/problem.getNumberOfVariables()) ;
	    parameters.put("distributionIndex", 20.0) ;
		gaussian = MutationFactory.getMutationOperator("PolynomialMutation", parameters);
		selection = new FireworksSelection(null);

		algorithm.addOperator("selection", selection);
		algorithm.addOperator("explosion", explosion);
		algorithm.addOperator("gaussian", gaussian);

		// Execute the Algorithm
		long initTime = System.currentTimeMillis();
		SolutionSet population = algorithm.execute();
		long estimatedTime = System.currentTimeMillis() - initTime;

		// Result messages
		System.out.println("Total execution time: " + estimatedTime + "ms");
		System.out.println("Objectives values have been writen to file FUN");
		population.printObjectivesToFile("FUN");

	}

}
