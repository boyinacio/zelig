package zelig.test;

import java.util.HashMap;

import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;
import zelig.util.distances.CoassociationDistance;

public class TestCoassiationMatrix {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Instances baseDados = null;

		try {
			DataSource source = new DataSource("/opt/weka-3-6-6/data/iris.arff");
			baseDados = source.getDataSet();
			baseDados.setClassIndex(baseDados.numAttributes() - 1);
		} catch (Exception e) {
			System.out.println("Não foi possível carregar a base!!\n\n");
			e.printStackTrace();
			System.exit(1);
		}

		Instances[] partitions = { baseDados, baseDados, baseDados };

		HashMap<Instance, Integer> instanceIndexMap = new HashMap<Instance, Integer>();

		for (int i = 0; i < baseDados.numInstances(); i++) {
			instanceIndexMap.put(baseDados.instance(i), i);
		}

		CoassociationDistance measure = new CoassociationDistance(partitions,
				baseDados, instanceIndexMap, false);

		for (int i = 0; i < baseDados.numInstances(); i++) {
			for (int j = 0; j < baseDados.numInstances(); j++) {
				System.out.printf(
						"%.2f\t",
						measure.distance(baseDados.instance(i),
								baseDados.instance(j)));
			}
			System.out.println();
		}

	}

}
