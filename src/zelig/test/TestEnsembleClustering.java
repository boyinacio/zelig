package zelig.test;

import jmetal.util.PseudoRandom;
import weka.clusterers.DBSCAN;
import weka.clusterers.EM;
import weka.clusterers.SimpleKMeans;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;
import zelig.clusteringEnsemble.ClusteringEnsembleRunner;
import zelig.clusteringEnsemble.consensusFunctions.CoassociationMatrix;
import zelig.util.clusteringEvaluators.AdjustedRandIndex;
import zelig.util.clusteringEvaluators.BasicDaviesBouldin;

public class TestEnsembleClustering {
	
	public static Instances dataset, result;
		
	public static void main(String[] args) throws Exception {
		String base = "iris";
		String path = base + ".arff";
		
		System.out.print("Carregando base de dados... ");
		
		// Load dataset 
		// ==================================================
		loadDataset(path);
		
		// ==================================================
		
		System.out.print("Carregada!\nCarregando algoritmos... ");
		
		// CLUSTERING ALGORITHMS
		// ==================================================
		// K-MEANS
		SimpleKMeans kmeans = new SimpleKMeans();
		kmeans.setPreserveInstancesOrder(true);
		kmeans.setNumClusters(dataset.numClasses());
		kmeans.setSeed(PseudoRandom.randInt(1000, 10000));
				
		// DBSCAN
		DBSCAN dbscan = new DBSCAN();
		dbscan.setEpsilon(0.4);
		
		// EM
		EM em = new EM();
		em.setNumClusters(dataset.numClasses());
		em.setSeed(PseudoRandom.randInt(1000, 10000));
		// ==================================================
		
		System.out.print("Carregados!\nCarregando Função de Consenso... ");
		
		// CONSENSUS FUNCTION
		// ==================================================
		CoassociationMatrix CM = new CoassociationMatrix();
		CM.setOriginalDataset(dataset);
		// ==================================================
		
		System.out.print("Carregada!\nCarregando o Executor... ");
		
		// RUNNER
		// ==================================================
		ClusteringEnsembleRunner runner = new ClusteringEnsembleRunner(CM, dataset);
//		runner.addAlgorithm(kmeans);
		runner.addAlgorithm(dbscan);
		runner.addAlgorithm(em);
		// ==================================================
		
		System.out.print("Carregado!\nExecutando... ");
		
		// EXECUTES RUNNER
		// ==================================================
		runner.run();
		result = runner.getFinalPartition();
		
		System.out.println("==============================================");
		System.out.println(result);
		System.out.println("==============================================");
						
		System.out.print("Acabou!\nÍndices\n");
		System.out.println("RAND: " + AdjustedRandIndex.ARI(dataset, result));
		System.out.println("DB: " + BasicDaviesBouldin.main(result));
		// ==================================================		
	}
	
	public static void loadDataset(String path) {
		try {
			DataSource source = new DataSource(path);
			dataset = source.getDataSet();
			dataset.setClassIndex(dataset.numAttributes() - 1);
		} catch (Exception e) {
			System.out.println("Não foi possível carregar a base!!\n\n");
			e.printStackTrace();
			System.exit(1);
		}
	}

}
