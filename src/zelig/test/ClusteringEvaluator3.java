package zelig.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

import weka.core.Instances;
import zelig.util.clusteringEvaluators.CalinskiHarabaszIndex;
import zelig.util.clusteringEvaluators.DomIndex;
import zelig.util.clusteringEvaluators.DunnIndex;
import zelig.util.clusteringEvaluators.JaccardCoefficient;

public class ClusteringEvaluator3 {

	public static String calculaIndices(String base, String abordagem,
			ArrayList<Instances> bases, Instances original) throws IOException {
		String saidaArquivo = "";

		saidaArquivo = "Dunn,CH,Dom,Jaccard\n";

		saidaArquivo += geraResultados(original, bases);

//		System.out.print("Salvando em arquivo... ");
//
//		File file = new File("resultados_" + base + "_" + abordagem + ".txt");

//		// if file doesnt exists, then create it
//		if (!file.exists()) {
//			file.createNewFile();
//		}
//
//		FileWriter fw = new FileWriter(file.getAbsoluteFile());
//		BufferedWriter bw = new BufferedWriter(fw);
//		bw.write(saidaArquivo);
//		bw.close();
//
//		System.out.print("Salvo.\n");
		
		return saidaArquivo;
	}

	public static String geraResultados(Instances original,
			ArrayList<Instances> bases) throws IOException {

		String saida = "";

//		System.out.print("Calculando Índices... ");

		for(Instances base : bases){
			saida += with3DecimalPlaces(DunnIndex.calculate(base)) + ",";
			saida += with3DecimalPlaces(CalinskiHarabaszIndex.calculate(base))
					+ ",";
			saida += with3DecimalPlaces(DomIndex.calculate(original, base))
					+ ",";
			saida += with3DecimalPlaces(JaccardCoefficient.calculate(original,
					base)) + "\n";
		}
		
//		System.out.print("Calculados\n");

		return saida;
	}

	public static String with3DecimalPlaces(double value) {
		if ((value >= Double.MIN_VALUE) && (value <= Double.MAX_VALUE)) {
			return String.format(Locale.ENGLISH, "%.3f", value);
		}
		return "0";
	}

}
