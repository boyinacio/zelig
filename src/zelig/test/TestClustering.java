package zelig.test;

import jmetal.util.MersenneTwisterFast;
import weka.clusterers.SimpleKMeans;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;

public class TestClustering {

	public TestClustering() {
	}
	
	public static void main(String[] args) throws Exception{
		String base = "iris";
		Instances baseDados = null;
		
		try {
			DataSource source = new DataSource(base + ".arff");
			baseDados = source.getDataSet();
			baseDados.setClassIndex(baseDados.numAttributes() - 1);
		} catch (Exception e) {
			System.out.println("Não foi possível carregar a base!!\n\n");
			e.printStackTrace();
			System.exit(1);
		}	
						
		SimpleKMeans kmeans = new SimpleKMeans();
		kmeans.setPreserveInstancesOrder(true);
		
		Instances copy = null;
		Remove filter = new Remove();
		
		try {
			filter.setAttributeIndices("" + (baseDados.classIndex() + 1));
			filter.setInputFormat(baseDados);
			copy = Filter.useFilter(baseDados, filter);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}
		
		try {
			kmeans.setNumClusters(3);
			kmeans.setSeed(1000 + new MersenneTwisterFast().nextInt(10000));
			kmeans.buildClusterer(copy);
		} catch (Exception e3) {
			e3.printStackTrace();
			System.exit(1);
		}
		
		System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
		
		System.out.println(kmeans.clusterInstance(copy.instance(3)));
		
		System.out.println(copy);
		
	}

}
