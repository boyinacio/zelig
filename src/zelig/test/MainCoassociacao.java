package zelig.test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Formatter;
import java.util.FormatterClosedException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Scanner;

import jmetal.util.PseudoRandom;
import weka.clusterers.Clusterer;
import weka.clusterers.DBSCAN;
import weka.clusterers.EM;
import weka.clusterers.HierarchicalClusterer;
import weka.clusterers.SimpleKMeans;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ArffSaver;
import weka.core.converters.ConverterUtils.DataSource;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;
import zelig.util.ClustererAssigner;
import zelig.util.distances.CoassociationDistance;
import zelig.util.translators.ClusterNumberTranslator;

public class MainCoassociacao {

	public static void main(String[] args) {
		Scanner reader = null;
		File F;

		try {
			F = new File("bases.txt");
			reader = new Scanner(F);
		} catch (FileNotFoundException e) {
			System.err.println("Error opening file.");
			System.exit(1);
		}

		try {
			LinkedList<String> bases = new LinkedList<String>();

			while (reader.hasNext()) {
				bases.add(reader.nextLine());
			}
			for (int i = 0; i < 10; i++) {
				runForBase(bases, i);
			}

			// runForBase(bases);
		} catch (NoSuchElementException elementException) {
			System.err.println("File improperly formed.");
			elementException.printStackTrace();
			reader.close();
			System.exit(1);
		} catch (IllegalStateException stateException) {
			System.err.println("Error reading from file.");
			System.exit(1);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}

		reader.close();

	}

	private static Instances runExecution(Instances[] bases_entrada,
			Instances baseOriginal,
			HashMap<Instance, Integer> instanceIndexMap, int numExecucao,
			int numeroGrupo, String directory) throws Exception {
		ArrayList<Instances> lista_auxiliar = new ArrayList<Instances>();

		for (int i = 0; i < bases_entrada.length; i++) {
			lista_auxiliar.addAll(Arrays.asList(gerarAgrupamentos(
					bases_entrada[i], bases_entrada, 2)));
		}

		Instances[] bases_coassociacao = new Instances[lista_auxiliar.size()];

		for (int i = 0; i < bases_coassociacao.length; i++) {
			bases_coassociacao[i] = lista_auxiliar.get(i);
		}

		CoassociationDistance distance = new CoassociationDistance(
				bases_coassociacao, baseOriginal, instanceIndexMap, false);

		// Clusteriza a base com base no hierarquico
		// -------------------------------------------------------------
		HierarchicalClusterer clusterer = new HierarchicalClusterer();

		clusterer.setDistanceFunction(distance);
		clusterer.setNumClusters(bases_entrada[0].numClasses());
		clusterer.buildClusterer(baseOriginal);

		ClusterNumberTranslator translator = new ClusterNumberTranslator();
		int[] labels = new int[baseOriginal.numInstances()];
		int sz = clusterer.numberOfClusters();

		for (int i = 0; i < baseOriginal.numInstances(); i++) {
			try {
				labels[i] = clusterer.clusterInstance(baseOriginal.instance(i));
			} catch (Exception e) {
				labels[i] = sz + 1;
			}
		}

		baseOriginal = translator.translate(labels, baseOriginal);
		// -------------------------------------------------------------
		// /** // código para mostrar o resultado da Coaassociação
		String nomeAlgoritmo = "Coassociacao";
		String nomeBase = "Wine";
		String tamanhoComite = "18";
		// String nomeIndice = "MX";
		// String tamanhoComite = "12";
		// String filename = nomeAlgoritmo + "_" + datasetCopy.relationName() +
		// ".arff";
		// String filename = nomeAlgoritmo + "_execucao_" + numExecucao + "_" +
		// "solucao" + "_" + i + "_" + datasetCopy.relationName() + ".arff";
										
		String filename = nomeAlgoritmo + "_execucao_" + numExecucao + "_"
				+ numeroGrupo + "_" + tamanhoComite + "_" + nomeBase + ".arff";
		ArffSaver saver = new ArffSaver();
		saver.setInstances(baseOriginal);

		try {
			saver.setFile(new File(directory+"/"+filename));
			saver.writeBatch();
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}// **/
			// -------------------------------------------------------------

		// Result messages
		// -------------------------------------------------------------
		// double db = BasicDaviesBouldin.main(datasetCopy);
		// double dom = DomIndex.calculate(dataset, datasetCopy);
		// double jaccard = JaccardCoefficient.calculate(dataset, datasetCopy);
		// double dunn = DunnIndex.calculate(datasetCopy, new JCDistance());
		// double ari = AdjustedRandIndex.ARI(bases_entrada[0], baseOriginal);
		// System.out.println("DB da coassociação: " + db);
		// System.out.println("RAND da coassociação: " + ari);

		return baseOriginal;
	}

	private static void runForBase(LinkedList<String> bases, int numExecucao)
			throws Exception {
		Instances[] bases_entrada = new Instances[bases.size()];
		String resultadosSalvar = "";
		String abordagem = "";

		for (int i = 0; i < bases_entrada.length; i++) {
			bases_entrada[i] = loadDataset(bases.get(i));
		}

		Instances baseOriginal = new Instances(bases_entrada[0]);
		HashMap<Instance, Integer> instanceIndexMap = new HashMap<Instance, Integer>();

		for (int i = 0; i < baseOriginal.numInstances(); i++) {
			instanceIndexMap.put(baseOriginal.instance(i), i);
		}

		ArrayList<Instances> resultados = new ArrayList<Instances>();
		
		for (int grupos = 2; grupos <= 10; grupos++) {
			
			String nomeAlgoritmo = "CRO";
			String nomeBase = "Wine";
			String nomeIndice = "DB";
			String tamanhoComite = "18";
			
			String directory = nomeBase + "_" + nomeAlgoritmo + "_" + nomeIndice + "-" + grupos;
			File dir = new File(directory);
			dir.mkdir();
						
			for (int execucoes = 0; execucoes < 10; execucoes++) {
				resultados.add(runExecution(bases_entrada, baseOriginal,
						instanceIndexMap, execucoes, grupos,directory));
			}

			abordagem = nomeBase + "_" + nomeAlgoritmo + "_" + nomeIndice
					+ "_Comite_" + tamanhoComite + "_Grupos_" + grupos;

			System.out.println(abordagem);
			
			String resultadosIndices = ClusteringEvaluator3.calculaIndices(nomeBase,
					abordagem, resultados, bases_entrada[0]);
			
			System.out.println(resultadosIndices);
			
			resultadosSalvar += resultadosIndices;
		}
		
		Formatter saver = null;

		try {
			saver = new Formatter("resultados_" + abordagem + ".txt");
		} catch (SecurityException securityException) {
			System.err.println("You do not have write access to this file.");
			System.exit(1);
		} catch (FileNotFoundException e) {
			System.err.println("Error opening or creating file.");
			System.exit(1);
		}

		try {
			saver.format("%s\n", resultadosSalvar);
		} catch (FormatterClosedException formatterClosedException) {
			System.err.println("Error writing to file.");
			return;
		} catch (NoSuchElementException elementException) {
			System.err.println("Invalid input. Please try again.");
			return;
		}

		saver.close();
	}

	public static Instances loadDataset(String base) {
		Instances dataset = null;

		try {
			DataSource source = new DataSource("bases/" + base + ".arff");
			dataset = source.getDataSet();
			dataset.setClassIndex(dataset.numAttributes() - 1);
		} catch (Exception e) {
			System.out.println("Não foi possível carregar a base!!\n\nbase");
			e.printStackTrace();
			System.exit(1);
		}

		return dataset;
	}

	public static Instances[] gerarAgrupamentos(Instances base,
			Instances[] entrada, int numGrupos) throws Exception {
		Instances[] particoes = new Instances[3 * entrada.length];
		Clusterer agrupador = null;
		SimpleKMeans kmeans;
		EM em;
		HierarchicalClusterer hierarquico;
		DBSCAN dbscan;

		ClustererAssigner assigner = new ClustererAssigner();

		// int numGrupos = base.numClasses();
		for (int i = 0; i <= (particoes.length - 3); i += 3) {
			particoes[i] = new Instances(base);
			particoes[i + 1] = new Instances(base);
			particoes[i + 2] = new Instances(base);

			for (int j = 0; j < 3; j++) {
				Remove filter = new Remove();
				try {
					filter.setAttributeIndices(""
							+ (particoes[i + j].classIndex() + 1));
					filter.setInputFormat(particoes[i + j]);
					particoes[i + j] = Filter.useFilter(particoes[i + j],
							filter);
				} catch (Exception e) {
					e.printStackTrace();
					System.exit(-1);
				}

				switch (j) {
				/**
				 * case 0: // Saída do K-Means kmeans = new SimpleKMeans();
				 * kmeans.setPreserveInstancesOrder(true);
				 * kmeans.setNumClusters(5);
				 * kmeans.setSeed(PseudoRandom.randInt(1000, 10000)); agrupador
				 * = kmeans; kmeans.setDistanceFunction(new
				 * EuclideanDistance(particoes[i + j])); break;*
				 */
				case 0:
					/**
                         *
                         */
					kmeans = new SimpleKMeans();
					kmeans.setPreserveInstancesOrder(true);
					kmeans.setNumClusters(numGrupos);
					kmeans.setSeed(PseudoRandom.randInt(1000, 10000));
					agrupador = kmeans;
					break;

				case 1: // Saída do EM
					em = new EM();
					em.setNumClusters(numGrupos);
					em.setSeed(PseudoRandom.randInt(1000, 10000));
					agrupador = em;
					break;

				/**
				 * dbscan = new DBSCAN(); dbscan.setEpsilon(0.5);
				 * dbscan.setMinPoints(3); agrupador = dbscan; break;*
				 */
				default: // Saída do Hierárquico
					hierarquico = new HierarchicalClusterer();
					hierarquico.setNumClusters(numGrupos);
					agrupador = hierarquico;
					// hierarquico.setDistanceFunction(new
					// EuclideanDistance(particoes[i + j]));
					break;
				}

				agrupador.buildClusterer(particoes[i + j]);
				particoes[i + j] = assigner.assignFromClusterer(agrupador,
						particoes[i + j]);
			}
		}

		return particoes;
	}

}
