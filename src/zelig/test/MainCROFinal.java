package zelig.test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.FormatterClosedException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

import jmetal.core.Algorithm;
import jmetal.core.Solution;
import jmetal.core.SolutionSet;
import jmetal.core.Variable;
import jmetal.metaheuristics.cro.MaxCRO;
import jmetal.operators.crossover.KMeansOperator;
import jmetal.operators.mutation.DistanceBasedMutation;
import jmetal.operators.selection.MaxRouletteWheelSelection;
import jmetal.problems.clustering.EnsembleProblem;
import jmetal.util.Configuration;
import jmetal.util.JMException;
import jmetal.util.comparators.SingleObjectiveComparatorMaximum;
import weka.clusterers.HierarchicalClusterer;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ArffSaver;
import weka.core.converters.ConverterUtils.DataSource;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;
import zelig.util.distances.CoassociationDistance;
import zelig.util.distances.Distance;
import zelig.util.distances.JoaoCarlosDistance;
import zelig.util.translators.ClusterNumberTranslator;

public class MainCROFinal {

	public static Logger logger_; // Logger object
	public static FileHandler fileHandler_; // FileHandler object
	// public static Instances dataset;
	public static Instances[] particoes, novasParticoes;

	public static Instances loadDataset(String base) {
		Instances dataset = null;

		try {
			DataSource source = new DataSource("bases/" + base + ".arff");
			dataset = source.getDataSet();
			dataset.setClassIndex(dataset.numAttributes() - 1);
		} catch (Exception e) {
			System.out.println("Não foi possível carregar a base!!\n\nbase");
			e.printStackTrace();
			System.exit(1);
		}

		return dataset;
	}

	public static Instances translate(Instances dataset, Solution solution)
			throws JMException {
		Variable[] vars = solution.getDecisionVariables(); // Cromossomo
		int[] rotulos = new int[vars.length];

		for (int i = 0; i < rotulos.length; i++) {
			rotulos[i] = ((Double) vars[i].getValue()).intValue();
			rotulos[i] = ((Double) (particoes[rotulos[i]].instance(i)
					.classValue())).intValue();
		}

		return new ClusterNumberTranslator().translate(rotulos, dataset);
	}

	public static void saveResults(double dbIndex, String base) {
		Formatter saver = null;

		try {
			saver = new Formatter(base + "_COASSOCIACAO_DB");
		} catch (SecurityException securityException) {
			System.err.println("You do not have write access to this file.");
			System.exit(1);
		} catch (FileNotFoundException e) {
			System.err.println("Error opening or creating file.");
			System.exit(1);
		}

		try {
			saver.format("%.10f\n", dbIndex);
		} catch (FormatterClosedException formatterClosedException) {
			System.err.println("Error writing to file.");
			return;
		} catch (NoSuchElementException elementException) {
			System.err.println("Invalid input. Please try again.");
			return;
		}

		saver.close();

	}

	public static void configureCRO(Instances dataset, Algorithm algorithm)
			throws JMException {
		KMeansOperator crossover; // Crossover operator
		DistanceBasedMutation mutation; // Mutation operator
		MaxRouletteWheelSelection selection;// Selection operator

		int reefSizeM = 10;
		int reefSizeN = 10;
		int generations = 500;
		int opportunitiesToSettle = 3;

		double broadcastProb = 0.9;
		double predationProb = 0.1;
		double fractionCoralsDepredated = 0.1;
		double freeOccupiedProportion = 0.6;
		double EPS = 0.8;
		double probability = 0.05;

		Distance distance = new JoaoCarlosDistance();

		algorithm.setInputParameter("generations", generations);

		algorithm.setInputParameter("reefSizeM", reefSizeM);
		algorithm.setInputParameter("reefSizeN", reefSizeN);

		algorithm.setInputParameter("broadcastProb", broadcastProb);
		algorithm.setInputParameter("predationProb", predationProb);

		algorithm.setInputParameter("fractionCoralsDepredated",
				fractionCoralsDepredated);
		algorithm.setInputParameter("freeOccupiedProportion",
				freeOccupiedProportion);

		algorithm.setInputParameter("opportunitiesToSettle",
				opportunitiesToSettle);

		HashMap<String, Object> parameters;

		parameters = new HashMap<String, Object>();
		parameters.put("base", dataset);
		parameters.put("distance", distance);
		parameters.put("EPS", EPS);
		parameters.put("HMRC", 0.3);
		parameters.put("comparator", new SingleObjectiveComparatorMaximum());
		// parameters.put("translator", new ClusterNumberTranslator());
		// parameters.put("problem", problem);
		// crossover = new HMCRCrossover(parameters);
		crossover = new KMeansOperator(parameters);
		// crossover = new GKAKMeansOperator(parameters);
		// crossover = new DensityCrossover(parameters);
		// crossover = new StringCrossover(parameters);
		// crossover = new UniformCrossover(parameters);
		// crossover = new HeritageCrossover(parameters);

		parameters = new HashMap<String, Object>();
		parameters.put("base", dataset);
		parameters.put("distance", distance);
		parameters.put("probability", probability);
		parameters.put("PAR", 0.4);
		// mutation = new PARMutation(parameters);
		mutation = new DistanceBasedMutation(parameters);

		// parameters.put("initialProbability", 0.4);
		// parameters.put("finalProbability", 0.01);
		// parameters.put("generations", 500);
		// mutation = new GAKCMutation(parameters);
		parameters = new HashMap<String, Object>();
		selection = new MaxRouletteWheelSelection(parameters);

		// selection = new RouletteWhellSelection(parameters);
		// selection = new GKARouletteWhellSelection(parameters);
		algorithm.addOperator("crossover", crossover);
		algorithm.addOperator("mutation", mutation);
		algorithm.addOperator("selection", selection);

	}

	public static void saveLabelsFromCoassociation(Instances I, String base) {
		Formatter saver = null;

		try {
			saver = new Formatter(base + "_COASSOCIACAO_CROMOSSOMO");
		} catch (SecurityException securityException) {
			System.err.println("You do not have write access to this file.");
			System.exit(1);
		} catch (FileNotFoundException e) {
			System.err.println("Error opening or creating file.");
			System.exit(1);
		}

		try {
			for (int j = 0; j < I.numInstances(); j++) {

				int x = (int) (I.instance(j).classValue() - (I.instance(j)
						.classValue() % 1));

				saver.format("%d\n", x);
			}
		} catch (FormatterClosedException formatterClosedException) {
			System.err.println("Error writing to file.");
			return;
		} catch (NoSuchElementException elementException) {
			System.err.println("Invalid input. Please try again.");
			return;
		}

		saver.close();

	}

	public static Instances runExecution(Algorithm algorithm,
			Instances[] bases_entrada, int numExecucao, int numeroGrupo,
			String directory) throws Exception {
		// -------------------------------------------------------------
		// Executa o algoritmo
		// -------------------------------------------------------------
		long initTime = System.currentTimeMillis();
		SolutionSet population = algorithm.execute();
		long estimatedTime = System.currentTimeMillis() - initTime;
		// -------------------------------------------------------------

		// Aplica a coassiociação nas soluções
		// -------------------------------------------------------------
		novasParticoes = new Instances[population.size()];
		Instances datasetCopy = new Instances(bases_entrada[0]);
		datasetCopy.setClassIndex(-1);

		Remove filter = new Remove();
		try {
			filter.setAttributeIndices("" + (bases_entrada[0].classIndex() + 1));
			filter.setInputFormat(bases_entrada[0]);
			datasetCopy = Filter.useFilter(bases_entrada[0], filter);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}

		int best = 0;

		for (int i = 0; i < novasParticoes.length; i++) {
			novasParticoes[i] = translate(bases_entrada[0], population.get(i));
			/**
			 * boolean conditionMin = (i > 0) &&
			 * (population.get(i).getObjective(0) <= population.get(i -
			 * 1).getObjective(0));
			 * 
			 * boolean conditionMax = !conditionMin;
			 * 
			 * if (conditionMin) { best = i; } String nomeAlgoritmo = "CRO";
			 * String filename = nomeAlgoritmo + "_" + "solucao" + "_" + i + "_"
			 * + datasetCopy.relationName() + ".arff";
			 * 
			 * ArffSaver saver = new ArffSaver();
			 * saver.setInstances(novasParticoes[i]); try { saver.setFile(new
			 * File(filename)); saver.writeBatch(); } catch (IOException e) {
			 * e.printStackTrace(); System.exit(1); }
			 **/
		}

		/**
		 * /// Salvando a melhor solucao String nomeAlgoritmo = "CRO"; String
		 * filename = nomeAlgoritmo + "_" + "melhor_solucao" + "_" +
		 * datasetCopy.relationName() + ".arff";
		 *
		 * ArffSaver saver = new ArffSaver();
		 * saver.setInstances(novasParticoes[best]); try { saver.setFile(new
		 * File(filename)); saver.writeBatch(); } catch (IOException e) {
		 * e.printStackTrace(); System.exit(1); }*
		 */
		datasetCopy = new Instances(bases_entrada[0]);
		HashMap<Instance, Integer> instanceIndexMap = new HashMap<Instance, Integer>();

		for (int i = 0; i < datasetCopy.numInstances(); i++) {
			instanceIndexMap.put(datasetCopy.instance(i), i);
		}

		CoassociationDistance distance = new CoassociationDistance(
				novasParticoes, datasetCopy, instanceIndexMap, false);

		// Clusteriza a base com base no hierarquico
		// -------------------------------------------------------------
		HierarchicalClusterer clusterer = new HierarchicalClusterer();

		clusterer.setDistanceFunction(distance);
		clusterer.setNumClusters(bases_entrada[0].numClasses());
		clusterer.buildClusterer(datasetCopy);

		ClusterNumberTranslator translator = new ClusterNumberTranslator();
		int[] labels = new int[datasetCopy.numInstances()];
		int sz = clusterer.numberOfClusters();

		for (int i = 0; i < datasetCopy.numInstances(); i++) {
			try {
				labels[i] = clusterer.clusterInstance(datasetCopy.instance(i));
			} catch (Exception e) {
				labels[i] = sz + 1;
			}
		}

		datasetCopy = translator.translate(labels, datasetCopy);

		// /** // código para mostrar o resultado da Coaassociação
		String nomeAlgoritmo = "CRO";
		String nomeBase = "Wine";
		String nomeIndice = "DB";
		String tamanhoComite = "18";
		// String filename = nomeAlgoritmo + "_" + datasetCopy.relationName() +
		// ".arff";
		// String filename = nomeAlgoritmo + "_execucao_" + numExecucao + "_" +
		// "solucao" + "_" + i + "_" + datasetCopy.relationName() + ".arff";

		String filename = nomeAlgoritmo + "_execucao_" + numExecucao
				+ "_coassociacao_" + "_" + numeroGrupo + "_" + nomeIndice + "_"
				+ tamanhoComite + "_" + nomeBase + ".arff";
		ArffSaver saver = new ArffSaver();
		saver.setInstances(datasetCopy);

		try {
			saver.setFile(new File(directory + "/" + filename));
			saver.writeBatch();
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}// **/

		// -------------------------------------------------------------
		// -------------------------------------------------------------
		// Result messages
		// -------------------------------------------------------------
		// double db = BasicDaviesBouldin.main(datasetCopy);
		// double dom = DomIndex.calculate(dataset, datasetCopy);
		// double jaccard = JaccardCoefficient.calculate(dataset, datasetCopy);
		// double jaccard = JaccardCoefficient.calculate(bases_entrada[0],
		// datasetCopy);
		// double dunn = DunnIndex.calculate(datasetCopy, new JCDistance());
		// double ari = AdjustedRandIndex.ARI(bases_entrada[0], datasetCopy);
		// System.out.println("Jaccard da coassociação: " + jaccard);
		logger_.info("Total execution time: " + estimatedTime + "ms");

		// population.printObjectivesToFile(bases_entrada[0].relationName());
		// logger_.info(bases_entrada[0].relationName());

		// saveResults(jaccard, bases_entrada[0].relationName());
		// logger_.info("Coassociation DB Index value have been writen to file COASSOCIACAO_DB");
		// saveLabelsFromCoassociation(datasetCopy,base);
		// logger_.info("Coassociation labels have been writen to file COASSOCIACAO_CROMOSSOMO");
		// -------------------------------------------------------------

		return datasetCopy;

	}

	public static double[] runForBase(List<String> bases, int numGrupos)
			throws Exception {
		logger_ = Configuration.logger_;
		fileHandler_ = new FileHandler("Ensemble_Clusering_plus_NSGAII.log");
		logger_.addHandler(fileHandler_);

		// Instacia e configura o algoritmo e o problema
		// -------------------------------------------------------------
		EnsembleProblem problem; // The problem to solve
		Algorithm algorithm; // The algorithm to use

		Instances[] bases_entrada = new Instances[bases.size()];

		for (int i = 0; i < bases_entrada.length; i++) {
			bases_entrada[i] = loadDataset(bases.get(i));
		}

		problem = new EnsembleProblem(bases_entrada, numGrupos);
		particoes = problem.getParticoes();

		Instances baseOriginal = new Instances(bases_entrada[0]);

		algorithm = new MaxCRO(problem);
		configureCRO(bases_entrada[0], algorithm);

		ArrayList<Instances> resultados = new ArrayList<Instances>();

		String nomeAlgoritmo = "CRO";
		String nomeIndice = "DB";
		int tamanhoComite = 18;
		String nomeBase = "Wine";

		for (int numExecucao = 0; numExecucao < 10; numExecucao++) {
			String directory = nomeBase + "_" + nomeAlgoritmo + "_"
					+ nomeIndice + "-" + numGrupos;
			File dir = new File(directory);
			dir.mkdir();

			resultados.add(runExecution(algorithm, bases_entrada, numExecucao,
					numGrupos, directory));
		}

		return ClusteringEvaluator4.calculaIndices(resultados, baseOriginal);
	}

	public static void main(String[] args) throws Exception {
		Scanner reader = null;
		File F;

		try {
			F = new File("bases.txt");
			reader = new Scanner(F);
		} catch (FileNotFoundException e) {
			System.err.println("Error opening file.");
			System.exit(1);
		}

		try {
			LinkedList<String> bases = new LinkedList<String>();

			while (reader.hasNext()) {
				bases.add(reader.nextLine());
			}

			String abordagem = "Digite o nome da abordagem aqui";

			String resultados = abordagem + "\t";

			for (int numGrupos = 2; numGrupos <= 10; numGrupos++) {
				resultados += numGrupos + "\t";
			}

			resultados += "\n\t";

			ArrayList<double[]> resultadosPorNumero = new ArrayList<double[]>();

			for (int numGrupos = 2; numGrupos <= 10; numGrupos++) {
				resultadosPorNumero.add(runForBase(bases, numGrupos));
			}

			for (int i = 0; i < 4; i++) {
				for (double[] resultado : resultadosPorNumero) {
					resultados += resultado[i] + "\t";
				}
				resultados += "\n\t";
			}

			Formatter saver = null;

			saver = new Formatter("resultados_" + abordagem + ".txt");
			saver.format("%s\n", resultados);
			saver.close();

		} catch (SecurityException securityException) {
			System.err.println("You do not have write access to this file.");
			System.exit(1);
		} catch (FileNotFoundException e) {
			System.err.println("Error opening or creating file.");
			System.exit(1);
		} catch (FormatterClosedException formatterClosedException) {
			System.err.println("Error writing to file.");
			return;
		} catch (NoSuchElementException elementException) {
			System.err.println("Invalid input. Please try again.");
			return;
		} catch (IllegalStateException stateException) {
			System.err.println("Error reading from file.");
			System.exit(1);
		}

		reader.close();

	} // main
} // NSGAII_main
