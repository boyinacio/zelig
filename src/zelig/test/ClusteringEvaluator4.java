package zelig.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

import weka.core.Instances;
import zelig.util.clusteringEvaluators.CalinskiHarabaszIndex;
import zelig.util.clusteringEvaluators.DomIndex;
import zelig.util.clusteringEvaluators.DunnIndex;
import zelig.util.clusteringEvaluators.JaccardCoefficient;

public class ClusteringEvaluator4 {

	public static double[] calculaIndices(ArrayList<Instances> bases,
			Instances original) throws IOException {
		return geraResultados(original, bases);
	}

	public static double[] geraResultados(Instances original,
			ArrayList<Instances> bases) throws IOException {

		double dunn, ch, dom, jaccard;
		dunn = 0;
		ch = 0;
		dom = 0;
		jaccard = 0;

		for (Instances base : bases) {
			dunn += DunnIndex.calculate(base);
			ch += CalinskiHarabaszIndex.calculate(base);
			dom += DomIndex.calculate(original, base);
			jaccard += JaccardCoefficient.calculate(original, base);
		}

		dunn = dunn / bases.size();
		ch = ch / bases.size();
		dom = dom / bases.size();
		jaccard = jaccard / bases.size();

		// saida = with3DecimalPlaces(dunn) + "\t" + with3DecimalPlaces(ch) +
		// "\t"
		// + with3DecimalPlaces(dom) + "\t" + with3DecimalPlaces(jaccard);

		// for(Instances base : bases){
		// saida += with3DecimalPlaces(DunnIndex.calculate(base)) + ",";
		// saida += with3DecimalPlaces(CalinskiHarabaszIndex.calculate(base))
		// + ",";
		// saida += with3DecimalPlaces(DomIndex.calculate(original, base))
		// + ",";
		// saida += with3DecimalPlaces(JaccardCoefficient.calculate(original,
		// base)) + "\n";
		// }

		// System.out.print("Calculados\n");

		double[] medias = { dunn, ch, dom, jaccard };

		return medias;
	}

	public static String with3DecimalPlaces(double value) {
		if ((value >= Double.MIN_VALUE) && (value <= Double.MAX_VALUE)) {
			return String.format(Locale.ENGLISH, "%.3f", value);
		}
		return "0";
	}

}
