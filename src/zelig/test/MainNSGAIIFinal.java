/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zelig.test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.FormatterClosedException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

import jmetal.core.Algorithm;
import jmetal.core.Operator;
import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.core.SolutionSet;
import jmetal.core.Variable;
import jmetal.metaheuristics.nsgaII.NSGAII;
import jmetal.operators.crossover.CrossoverFactory;
import jmetal.operators.mutation.MutationFactory;
import jmetal.operators.selection.SelectionFactory;
import jmetal.problems.clustering.EnsembleProblem;
import jmetal.util.Configuration;
import jmetal.util.JMException;
import weka.clusterers.HierarchicalClusterer;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ArffSaver;
import weka.core.converters.ConverterUtils;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;
import zelig.util.distances.CoassociationDistance;
import zelig.util.translators.ClusterNumberTranslator;

/**
 *
 * @author Huliane
 */
public class MainNSGAIIFinal {

	public static Logger logger_; // Logger object
	public static FileHandler fileHandler_; // FileHandler object
	public static Instances dataset;
	public static Instances[] particoes, novasParticoes;

	public static Instances loadDataset(String base) {
		Instances dataset = null;

		try {
			ConverterUtils.DataSource source = new ConverterUtils.DataSource(
					"bases/" + base + ".arff");
			dataset = source.getDataSet();
			dataset.setClassIndex(dataset.numAttributes() - 1);
		} catch (Exception e) {
			System.out.println("Não foi possível carregar a base!!\n\nbase");
			e.printStackTrace();
			System.exit(1);
		}

		return dataset;
	}

	public static Instances translate(Instances dataset, Solution solution)
			throws JMException {
		Variable[] vars = solution.getDecisionVariables(); // Cromossomo
		int[] rotulos = new int[vars.length];

		for (int i = 0; i < rotulos.length; i++) {
			rotulos[i] = ((Double) vars[i].getValue()).intValue();
			rotulos[i] = ((Double) (particoes[rotulos[i]].instance(i)
					.classValue())).intValue();
		}

		return new ClusterNumberTranslator().translate(rotulos, dataset);
	}

	public static void saveResults(double dbIndex, String base) {
		Formatter saver = null;

		try {
			saver = new Formatter(base + "_COASSOCIACAO_DB");
		} catch (SecurityException securityException) {
			System.err.println("You do not have write access to this file.");
			System.exit(1);
		} catch (FileNotFoundException e) {
			System.err.println("Error opening or creating file.");
			System.exit(1);
		}

		try {
			saver.format("%.10f\n", dbIndex);
		} catch (FormatterClosedException formatterClosedException) {
			System.err.println("Error writing to file.");
			return;
		} catch (NoSuchElementException elementException) {
			System.err.println("Invalid input. Please try again.");
			return;
		}

		saver.close();

	}

	public static void configureNSGAII(Instances dataset, Algorithm algorithm,
			Problem problem) throws JMException {
		Operator crossover; // Crossover operator
		Operator mutation; // Mutation operator
		Operator selection; // Selection operator
		HashMap parameters; // Operator parameters
		// Algorithm parameters
		algorithm.setInputParameter("populationSize", 200);
		algorithm.setInputParameter("maxEvaluations", 25000);
		// Mutation and Crossover for Real codification
		parameters = new HashMap();
		parameters.put("probability", 0.9);
		parameters.put("distributionIndex", 20.0);
		crossover = CrossoverFactory.getCrossoverOperator(
				"SinglePointCrossover", parameters);

		parameters = new HashMap();
		parameters.put("probability", 1.0 / problem.getNumberOfVariables());
		parameters.put("distributionIndex", 20.0);
		mutation = MutationFactory.getMutationOperator("BitFlipMutation",
				parameters);
		// Selection Operator
		parameters = null;
		selection = SelectionFactory.getSelectionOperator("BinaryTournament2",
				parameters);

		// Add the operators to the algorithm
		algorithm.addOperator("crossover", crossover);
		algorithm.addOperator("mutation", mutation);
		algorithm.addOperator("selection", selection);

		// Add the indicator object to the algorithm
		algorithm.setInputParameter("indicators", null);
	}

	public static void saveLabelsFromCoassociation(Instances I, String base) {
		Formatter saver = null;

		try {
			saver = new Formatter(base + "_COASSOCIACAO_CROMOSSOMO");
		} catch (SecurityException securityException) {
			System.err.println("You do not have write access to this file.");
			System.exit(1);
		} catch (FileNotFoundException e) {
			System.err.println("Error opening or creating file.");
			System.exit(1);
		}

		try {
			for (int j = 0; j < I.numInstances(); j++) {

				int x = (int) (I.instance(j).classValue() - (I.instance(j)
						.classValue() % 1));

				saver.format("%d\n", x);
			}
		} catch (FormatterClosedException formatterClosedException) {
			System.err.println("Error writing to file.");
			return;
		} catch (NoSuchElementException elementException) {
			System.err.println("Invalid input. Please try again.");
			return;
		}

		saver.close();

	}

	public static Instances runExecution(Algorithm algorithm,
			Instances[] bases_entrada, int numExecucao, int numeroGrupo)
			throws Exception {
		// -------------------------------------------------------------
		// Executa o algoritmo
		// -------------------------------------------------------------
		long initTime = System.currentTimeMillis();
		SolutionSet population = algorithm.execute();
		long estimatedTime = System.currentTimeMillis() - initTime;
		// -------------------------------------------------------------

		// Aplica a coassiociação nas soluções
		// -------------------------------------------------------------
		novasParticoes = new Instances[population.size()];
		Instances datasetCopy = new Instances(bases_entrada[0]);
		datasetCopy.setClassIndex(-1);

		Remove filter = new Remove();
		try {
			filter.setAttributeIndices("" + (bases_entrada[0].classIndex() + 1));
			filter.setInputFormat(bases_entrada[0]);
			datasetCopy = Filter.useFilter(bases_entrada[0], filter);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}

		for (int i = 0; i < novasParticoes.length; i++) {
			novasParticoes[i] = translate(bases_entrada[0], population.get(i));
		}

		datasetCopy = new Instances(bases_entrada[0]);

		HashMap<Instance, Integer> instanceIndexMap = new HashMap<Instance, Integer>();

		for (int i = 0; i < datasetCopy.numInstances(); i++) {
			instanceIndexMap.put(datasetCopy.instance(i), i);
		}

		CoassociationDistance distance = new CoassociationDistance(
				novasParticoes, datasetCopy, instanceIndexMap, false);

		// Clusteriza a base com base no hierarquico
		// -------------------------------------------------------------
		HierarchicalClusterer clusterer = new HierarchicalClusterer();

		clusterer.setDistanceFunction(distance);
		clusterer.setNumClusters(bases_entrada[0].numClasses());
		clusterer.buildClusterer(datasetCopy);

		ClusterNumberTranslator translator = new ClusterNumberTranslator();
		int[] labels = new int[datasetCopy.numInstances()];
		int sz = clusterer.numberOfClusters();

		for (int i = 0; i < datasetCopy.numInstances(); i++) {
			try {
				labels[i] = clusterer.clusterInstance(datasetCopy.instance(i));
			} catch (Exception e) {
				labels[i] = sz + 1;
			}
		}

		datasetCopy = translator.translate(labels, datasetCopy);
		// -------------------------------------------------------------
		// /** // código para mostrar o resultado da Coaassociação
		String nomeAlgoritmo = "AG";
		String nomeBase = "Wine";
		String nomeIndice = "DB";
		String tamanhoComite = "18";
		// String filename = nomeAlgoritmo + "_" + datasetCopy.relationName() +
		// ".arff";
		// String filename = nomeAlgoritmo + "_execucao_" + numExecucao + "_" +
		// "solucao" + "_" + i + "_" + datasetCopy.relationName() + ".arff";
		String filename = nomeAlgoritmo + "_execucao_" + numExecucao
				+ "_coassociacao_" + "_" + numeroGrupo + "_" + nomeIndice + "_"
				+ tamanhoComite + "_" + nomeBase + ".arff";
		ArffSaver saver = new ArffSaver();
		saver.setInstances(datasetCopy);

		try {
			saver.setFile(new File(filename));
			saver.writeBatch();
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}// **/
			// -------------------------------------------------------------

		// Result messages
		// -------------------------------------------------------------
		// double db = BasicDaviesBouldin.main(datasetCopy);
		// double dom = DomIndex.calculate(dataset, datasetCopy);
		// double jaccard = JaccardCoefficient.calculate(dataset, datasetCopy);
		// double dunn = DunnIndex.calculate(datasetCopy, new JCDistance());
		// double ari = AdjustedRandIndex.ARI(bases_entrada[0], datasetCopy);
		// System.out.println("DB da coassociação: " + db);
		// System.out.println("RAND da coassociação: " + ari);
		logger_.info("Total execution time: " + estimatedTime + "ms");

		// population.printObjectivesToFile(bases_entrada[0].relationName() +
		// "_DB_POP_FINAL");
		// logger_.info("DB indexes from final population cromossomes have been writen to file "
		// + bases_entrada[0].relationName() + "_DB_POP_FINAL");
		// saveResults(ari, bases_entrada[0].relationName());
		// logger_.info("Coassociation DB Index value have been writen to file COASSOCIACAO_DB");
		// saveLabelsFromCoassociation(datasetCopy,base);
		// logger_.info("Coassociation labels have been writen to file COASSOCIACAO_CROMOSSOMO");
		// -------------------------------------------------------------

		return datasetCopy;
	}

	public static String runForBase(List<String> bases, int numGrupos)
			throws Exception {
		logger_ = Configuration.logger_;
		fileHandler_ = new FileHandler("Ensemble_Clusering_plus_NSGAII.log");
		logger_.addHandler(fileHandler_);

		// Instacia e configura o algoritmo e o problema
		// -------------------------------------------------------------
		EnsembleProblem problem; // The problem to solve
		Algorithm algorithm; // The algorithm to use

		Instances[] bases_entrada = new Instances[bases.size()];

		for (int i = 0; i < bases_entrada.length; i++) {
			bases_entrada[i] = loadDataset(bases.get(i));
		}

		// problem = new EnsembleProblem(bases_entrada);
		problem = new EnsembleProblem(bases_entrada, numGrupos);
		particoes = problem.getParticoes();

		algorithm = new NSGAII(problem);
		configureNSGAII(dataset, algorithm, problem);
		// configureNSGAII(algorithm, problem);

		// configureNSGAII(bases_entrada[0],algorithm);

		ArrayList<Instances> resultados = new ArrayList<Instances>();

		for (int numExecucao = 0; numExecucao < 10; numExecucao++) {
			resultados.add(runExecution(algorithm, bases_entrada, numExecucao,
					numGrupos));
		}
		
		String nomeAlgoritmo = "AG";
		String nomeIndice = "DB";
		int tamanhoComite = 18;
		String nomeBase = "Wine";

		String abordagem = nomeBase + "_" + nomeAlgoritmo + "_" + nomeIndice
				+ "_Comite_" + tamanhoComite + "_Grupos_" + numGrupos;

		System.out.println(abordagem);
		
		String resultadosIndices = ClusteringEvaluator3.calculaIndices(nomeBase,
				abordagem, resultados, bases_entrada[0]);
		
		System.out.println(resultadosIndices);
		
		return resultadosIndices;

	}

	public static void main(String[] args) throws Exception {
		Scanner reader = null;
		File F;

		try {
			F = new File("bases.txt");
			reader = new Scanner(F);
		} catch (FileNotFoundException e) {
			System.err.println("Error opening file.");
			System.exit(1);
		}

		try {
			LinkedList<String> bases = new LinkedList<String>();

			while (reader.hasNext()) {
				bases.add(reader.nextLine());
			}

			String resultados = "";

			for (int numGrupos = 2; numGrupos <= 10; numGrupos++) {
				resultados += runForBase(bases, numGrupos);
			}

			Formatter saver = null;

			String abordagem = "Digite o nome da abordagem aqui";

			saver = new Formatter("resultados_" + abordagem + ".txt");
			saver.format("%s\n", resultados);
			saver.close();

		} catch (SecurityException securityException) {
			System.err.println("You do not have write access to this file.");
			System.exit(1);
		} catch (FileNotFoundException e) {
			System.err.println("Error opening or creating file.");
			System.exit(1);
		} catch (FormatterClosedException formatterClosedException) {
			System.err.println("Error writing to file.");
			return;
		} catch (NoSuchElementException elementException) {
			System.err.println("Invalid input. Please try again.");
			return;
		} catch (IllegalStateException stateException) {
			System.err.println("Error reading from file.");
			System.exit(1);
		}

		reader.close();

	} // main
} // NSGAII_main
