package zelig.clusteringEnsemble;

import weka.core.Instances;

/**
 * Interface which implements a Consensus Function. It receives
 * a set of partitions, and returns a new partition, combination
 * of input ones
 *  
 * @author inacio-medeiros
 *
 */
public interface ConsensusFunction {
			
	public Instances combine(Instances[] partitions);

}
