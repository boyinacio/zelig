package zelig.clusteringEnsemble;

import java.util.ArrayList;
import java.util.List;

import weka.clusterers.Clusterer;
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;

/**
 * Implements a Runner that executes a clustering ensemble. When user instantiate this
 * class, it must before set clustering algorithms, dataset, and consensus function
 * for execution of ensemble
 * @author inacio-medeiros
 *
 */
public class ClusteringEnsembleRunner {
	
	private Instances dataset, finalPartition;
	private ConsensusFunction function;
	private List<Clusterer> algorithms;
		
	public ClusteringEnsembleRunner(){
		init();
	}
	
	public ClusteringEnsembleRunner(ConsensusFunction function, Instances dataset){
		init();
		
		this.function = function;
		this.dataset = dataset;
	}
	
	private void init(){
		algorithms = new ArrayList<Clusterer>();
	}
	
	public void run(){
		Instances[] partitions = new Instances[algorithms.size()];
		Remove filter = new Remove();
		
		for (int i = 0; i < partitions.length; i++) {
			partitions[i] = new Instances(dataset);
			
			try {
				filter.setAttributeIndices("" + (partitions[i].classIndex() + 1));
				filter.setInputFormat(partitions[i]);
				partitions[i] = Filter.useFilter(partitions[i], filter);
			} catch (Exception e) {
				e.printStackTrace();
				System.exit(-1);
			}
			
			partitions[i] = PartitionGenerator.generatePartition(algorithms.get(i), partitions[i]);
		}
		
		finalPartition = function.combine(partitions);
	}
	
	public void addAlgorithm(Clusterer algorithm){
		algorithms.add(algorithm);
	}
		
	public Instances getDataset() {
		return dataset;
	}
	
	public Instances getFinalPartition(){
		return finalPartition;
	}

	public ConsensusFunction getFunction() {
		return function;
	}

	public void setDataset(Instances dataset) {
		this.dataset = dataset;
	}

	public void setFunction(ConsensusFunction function) {
		this.function = function;
	}

}
