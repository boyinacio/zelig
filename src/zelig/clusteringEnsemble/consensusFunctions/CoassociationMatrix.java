package zelig.clusteringEnsemble.consensusFunctions;

import java.util.HashMap;

import weka.clusterers.HierarchicalClusterer;
import weka.core.Instance;
import weka.core.Instances;
import zelig.clusteringEnsemble.ConsensusFunction;
import zelig.clusteringEnsemble.PartitionGenerator;
import zelig.util.distances.CoassociationDistance;

public class CoassociationMatrix implements ConsensusFunction {
	
	private CoassociationDistance distance;
	private Instances originalDataset;
	HashMap<Instance, Integer> instanceIndexMap;
		
	@Override
	public Instances combine(Instances[] partitions) {
		distance = new CoassociationDistance(
				partitions, originalDataset, instanceIndexMap, false);
		
		HierarchicalClusterer clusterer = new HierarchicalClusterer();

		clusterer.setNumClusters(originalDataset.numClasses());
		clusterer.setDistanceFunction(distance);
		clusterer.setNumClusters(this.originalDataset.numClasses());
				
		return PartitionGenerator.generatePartition(clusterer, this.originalDataset);
	}

	public void setOriginalDataset(Instances originalDataset) {
		this.originalDataset = originalDataset;
		instanceIndexMap = new HashMap<Instance, Integer>();
		
		for (int i = 0; i < this.originalDataset.numInstances(); i++) {
			instanceIndexMap.put(this.originalDataset.instance(i), i);
		}
	}

}
