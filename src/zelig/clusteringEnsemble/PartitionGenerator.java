package zelig.clusteringEnsemble;

import weka.clusterers.Clusterer;
import weka.core.Instances;
import zelig.util.ClustererAssigner;

/**
 * Implements a generator of partitions. From a given dataset and a clustering algorithm,
 * it uses that algorithm to produce a partition from dataset
 */
public class PartitionGenerator {
				
	/**
	 * Generates a partition from given dataset using Clustering Algorithm algorithm
	 * @param algorithm Clustering algorithm used for generate partition 
	 * @param dataset Input dataset 
	 * @return partition generated through Clustering Algorithm
	 */
	public static Instances generatePartition(Clusterer algorithm, Instances dataset){
		ClustererAssigner assigner = new ClustererAssigner();
		try {
			algorithm.buildClusterer(dataset);
		} catch (Exception e) {
			System.out.println("ERRO! Não foi possível gerar uma partição para a base!");
			e.printStackTrace();
		}
		
		return assigner.assignFromClusterer(algorithm, dataset);
	}

}
