package zelig.util.distances;

import weka.core.Instance;

public interface Distance {

	public double distanceBetween(Instance A, Instance B);
	
}
