package zelig.util.distances;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import weka.core.Instance;
import weka.core.Instances;
import weka.core.NormalizableDistance;
import weka.core.TechnicalInformation;
import weka.core.TechnicalInformation.Field;
import weka.core.TechnicalInformation.Type;
import weka.core.TechnicalInformationHandler;

public class CoassociationDistance extends NormalizableDistance implements
		Cloneable, TechnicalInformationHandler {

	/**
	 * 
	 */
	
	Instances[] partitionsForCalculus; 
	Instances partitionsForDistance;
	private static final long serialVersionUID = 1L;
	private double[][] matrix;
	private HashMap<Instance, Integer> instanceIndexMap;
	private boolean similarityDissimilarity; // True = similarity | False =
												// dissimilarity

	/**
	 * Construtor de CoassociationDistance 
	 * 
	 * @param partitionsForCalculus
	 * 				   O conjunto de partições base para montar a matriz
	 * @param useSimilarity
	 * 				   Se a matriz será usada para representar distancias de similaridade
	 * 				   ou dissimilaridade (true = similaridade | false = dissimilaridade)
	 */
	public CoassociationDistance(Instances[] partitionsForCalculus, Instances partitionsForDistance, 
			HashMap<Instance,Integer> map, boolean useSimilarity) {
		
		this.partitionsForCalculus = partitionsForCalculus;
		this.partitionsForDistance = partitionsForDistance;
		
		similarityDissimilarity = useSimilarity;

		int numInstances = partitionsForCalculus[0].numInstances();

		matrix = new double[numInstances][numInstances];

		for (int i = 0; i < matrix.length; i++) {
			Arrays.fill(matrix[i], 0);
		}

		instanceIndexMap = map;

		// Os dois primeiros "for"s percorrem as partições
		for (int i = 0; i < partitionsForCalculus.length; i++) {
			for (int j = i + 1; j < partitionsForCalculus.length; j++) {

				// Esses dois últimos fors percorrem as instancias
				for (int k = 0; k < numInstances; k++) {
					for (int l = k; l < numInstances; l++) {
						if (partitionsForCalculus[i].instance(k).classValue() == partitionsForCalculus[j]
								.instance(l).classValue()) {
							matrix[k][l]++;

							if (k != l) {
								matrix[l][k]++;
							}

						}
					}
				}

			}
		}

		for (int i = 0; i < numInstances; i++) {
			for (int j = 0; j < numInstances; j++) {
				matrix[i][j] /= partitionsForCalculus.length;
			}
		}

	}

	/**
	 * Seta se o uso dessa matriz será como distância de similaridade ou
	 * dissimilaridade.
	 * 
	 * @param useSimilarity
	 *            flag que indica se o uso dessa matriz será como distância de
	 *            similaridade ou dissimilaridade. (true = usa como similaridade,
	 *            false = usa como dissimilaridade)
	 * 
	 */
	public void setUseSimilarity(boolean useSimilarity) {
		similarityDissimilarity = useSimilarity;
	}

	/**
	 * Calculates the distance between two instances.
	 * 
	 * @param first
	 *            the first instance
	 * @param second
	 *            the second instance
	 * @return the distance between the two given instances
	 */
	public double distance(Instance first, Instance second) {						
		int x = find(first);
		int y = find(second);

		if (!similarityDissimilarity) {
			return 1 - matrix[x][y];
		}

		return matrix[x][y];
	}
	
	private int find(Instance I){
		for(Map.Entry<Instance, Integer> entry: instanceIndexMap.entrySet()){
			if(compare(I,entry.getKey())){
				return entry.getValue();
			}
		}
		return 0;
	}
	
	private boolean compare(Instance A, Instance B){
		for (int att = 0; att < A.numAttributes()-1; att++) {
			if(A.value(att) != B.value(att)){
				return false;
			}
		}
		return true;
	}
	

	@Override
	public String getRevision() {
		return "Revision 0.1 2015-04";
	}

	@Override
	public TechnicalInformation getTechnicalInformation() {
		TechnicalInformation result;

		result = new TechnicalInformation(Type.MISC);
		result.setValue(Field.AUTHOR, "A. L. N. Fred and A. K. Jain");
		result.setValue(Field.TITLE,
				"Combining multiple clusterings using evidence accumulation");
		result.setValue(
				Field.URL,
				"http://web.cs.msu.edu/prip/ResearchProjects/cluster_research/papers/TPAMI-0239-0504.R1.pdf");

		return result;
	}

	@Override
	public String globalInfo() {
		return "Implementing Co-Association matrix like distance (or similarity) function.\n\n";
	}

	@Override
	protected double updateDistance(double currDist, double diff) {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix.length; j++) {
				if (matrix[i][j] == currDist) {
					matrix[i][j] += diff;
					return matrix[i][j];
				}
			}
		}
		return 0;
	}

}
