package zelig.util.distances;

import weka.core.Instance;

public class JoaoCarlosDistance implements Distance {

	@Override
	public double distanceBetween(Instance A, Instance B) {
		double diff = 0;
		for (int i = 1; i < A.numAttributes() - 1; i++) {
			if ( A.attribute( i ).isNominal() ) {
			   diff += A.value( i ) == B.value( i ) ? 0 : 1;
			}					
			// atributo numerico	
			else{
			   diff += Math.abs( A.value( i ) - B.value( i ) );
			}				
		}
		return diff;
	}
}
