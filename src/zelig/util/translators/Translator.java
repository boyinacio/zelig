package zelig.util.translators;

import jmetal.core.Solution;
import weka.core.Instances;

public interface Translator {

	public Instances translate(Solution solution, Instances instances);
	
}
