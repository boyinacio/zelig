package zelig.util.translators;

import java.util.Arrays;
import java.util.HashMap;

import jmetal.core.Solution;
import jmetal.core.Variable;
import jmetal.util.JMException;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instances;

public class ClusterNumberTranslator implements Translator {

	@Override
	public Instances translate(Solution solution, Instances instances) {
		int[] labels = generateLabels(solution);
		return translate(labels, instances);
	}

	public Instances translate(int[] labels, Instances instances) {
		// Coloca a classe para ser o primeiro atributo (para poder-se mexer na
		// verdadeira classe)
		instances.setClassIndex(0);

		// Coloca para a classe ser um atributo cluster
		instances.deleteAttributeAt(instances.numAttributes() - 1);

		// Copia o atributo "labels" para a variável aux
		int[] aux = new int[labels.length];
		for (int i = 0; i < aux.length; i++) {
			aux[i] = labels[i];
		}

		// Ordena aux (ela vai servir para gerar os labels das classes a serem
		// colocados na base
		Arrays.sort(aux);

		// Clusters aqui é usado para ajudar a fazer a *lista de labels dos
		// clusters*
		String[] clusters = getLabels(aux);

		FastVector attributeValues = new FastVector();
		for (int i = 0; i < clusters.length; i++) {
			if ((i > 0) && (clusters[i].compareTo(clusters[i - 1]) == 0)) {
				continue;
			}
			attributeValues.addElement(clusters[i]);
		}

		// Vetor de inteiros numeros de 0 ao tamanho de "attributeValues"
		int[] realClusters = new int[attributeValues.size()];
		for (int i = 0; i < realClusters.length; i++) {
			realClusters[i] = i + 1;
		}

		String[] grupos = getLabels(realClusters);

		/*
		 * Este mapa faz a relação entre os labels de "attributeValues" e
		 * "realClusters" de modo que os clusters fiquem com labels de 0 a
		 * clusters-1
		 */
		HashMap<String, String> mapa = new HashMap<String, String>();
		for (int i = 0; i < grupos.length; i++) {
			mapa.put((String) attributeValues.elementAt(i), grupos[i]);
		}

		attributeValues.removeAllElements();

		for (int i = 0; i < grupos.length; i++) {
			attributeValues.addElement(grupos[i]);
		}

		// Aqui, "clusters" é usado para colocar em cada instância seu
		// respectivo class label
		clusters = getLabels(labels);

		instances.insertAttributeAt(new Attribute("class", attributeValues),
				instances.numAttributes());

		// Recoloca a classe como o último atributo
		instances.setClassIndex(instances.numAttributes() - 1);

		for (int i = 0; i < instances.numInstances(); i++) {
			instances.instance(i).setValue(instances.numAttributes() - 1,
					mapa.get(clusters[i]));
		}

		return instances;
	}

	private int[] generateLabels(Solution solution) {
		Variable[] vars = solution.getDecisionVariables(); // Cromossomo
		int[] varsInt = new int[vars.length];

		for (int i = 0; i < vars.length; i++) {
			try {
				varsInt[i] = (int) vars[i].getValue();
			} catch (JMException e) {
				e.printStackTrace();
				System.exit(-1);
			}
		}

		return varsInt;
	}

	private String[] getLabels(int[] v) {
		String[] labelsS = new String[v.length];

		for (int i = 0; i < labelsS.length; i++) {
			labelsS[i] = "cluster" + v[i];
		}

		return labelsS;
	}

}
