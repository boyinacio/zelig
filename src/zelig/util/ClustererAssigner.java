package zelig.util;

import weka.clusterers.Clusterer;
import weka.core.Instance;
import weka.core.Instances;

public class ClustererAssigner implements Assigner {

	public ClustererAssigner() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Instances assignFromClusterer(Clusterer clusterer, Instances dataset) {
		dataset.setClassIndex(dataset.numAttributes() - 1);

		int size = dataset.numInstances();
		int cluster = 0;
		Instance I;

		for (int i = 0; i < size; i++) {
			I = dataset.instance(i);

			try {
				cluster = clusterer.clusterInstance(I);
			} catch (Exception e) {
				// e.printStackTrace();
				cluster = -1;
			}

			I.setClassValue(cluster);
		}

		return dataset;
	}

}
