package zelig.util;

public class Combinatorics {

	public static long combinationOf(long a, long b) {
		if ((a == b) || (b == 0)){
			return 1;
		} else if(b == (a-1)){
			return a;
		}
		
		if(b > (a-b)){
			b = Math.abs(a-b);
		}
		
		return arrange(a, b) / factorial(b);
	}

	public static long factorial(long n) {
		if (n < 3) {
			return n;
		}

		long x = 2;
		for (long i = 3; i <= n; i++) {
			x *= i;
		}

		return x;
	}

	public static long arrange(long a, long b) {
		if ((a == b) || (b == 0))
			return 1;

		long x = a;
		for (long i = x - 1; i >= (a - b + 1); i--) {
			x *= i;
		}
		return x;
	}
	
	
	
}
