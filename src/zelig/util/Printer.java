package zelig.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

import jmetal.core.Solution;
import jmetal.core.SolutionSet;
import weka.core.Instances;
import zelig.util.clusteringEvaluators.AdjustedRandIndex;
import zelig.util.clusteringEvaluators.BasicDaviesBouldin;
import zelig.util.clusteringEvaluators.Sillhouette;
import zelig.util.translators.ClusterNumberTranslator;

public class Printer {

	
	private ArrayList<Double> db;
	private ArrayList<Double> rand;
	private ArrayList<Double> sil;
	
	public Printer(){
		db = new ArrayList<Double>();
		rand = new ArrayList<Double>();
		sil = new ArrayList<Double>();
	}
	
	private Instances loadBase(Solution S, Instances base) {
		ClusterNumberTranslator translator = new ClusterNumberTranslator();
		Instances I = translator.translate(S, base);
		return I;
	}

	public void printData(SolutionSet population,
			Comparator<Solution> comparator, Instances base) {
		int popSize = population.size();
		
		population.sort(comparator);
		Solution best = population.get(popSize - 1);
		Solution worst = population.get(0);

		Solution S;
		Instances I;

		int clustersBest = loadBase(best, new Instances(base))
				.numClasses();
		int clustersWorst = loadBase(worst, new Instances(base))
				.numClasses();
		
		System.out.println(best);

		double[] db = new double[this.db.size()];
		double[] rand = new double[this.rand.size()];
		double[] sil = new double[this.sil.size()];
		
		for (int j = 0; j < sil.length; j++) {
			db[j] = this.db.get(j);
			rand[j] = this.rand.get(j);
			sil[j] = this.sil.get(j);
		}

//		for (int i = 0; i < popSize; i++) {
//			S = population.get(i);
//			I = loadBase(S, new Instances(base));
//
//			try {
//				db[i] = BasicDaviesBouldin.main(I);
//			} catch (IOException e) {
//				e.printStackTrace();
//				System.exit(1);
//			}
//
//			rand[i] = AdjustedRandIndex.ARI(base, I);
//			try {
//				sil[i] = Sillhouette.Silhouette(base);
//			} catch (IOException e) {
//				e.printStackTrace();
//				System.exit(1);
//			}
//		}

		Arrays.sort(db);
		Arrays.sort(rand);
		Arrays.sort(sil);

		System.out.println(clustersBest + "\t" + db[0] + "\t"
				+ rand[popSize - 1] + "\t" + sil[popSize - 1]);
		System.out.println(clustersWorst + "\t" + db[popSize - 1] + "\t"
				+ rand[0] + "\t" + sil[0]);
		System.out.println("-" + "\t" + Statistics.mean(db) + "\t"
				+ Statistics.mean(rand) + "\t" + Statistics.mean(sil));
		System.out.println("-" + "\t" + Statistics.deviation(db) + "\t"
				+ Statistics.deviation(rand) + "\t" + Statistics.deviation(sil));

	}
	
	public void printIndexes(Solution S, Instances base){
		Instances I = loadBase(S, new Instances(base));
		
		try {
			db.add(BasicDaviesBouldin.main(I));
			sil.add(Sillhouette.Silhouette(I));
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}

		rand.add(AdjustedRandIndex.ARI(base, I));
		
		System.out.print(db.get(db.size()-1) + "\t" + rand.get(rand.size()-1) + "\t" + sil.get(sil.size()-1));
				
	}
	
	public void cleanAll(){
		this.db.clear();
		this.rand.clear();
		this.sil.clear();
	}

}
