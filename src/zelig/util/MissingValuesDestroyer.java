package zelig.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.JFileChooser;

import weka.core.Instances;
import weka.core.converters.ArffSaver;

/*
 * Este programa abre um ARFF e mostra a media dos atributos
 */

public class MissingValuesDestroyer {
	
	public static double [][] centroide;

	public static void main ( String [] args ) throws IOException {
		File file = openFile();
		
		if( file != null ){
			
			// abrir arff
			FileInputStream inFile = new FileInputStream( file );
			InputStreamReader in = new InputStreamReader( inFile );
			Instances base = new Instances( in );
			
			// observa��o: no aquivo arff o atributo das class deve ser o �ltimo
			base.setClassIndex( base.numAttributes() - 1 );
			
			
			Hashtable <Integer,Vector<Integer>> classPerInstance = new Hashtable <Integer,Vector<Integer>> ();
			// Chave: valor da classe
			// Valor: quantidade de instancias que tem esse classe
			
			for(int i=0;i<base.numInstances();i++){
				if( !classPerInstance.containsKey( (int) base.instance( i ).classValue() ))
					classPerInstance.put( (int) base.instance( i ).classValue(), new Vector <Integer> () );
				classPerInstance.get( (int) base.instance( i ).classValue() ).add( i );
			}
			
			Enumeration <Integer> e = classPerInstance.keys();
						
			while( e.hasMoreElements() ){
				int cluster = e.nextElement();
				Vector <Integer> instances = classPerInstance.get( cluster );
				
				Object [] median = new Object [ base.numAttributes() -1 ];
				for(int i=0;i<base.numAttributes()-1;i++){
					if( base.attribute( i ).isNumeric() ){
						median[i] = medianNumeric( base, instances, i );
					} else if ( base.attribute( i ).isNominal() ){
						median[i] = medianNominal( base, instances, i );
					}
					
					for(int j = 0; j < base.numInstances(); j++){
						if(classPerInstance.containsKey((int) base.instance(j).classValue())){
							if(base.instance(j).isMissing(i)){
								if(base.instance(j).attribute(i).isNumeric()){
									base.instance(j).setValue(i, (Double) median[i]);
								} else {
									base.instance(j).setValue(i, (String) median[i]);
								}
							}
						}
					}
					
				}
								
				System.out.println( "Cluster: " + cluster );
				//System.out.println(median.length);
				for(int i=0;i<median.length;i++){
					System.out.println( "\tMedian Attribute " + base.attribute( i ).name() + ": " + median[i] );
				}
				System.out.println();
			}
			
			ArffSaver saver = new ArffSaver();
			saver.setInstances(base);
			saver.setFile(new File("files/"+base.relationName()+"_novo.arff"));
			saver.writeBatch();
			
		}
	}
	
	public static double medianNumeric ( Instances base, Vector<Integer> index, int attribute ){
		Double median = null;
		if( base.attribute( attribute ).isNumeric() ){
			double m = 0;
			for(int i=0;i<index.size();i++){
				m += base.instance( index.get( i ) ).value( attribute );
			}
			m /= index.size();
			median = m;
		}
		return median;
	}
	
	public static String medianNominal ( Instances base, Vector<Integer> index, int attribute ){
		String median = null;
		
		if ( base.attribute( attribute ).isNominal() ){
			int [] qtd = new int [ base.attribute( attribute ).numValues() ];
			Arrays.fill( qtd, 0 );
			Hashtable <String,Integer> values = new Hashtable <String,Integer> ();
			Hashtable <Integer,String> valuesInverse = new Hashtable <Integer,String> ();
			for(int i=0;i<index.size();i++){
				String key = base.instance( index.get( i ) ).stringValue( attribute );
				if( ! values.containsKey( key ) ){
					int value = values.size();
					values.put( key, value );
					valuesInverse.put( value, key );
				}
				qtd[ values.get( key ) ]++;
			}
			int max = 0;
			for(int i=0;i<qtd.length;i++){
				if( qtd[i] > qtd[max])
					max = i;
			}
			median = valuesInverse.get( max );
		}
		
		return median;
	}
		
	public static File openFile (){
		File selected = null;
		
		JFileChooser chooser = new JFileChooser( "." );
		if( chooser.showOpenDialog( null ) == JFileChooser.APPROVE_OPTION ){
			selected = chooser.getSelectedFile();
		}
		
		return selected;
	}
}