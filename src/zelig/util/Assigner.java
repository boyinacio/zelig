package zelig.util;

import weka.clusterers.Clusterer;
import weka.core.Instances;

public interface Assigner {
	
	public Instances assignFromClusterer(Clusterer clusterer, Instances dataset);

}
