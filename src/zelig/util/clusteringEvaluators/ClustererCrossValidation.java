package zelig.util.clusteringEvaluators;

import java.util.Random;

import weka.clusterers.ClusterEvaluation;
import weka.clusterers.DensityBasedClusterer;
import weka.core.Instances;

public class ClustererCrossValidation {

	public static double evaluate(Instances dataset,
			DensityBasedClusterer clusterer, int folds, int seed) {
		double res = 0;

		try {
			res = ClusterEvaluation.crossValidateModel(clusterer, dataset,
					folds, new Random(seed));
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}

		return res;
	}

}
