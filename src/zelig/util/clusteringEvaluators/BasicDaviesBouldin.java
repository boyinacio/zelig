package zelig.util.clusteringEvaluators;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import javax.swing.JFileChooser;
import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.SparseInstance;

public class BasicDaviesBouldin {

	private static File[] files;
	
	public interface Mensurably<T> {

		public double distance ( T a, T b );
	}

	private static void AbrirTodos() {
		
		//String url = ".";
		//String url = "D:/JCXavier/Doutorado/Sandwich/K-Means";		
		//String url = "D:/JCXavier/Doutorado/Sandwich/BaseDados/Filmes";
		//String url = "D:/JCXavier/Doutorado/Sandwich/BaseDados/Nursery";
		String url = "D:/JCXavier/Doutorado/Sandwich/BaseDados/Viagem";
		
		JFileChooser chooser = null;
		chooser = new JFileChooser( url );
		chooser.addChoosableFileFilter(new FiltraExtensoes());
		chooser.setMultiSelectionEnabled( true );
		if( chooser.showOpenDialog( null ) == JFileChooser.APPROVE_OPTION ) {
			files = chooser.getSelectedFiles();
		}
	}
	
	public static File openFile () {
		File selected = null;
		JFileChooser chooser = null;
		//String url = "C:/JCXavier/Doutorado/Sandwich/K-Means/Instancia300/";
		//String url = "D:/JCXavier/Doutorado/Sandwich/Implementacao/src/";
		//String url = "D:/JCXavier/Doutorado/Sandwich/Aglomerativo_Hierarquico/Instancia750/TSMRC/Merge_6k/";
		
		//String url = "D:/JCXavier/Doutorado/Sandwich/BaseDados/Nursery/RCRAI/";
		String url = "bases/";
		
		chooser = new JFileChooser( url );
		chooser.addChoosableFileFilter(new FiltraExtensoes());
		chooser.setAcceptAllFileFilterUsed( false );
		chooser.setFileSelectionMode( JFileChooser.FILES_AND_DIRECTORIES );
		if( chooser.showOpenDialog( null ) == JFileChooser.APPROVE_OPTION ) {
			selected = chooser.getSelectedFile();
		}
		return selected;
	}

	public static double main (Instances instances) throws IOException {
		Mensurably <Instance> difference = new Mensurably <Instance>() {

			public double distance ( Instance a, Instance b ) {
				double diff = 0;
				for (int i = 1; i < a.numAttributes() - 1; i++) {
					if ( a.attribute( i ).isNominal() ) {
					   diff += a.value( i ) == b.value( i ) ? 0 : 1;
					}					
					// atributo numerico	
					else{
					   diff += Math.abs( a.value( i ) - b.value( i ) );
					}				
				}
				return diff;
			}
		};

		/*File file = openFile();
		FileInputStream inFile = new FileInputStream( file );
		InputStreamReader in = new InputStreamReader( inFile );*/
		Instances base = new Instances(instances);
		base.setClassIndex( base.numAttributes() - 1 );

		BasicDaviesBouldin bdb = new BasicDaviesBouldin();
		
		return bdb.daviesBouldin( base, difference );

		/*System.out.println(file);
		System.out.println("DB index = " +  bdb.daviesBouldin( base, difference ) );*/
	}
	
	public static void main(String[] args) throws IOException{
		Mensurably <Instance> difference = new Mensurably <Instance>() {

			public double distance ( Instance a, Instance b ) {
				double diff = 0;
				for (int i = 1; i < a.numAttributes() - 1; i++) {
					if ( a.attribute( i ).isNominal() ) {
					   diff += a.value( i ) == b.value( i ) ? 0 : 1;
					}					
					// atributo numerico	
					else{
					   diff += Math.abs( a.value( i ) - b.value( i ) );
					}				
				}
				return diff;
			}
		};

		AbrirTodos();
		
		for (int i = 0; i < files.length; i++) {
			FileInputStream inFile = new FileInputStream( files[i] );
			InputStreamReader in = new InputStreamReader( inFile );
			
			Instances base = new Instances(in);
			base.setClassIndex( base.numAttributes() - 1 );

			BasicDaviesBouldin bdb = new BasicDaviesBouldin();
			
			System.out.println(files[i]);
			System.out.println("DB index = " +  bdb.daviesBouldin( base, difference ) );
			System.out.println();
			System.out.println();
		}
		
	}

	public double daviesBouldin ( Instances base, Mensurably <Instance> difference ) {
		Instances [] group = new Instances [base.numClasses()];
		for( int i = 0; i < group.length; i++ )
			group[ i ] = new Instances( base, 0 );
		
		for( int i = 0; i < base.numInstances(); i++ ) {
			group[ (int) base.instance( i ).classValue() ].add( base.instance( i ) );
		}

		double [] E = new double [group.length];
		Instance [] center = new Instance [group.length];
		for( int i = 0; i < group.length; i++ ) {
			center[ i ] = this.center( group[ i ] );
			center[ i ].setDataset( group[ i ] );
			E[ i ] = medianSquaredDistance( group[ i ], center[ i ], difference );
		}

		double db = 0;
		for( int i = 0; i < group.length; i++ ) {
			db += mrs( i, E, center, difference );
		}
		return db / (double) group.length;
	}

	protected double mrs ( int index, double [] E, Instance [] center, Mensurably <Instance> groupDistance ) {
		double max = Double.NEGATIVE_INFINITY;
		for( int i = 0; i < center.length; i++ ) {
			if( i != index ) {
				double aux = rs( E[ index ], E[ i ], center[ index ], center[ i ], groupDistance );
				if( aux > max ) {
					max = aux;
				}
			}
		}
		return max;
	}

	protected double rs ( double E1, double E2, Instance center1, Instance center2, Mensurably <Instance> groupDistance ) {
		return ( E1 + E2 ) / ( groupDistance.distance( center1, center2 ) );
	}

	protected double medianSquaredDistance ( Instances base, Instance center, Mensurably <Instance> difference ) {
		double median = 0;
		for( int i = 0; i < base.numInstances(); i++ )
			median += Math.pow( difference.distance( base.instance( i ), center ), 2 );
		return median / (double) base.numInstances();
	}

	protected Instance center ( Instances base ) {
		Instance center = new SparseInstance( base.firstInstance() );
		for( int i = 0; i < base.numAttributes(); i++ ) {
			if( base.attribute( i ).isNominal() )
				center.setValue( i, moda( base, base.attribute( i ) ) );
			else if( base.attribute( i ).isNumeric() )
				center.setValue( i, median( base, base.attribute( i ) ) );
			else throw new IllegalArgumentException( "Attribute " + base.attribute( i ).name() + " not is numeric or nominal" );
		}
		return center;
	}

	protected double moda ( Instances base, Attribute att ) {
		double [] count = new double [att.numValues()];
		for( int i = 0; i < base.numInstances(); i++ )
			count[ (int) base.instance( i ).value( att ) ]++;
		return maxIndex( count );
	}

	protected double median ( Instances base, Attribute att ) {
		double median = 0;
		for( int i = 0; i < base.numInstances(); i++ )
			median += base.instance( i ).value( att );
		return median / (double) base.numInstances();
	}

	private int maxIndex ( double ... values ) {
		int max = 0;
		for( int i = 1; i < values.length; i++ )
			if( values[ i ] > values[ max ] )
				max = i;
		return max;
	}
}