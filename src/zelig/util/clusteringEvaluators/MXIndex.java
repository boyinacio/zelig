package zelig.util.clusteringEvaluators;

import java.util.HashSet;

import weka.core.Instances;
import zelig.util.Calculator;
import zelig.util.distances.Distance;

public class MXIndex {
	
	public static double evaluate(Instances instances, Distance distance,
			double EPS) {
		int quantClusters = instances.numClasses();
		double result = 0;

		for (int i = 0; i < quantClusters; i++) {
			result += evaluate(instances, distance, EPS, i);
		}

		return (result / quantClusters);
	}

	public static double evaluate(Instances instances, Distance distance,
			double EPS, double clusterID) {
		int instancesInCluster;
		double[][] distanceMatrix;
		int[][] vizinhos;

		instancesInCluster = 0;

		for (int j = 0; j < instances.numInstances(); j++) {
			if (instances.instance(j).classValue() == clusterID) {
				instancesInCluster++;
			}
		}

		if (instancesInCluster > 0) {
			
			// Catches distance matrix from database
			distanceMatrix = Calculator.baseDistanceMatrix(instances, distance,
					(int) (clusterID - (clusterID % 1)));

			// In this matrix, position (i,j) indicates if ith instance
			// is neighbor of jth one (if distance between them is <= EPS)
			vizinhos = new int[distanceMatrix.length][distanceMatrix.length];
			HashSet<Integer> objetosNoCluster = new HashSet<Integer>();

			for (int j = 0; j < vizinhos.length; j++) {
				for (int k = j + 1; k < vizinhos.length; k++) {
					if (distanceMatrix[j][k] <= EPS) {
						vizinhos[j][k] = k;
						vizinhos[k][j] = j;
					} else {
						vizinhos[j][k] = -1;
						vizinhos[k][j] = -1;
					}
				}
			}

			for (int j = 0; j < vizinhos.length; j++) {
				for (int k = j + 1; k < vizinhos.length; k++) {
					if (vizinhos[j][k] > -1) {
						objetosNoCluster.add(k);
					}
				}
			}

			return (((double) objetosNoCluster.size()) / instancesInCluster);

		}
		
		return 0;

	}
	
	public static double evaluate(Instances instances, Distance distance,
			double EPS, double clusterID, double[][] distanceMatrix) {
		int instancesInCluster;
		int[][] vizinhos;

		instancesInCluster = 0;

		for (int j = 0; j < instances.numInstances(); j++) {
			if (instances.instance(j).classValue() == clusterID) {
				instancesInCluster++;
			}
		}

		if (instancesInCluster > 0) {
			vizinhos = new int[distanceMatrix.length][distanceMatrix.length];
			HashSet<Integer> objetosNoCluster = new HashSet<Integer>();

			for (int j = 0; j < vizinhos.length; j++) {
				for (int k = j + 1; k < vizinhos.length; k++) {
					if (distanceMatrix[j][k] <= EPS) {
						vizinhos[j][k] = k;
						vizinhos[k][j] = j;
					} else {
						vizinhos[j][k] = -1;
						vizinhos[k][j] = -1;
					}
				}
			}

			for (int j = 0; j < vizinhos.length; j++) {
				for (int k = j + 1; k < vizinhos.length; k++) {
					if (vizinhos[j][k] > -1) {
						objetosNoCluster.add(k);
					}
				}
			}

			return (((double) objetosNoCluster.size()) / instancesInCluster);

		}
		
		return 0;

	}

}
