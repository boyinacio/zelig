package zelig.util.clusteringEvaluators;

import weka.core.Instances;

public class JaccardCoefficient {

	public static double calculate(Instances original, Instances modified) {
		int numInstances = original.numInstances();

		int[] clustersOriginal = new int[numInstances];
		int[] clustersModified = new int[numInstances];

		for (int i = 0; i < numInstances; i++) {
			clustersOriginal[i] = ((Double) original.instance(i).classValue()).intValue();
			clustersModified[i] = ((Double) modified.instance(i).classValue()).intValue();
		}

		double a1 = 0, a2 = 0, a3 = 0;

		for (int i = 0; i < numInstances; i++) {
			for (int j = 0; j < numInstances; j++) {
				if ((clustersModified[i] == clustersModified[j])
						&& (clustersOriginal[i] == clustersOriginal[j])) {
					a1++;
				} else if ((clustersModified[i] == clustersModified[j])
						&& (clustersOriginal[i] != clustersOriginal[j])) {
					a2++;
				} else if ((clustersModified[i] != clustersModified[j])
						&& (clustersOriginal[i] == clustersOriginal[j])) {
					a3++;
				}
			}
		}

		return a1 / (a1 + a2 + a3);
	}

}
