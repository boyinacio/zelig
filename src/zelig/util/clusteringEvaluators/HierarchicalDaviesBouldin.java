package zelig.util.clusteringEvaluators;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import javax.swing.JFileChooser;
import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.SparseInstance;

public class HierarchicalDaviesBouldin {

	public interface Mensurably<T> {

		public double distance ( T a, T b );
	}

	public static File openFile () {
		File selected = null;
		JFileChooser chooser = null;
		String url = "D:/JCXavier/Doutorado/Sandwich/Aglomerativo_Hierarquico/Instancia300/TSMRC/Merge_4k";
		//String url = "D:/JCXavier/Doutorado/Sandwich/K-Means/Instancia300/Hierarquico";
		//String url = "D:/JCXavier/Doutorado/Sandwich/Implementacao/src";

		chooser = new JFileChooser( url );
		chooser.addChoosableFileFilter(new FiltraExtensoes());
		chooser.setAcceptAllFileFilterUsed( false );
		chooser.setFileSelectionMode( JFileChooser.FILES_AND_DIRECTORIES );
		if( chooser.showOpenDialog( null ) == JFileChooser.APPROVE_OPTION ) {
			selected = chooser.getSelectedFile();
		}
		return selected;
	}

	public static void main ( String [] args ) throws IOException {
		Mensurably <Instance> difference = new Mensurably <Instance>() {

			public double distance ( Instance a, Instance b ) {
				double diff = 0;
				for (int i = 1; i < a.numAttributes() - 1; i++) {
					if ( a.attribute( i ).isNominal() ) {
						// testa se atributo � hier�quico
						if (a.stringValue(i).startsWith("R.")){
							diff += computeDistance(a.stringValue(i), b.stringValue(i));
							//diff += Math.pow( computeDistance(a.stringValue(i), b.stringValue(i)), 2) ;
						}
						// atributo nao hieraquico
						else{
							diff += a.value( i ) == b.value( i ) ? 0 : 1;
							//if (a.value( i ) == b.value( i )) 	diff += Math.pow(0,2);
							//else 									diff += Math.pow(1,2);
						}					
					// atributo numerico	
					} else{
						diff += Math.abs( a.value( i ) - b.value( i ) );
						//diff += Math.pow( a.value( i ) - b.value( i ), 2 );
					}				
				}
				//diff = Math.sqrt(diff);
				return diff;
			}

			private double computeDistance(String s1, String s2) {
				String[] t1 = s1.split("[.]");
				String[] t2 = s2.split("[.]");
				
				if (s1.compareTo(s2) == 0){
					return 0;
				}
				else {
					if (t1[1].compareToIgnoreCase(t2[1]) == 0){
						if (t1[2].compareToIgnoreCase(t2[2]) == 0){
							if (t1[3].compareToIgnoreCase(t2[3]) == 0){
								if (t1[4].compareToIgnoreCase(t2[4]) == 0) {
									if (t1[5].compareToIgnoreCase(t2[5]) == 0) {
										if (t1[6].compareToIgnoreCase(t2[6]) == 0) {
											return 0.015625;
										}
										else {
											return 0.03125;
										}
									}
									else {
										return 0.0625;
									}
								}
								else {
									return 0.125;
								}		
							}
							else {
								return 0.25;
							}						
						}
						else {
							return 0.5;
						}	
					}
					else {
						return 1;
					}
				}
			}
		};

		File file = openFile();
		FileInputStream inFile = new FileInputStream( file );
		InputStreamReader in = new InputStreamReader( inFile );
		Instances base = new Instances( in );
		base.setClassIndex( base.numAttributes() - 1 );

		HierarchicalDaviesBouldin hdb = new HierarchicalDaviesBouldin();

		System.out.println(file);
		System.out.println("DB index = " +  hdb.daviesBouldin( base, difference ) );
	}

	public double daviesBouldin ( Instances base, Mensurably <Instance> difference ) {
		Instances [] group = new Instances [base.numClasses()];
		for( int i = 0; i < group.length; i++ )
			group[ i ] = new Instances( base, 0 );
		
		for( int i = 0; i < base.numInstances(); i++ ) {
			group[ (int) base.instance( i ).classValue() ].add( base.instance( i ) );
		}

		double [] E = new double [group.length];
		Instance [] center = new Instance [group.length];
		for( int i = 0; i < group.length; i++ ) {
			center[ i ] = this.center( group[ i ] );
			center[ i ].setDataset( group[ i ] );
			E[ i ] = medianSquaredDistance( group[ i ], center[ i ], difference );
		}

		double db = 0;
		for( int i = 0; i < group.length; i++ ) {
			db += mrs( i, E, center, difference );
		}
		return db / (double) group.length;
	}

	protected double mrs ( int index, double [] E, Instance [] center, Mensurably <Instance> groupDistance ) {
		double max = Double.NEGATIVE_INFINITY;
		for( int i = 0; i < center.length; i++ ) {
			if( i != index ) {
				double aux = rs( E[ index ], E[ i ], center[ index ], center[ i ], groupDistance );
				if( aux > max ) {
					max = aux;
				}
			}
		}
		return max;
	}

	protected double rs ( double E1, double E2, Instance center1, Instance center2, Mensurably <Instance> groupDistance ) {
		return ( E1 + E2 ) / ( groupDistance.distance( center1, center2 ) );
	}

	protected double medianSquaredDistance ( Instances base, Instance center, Mensurably <Instance> difference ) {
		double median = 0;
		for( int i = 0; i < base.numInstances(); i++ )
			median += Math.pow( difference.distance( base.instance( i ), center ), 2 );
		return median / (double) base.numInstances();
	}

	protected Instance center ( Instances base ) {
		Instance center = new SparseInstance( base.firstInstance() );
		for( int i = 0; i < base.numAttributes(); i++ ) {
			if( base.attribute( i ).isNominal() )
				center.setValue( i, moda( base, base.attribute( i ) ) );
			else if( base.attribute( i ).isNumeric() )
				center.setValue( i, median( base, base.attribute( i ) ) );
			else throw new IllegalArgumentException( "Attribute " + base.attribute( i ).name() + " not is numeric or nominal" );
		}
		return center;
	}

	protected double moda ( Instances base, Attribute att ) {
		double [] count = new double [att.numValues()];
		for( int i = 0; i < base.numInstances(); i++ )
			count[ (int) base.instance( i ).value( att ) ]++;
		return maxIndex( count );
	}

	protected double median ( Instances base, Attribute att ) {
		double median = 0;
		for( int i = 0; i < base.numInstances(); i++ )
			median += base.instance( i ).value( att );
		return median / (double) base.numInstances();
	}

	private int maxIndex ( double ... values ) {
		int max = 0;
		for( int i = 1; i < values.length; i++ )
			if( values[ i ] > values[ max ] )
				max = i;
		return max;
	}
}