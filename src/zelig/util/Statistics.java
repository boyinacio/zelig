package zelig.util;

public class Statistics {

	// Fonte:
	// http://cs.nyu.edu/courses/fall03/V22.0002-003/05_Deviation.htm
	
	public Statistics() {
		// TODO Auto-generated constructor stub
	}

	/** Method for computing deviation of double values */
	public static double deviation(double[] x) {
		double mean = mean(x);
		double squareSum = 0;

		for (int i = 0; i < x.length; i++) {
			squareSum += Math.pow(x[i] - mean, 2);
		}

		return Math.sqrt((squareSum) / (x.length - 1));
	}

	/** Method for computing deviation of int values */
	public static double deviation(int[] x) {
		double mean = mean(x);
		double squareSum = 0;

		for (int i = 0; i < x.length; i++) {
			squareSum += Math.pow(x[i] - mean, 2);
		}

		return Math.sqrt((squareSum) / (x.length - 1));
	}

	/** Method for computing mean of an array of double values */
	public static double mean(double[] x) {
		double sum = 0;

		for (int i = 0; i < x.length; i++)
			sum += x[i];

		return sum / x.length;
	}

	/** Method for computing mean of an array of int values */
	public static double mean(int[] x) {
		int sum = 0;

		for (int i = 0; i < x.length; i++)
			sum += x[i];

		return sum / x.length;
	}

}
