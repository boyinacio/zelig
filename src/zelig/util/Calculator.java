package zelig.util;

import java.util.Comparator;
import java.util.LinkedList;

import jmetal.core.SolutionSet;
import weka.core.Instance;
import weka.core.Instances;
import zelig.util.distances.Distance;

public class Calculator {

	private static LinkedList<Integer> objectsFromCluster(Instances base,
			int clusterId) {
		int numInstances = base.numInstances();
		Instance object;

		LinkedList<Integer> objetosNoCluster = new LinkedList<Integer>();

		for (int i = 0; i < numInstances; i++) {
			object = base.instance(i);
			if (object.classValue() == ((double) clusterId)) {
				objetosNoCluster.add(i);
			}
		}

		return objetosNoCluster;

	}

	public static double[] minValuesOfAttributes(Instances base) {
		double[] results = new double[base.numAttributes()];

		int numInstances = base.numInstances();

		double menor;

		for (int att = 0; att < results.length; att++) {
			menor = base.instance(0).value(att);
			results[att] = menor;
			for (int inst = 1; inst < numInstances; inst++) {
				if (base.instance(inst).value(att) < menor) {
					results[att] = base.instance(inst).value(att);
					menor = results[att];
				}
			}
		}

		return results;
	}

	public static double[] maxValuesOfAttributes(Instances base) {
		double[] results = new double[base.numAttributes()];

		int numInstances = base.numInstances();

		double maior;

		for (int att = 0; att < results.length; att++) {
			maior = base.instance(0).value(att);
			results[att] = maior;
			for (int inst = 1; inst < numInstances; inst++) {
				if (base.instance(inst).value(att) > maior) {
					results[att] = maior;
					maior = base.instance(inst).value(att);
				}
			}
		}

		return results;
	}

	private static Instance centroidOf(Instances base, int clusterId) {
		Instance centroid = new Instance(base.instance(0));
		int numAttributes = centroid.numAttributes();

		double accumulator;
		Instance object;

		LinkedList<Integer> objetosNoCluster = objectsFromCluster(base,
				clusterId);

		int numObjetos = objetosNoCluster.size();

		for (int i = 0; i < numAttributes; i++) {
			accumulator = 0;
			for (Integer objeto : objetosNoCluster) {
				object = base.instance(objeto);
				accumulator += object.value(i);
			}
			centroid.setValue(i, accumulator / numObjetos);
		}

		return centroid;
	}

	public static Instance[] centroidsOf(Instances base) {
		int numGrupos = base.numClasses();
		Instance[] centroids = new Instance[numGrupos];

		for (int i = 0; i < centroids.length; i++) {
			centroids[i] = centroidOf(base, i);
		}

		return centroids;
	}

	public static double withinClusterVariationOfCluster(Instances base,
			int clusterId, Distance distance) {
		Instance centroid = centroidOf(base, clusterId);
		double resultado = 0;

		Instance object;

		LinkedList<Integer> objetosNoCluster = objectsFromCluster(base,
				clusterId);

		for (Integer objeto : objetosNoCluster) {
			object = base.instance(objeto);
			resultado += distance.distanceBetween(object, centroid);
		}

		return resultado;
	}

	public static double totalWithinClusterVariationOfCluster(Instances base,
			Distance distance) {
		int numGrupos = base.numClasses();
		double resultado = 0;

		for (int i = 0; i < numGrupos; i++) {
			resultado += withinClusterVariationOfCluster(base, i, distance);
		}

		return resultado;
	}

	public static double[][] baseDistanceMatrix(Instances instances,
			Distance distance) {
		int numInstances = instances.numInstances();
		double[][] distanceMatrix = new double[numInstances][numInstances];

		for (int i = 0; i < distanceMatrix.length; i++) {
			for (int j = i + 1; j < distanceMatrix.length; j++) {
				distanceMatrix[i][j] = distance.distanceBetween(
						instances.instance(i), instances.instance(j));
				distanceMatrix[j][i] = distanceMatrix[i][j];
			}
		}

		return distanceMatrix;
	}

	public static double[][] baseDistanceMatrix(Instances instances,
			Distance distance, int clusterID) {
		int numInstances = 0;
		int aux = instances.numInstances();

		for (int i = 0; i < aux; i++) {
			if (instances.instance(i).classValue() == ((double) clusterID)) {
				numInstances++;
			}
		}

		double[][] distanceMatrix = new double[numInstances][numInstances];

		int ii = 0, jj = 0;
		for (int i = 0; i < aux; i++) {
			for (int j = i; j < aux; j++) {
				if (instances.instance(i).classValue() == instances.instance(j)
						.classValue()) {
					distanceMatrix[ii][jj] = distance.distanceBetween(
							instances.instance(i), instances.instance(j));
					distanceMatrix[jj][ii] = distanceMatrix[ii][jj];
					jj++;
					
					if(jj == numInstances){
						jj = 0;
						ii++;
						if(ii == numInstances){
							ii = 0;
						}
					}
				}
			}
		}

		return distanceMatrix;
	}
	
	public static double[] bestWorstMediaStdDev(SolutionSet population, Comparator comparator){
		double[] values = new double[4];
				
		values[0] = population.best(comparator).getObjective(0);
		values[1] = population.worst(comparator).getObjective(0);
		
		double[][] fitnesses = population.writeObjectivesToMatrix();
		
		double[] fits = new double[fitnesses[0].length];
		
		for (int i = 0; i < fitnesses[0].length; i++) {
			fits[i] = fitnesses[0][i];
		}
		
		values[2] = Statistics.mean(fits);
		values[3] = Statistics.deviation(fits);
 		
		return values;
		
	}

}
